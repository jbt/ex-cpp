#include "roman_numerals.h"

#include <array>
#include <string_view>

using namespace std::literals;

namespace {
  using P = std::pair<std::string_view, std::size_t>;
  constexpr std::array numerals{
      // clang-format off
      P{ "M"sv,1000UL}
    , P{"CM"sv, 900UL}
    , P{ "D"sv, 500UL}
    , P{"CD"sv, 400UL}
    , P{ "C"sv, 100UL}
    , P{"XC"sv,  90UL}
    , P{ "L"sv,  50UL}
    , P{"XL"sv,  40UL}
    , P{ "X"sv,  10UL}
    , P{"IX"sv,   9UL}
    , P{ "V"sv,   5UL}
    , P{"IV"sv,   4UL}
    , P{ "I"sv,   1UL}
    };
  // clang-format on
} // namespace

auto roman_numerals::convert(std::size_t val) -> std::string
{
  std::string rv;
  for (auto [numeral, numeral_val] : numerals) {
    while (val >= numeral_val) {
      rv.append(numeral);
      val -= numeral_val;
    }
  }
  return rv;
}
