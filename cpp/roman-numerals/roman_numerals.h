#if !defined(ROMAN_NUMERALS_H)
#define ROMAN_NUMERALS_H

#include <string>

/*! Roman Numerals exercize
 */
namespace roman_numerals {
  /*! Convert
   *  @return roman numeral string
   *  @param val The value to convert
   */
  std::string convert(std::size_t val);
} // namespace roman_numerals

#endif // ROMAN_NUMERALS_H
