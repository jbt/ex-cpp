#if !defined(BOB_H)
#define BOB_H

#include <string_view>

/*! The bob exercize
 */
namespace bob {

  /*! Get a response from Bob.
   * @param  query What you say to him
   * @return What he says in response.
   */
  std::string_view hey(std::string_view query);
} // namespace bob

#endif // BOB_H
