#include "bob.h"

#include <cctype>

#include <algorithm>

namespace rg = std::ranges;

auto bob::hey(std::string_view query) -> std::string_view
{
  if (rg::all_of(query, ::isspace)) {
    return "Fine. Be that way!";
  }
  auto q = query[query.find_last_not_of(" ")] == '?';
  auto yell = rg::any_of(query, ::isupper) && rg::none_of(query, ::islower);
  if (q) {
    return yell ? "Calm down, I know what I'm doing!" : "Sure.";
  } else if (yell) {
    return "Whoa, chill out!";
  }
  return "Whatever.";
}
