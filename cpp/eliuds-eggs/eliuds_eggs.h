#pragma once

#include <bitset>

namespace chicken_coop {

  constexpr auto EGG_LAYING_SPOT_COUNT = 32;
  using Display = std::bitset<EGG_LAYING_SPOT_COUNT>;

  constexpr auto positions_to_quantity(Display displayed) -> unsigned
  {
    return displayed.count();
  }

} // namespace chicken_coop
