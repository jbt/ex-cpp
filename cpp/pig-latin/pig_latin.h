#pragma once

#include <string>
#include <string_view>

/*! Pig Latin exercize
 */
namespace pig_latin {
  /*! @param english an English-language phrase
   *  @return The phrase munged into pig latin
   */
  std::string translate(std::string_view english);
} // namespace pig_latin
