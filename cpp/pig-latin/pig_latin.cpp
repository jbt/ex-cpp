#include "pig_latin.h"

#include <algorithm>
#include <array>

#if __cpp_lib_string_contains

using namespace std::literals;

namespace {
  constexpr std::string_view cVowels{"aeiouAEIOU"};
  constexpr std::string_view cWhiteSpace{" \n\t\r\v"};
  constexpr auto cRuleOne = std::array{"xr"sv, "yt"sv};

  void rotate_word(std::string &phrase, std::size_t b, std::size_t e);
} // namespace

auto pig_latin::translate(std::string_view english) -> std::string
{
  std::string result{english};
  for (auto i = 0U; i < result.size();
       i = result.find_first_not_of(cWhiteSpace, i)) {
    auto j = std::min(result.size(), result.find_first_of(cWhiteSpace, i));
    rotate_word(result, i, j);
    result.insert(j, "ay"sv);
    i = j + 2U;
  }
  return result;
}

namespace {
  void rotate_word(std::string &phrase, std::size_t b, std::size_t e)
  {
    // Rule 1
    if (cVowels.contains(phrase.at(b))) {
      return;
    }
    auto const word = std::string_view{phrase}.substr(b, e - b);
    for (auto pre : cRuleOne) {
      if (word.starts_with(pre)) {
        return;
      }
    }

    // Rule 2
    auto new_start = phrase.find_first_of(cVowels, b);

    if (new_start < e) {
      // Rule 3
      if (phrase.at(new_start - 1U) == 'q' && phrase.at(new_start) == 'u') {
        ++new_start;
      }
    } else {
      // Rule 4
      new_start = phrase.find('y', b);
    }
    // clang-format off
    std::rotate(
        std::next(phrase.begin(), b)
      , std::next(phrase.begin(), new_start)
      , std::next(phrase.begin(), e));
    // clang-format on
  }
} // namespace

#endif // __cpp_lib_string_contains
