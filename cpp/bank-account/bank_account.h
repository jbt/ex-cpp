#pragma once

#include <mutex>

namespace Bankaccount {

  /*! @brief Simplistic ledger account
   *  @note  All operations other than is_open() are conservatively
   * synchronized. Therefore it is largely thread-safe, but not very performant.
   */
  class Bankaccount {
    std::mutex mtx_;
    long balance_;
    bool open_ = false;

  public:
    /*! @brief   (re)open this account
     *  @details Transactions can only be performed on an open account, so this
     * preceeds all.
     *  @pre     !is_open()
     *  @post    is_open() && balance() == 0
     *  @throws  std::runtime_error
     */
    void open();

    /*! @brief  Close the account to future transactions
     *  @pre    is_open()
     *  @post   !is_open()
     *  @throws std::runtime_error
     */
    void close();

    /*! @brief  Check if the account is currently opened
     *  @return true if open() has been called one time more than close() has
     *  @note  This method does not block on other concurrent actions,
     *    so a pattern like \code if(a.is_open()){a.close();} \endcode
     *    can still throw if the a.close() is being called concurrently.
     */
    [[nodiscard]] auto is_open() const -> bool;

    /*! @brief  Add to the balance.
     *  @param amount
     *    Non-negative scalar amount of the default currency of this account
     *  @pre    is_open()
     *  @throws std::runtime_error
     */
    void deposit(long amount);

    /*! @brief  Remove from the balance.
     *  @param  amount
     *    Non-negative scalar amount of currency ≤ the previous balance
     *  @pre    is_open()
     *  @throws std::runtime_error
     */
    void withdraw(long amount);

    /*! @brief   Check the current balance
     *  @returns Current balance
     *  @pre     is_open()
     *  @note    non-const as it is synchronized with concurrent transactions
     */
    auto balance() -> long;
  }; // class Bankaccount

} // namespace Bankaccount
