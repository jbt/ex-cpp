#include "bank_account.h"

#include <stdexcept>

using Acct = Bankaccount::Bankaccount;

namespace {
  void check(bool b, std::string m)
  {
    if (!b) {
      throw std::runtime_error{std::move(m)};
    }
  }
} // namespace

auto Acct::balance() -> long
{
  std::unique_lock const lck{mtx_};
  check(open_, "Cannot check balance of closed account");
  return balance_;
}
void Acct::open()
{
  std::unique_lock const lck{mtx_};
  check(!open_, "Cannot open an already opened account");
  open_ = true;
  balance_ = 0;
}
void Acct::close()
{
  std::unique_lock const lck{mtx_};
  check(open_, "Cannot close an account that was not opened");
  open_ = false;
}
auto Acct::is_open() const -> bool
{
  return open_;
}
void Acct::deposit(long x)
{
  std::unique_lock const lck{mtx_};
  check(x >= 0, "Cannot deposit negative");
  check(open_, "Cannot deposit into closed account");
  balance_ += x;
}
void Acct::withdraw(long x)
{
  std::unique_lock const lck{mtx_};
  check(x >= 0, "Cannot withdraw negative");
  check(x <= balance_, "Insufficient balance");
  check(is_open(), "Don't withdraw from an already-closed account.");
  balance_ -= x;
}
