#if !defined(PHONE_NUMBER_H)
#define PHONE_NUMBER_H

#include <array>
#include <string_view>

/*! Phone Number exercize
 */
namespace phone_number {
  /*! A canonicalized 10-digit phone number
   */
  class phone_number {
    std::array<char,10> number_;
  public:
    /*! Construct from various string representations
     * @param input Possibly human-readable representation containing 10 digits and optionally a country code
     */
    explicit phone_number(std::string_view input);

    /*! @return the canonicalized number
     */
    auto number() const -> std::string_view;
  };
}  // namespace phone_number

#endif // PHONE_NUMBER_H
