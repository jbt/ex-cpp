#include "phone_number.hpp"

#include <algorithm>
#include <stdexcept>

using Self = phone_number::phone_number;

auto Self::number() const -> std::string_view
{
  return {number_.data(), number_.size()};
}
Self::phone_number(std::string_view in)
{
  if (in.front() == '+') {
    in.remove_prefix(in.find(' ', 1UL));
  }
  auto o = number_.begin();
  using namespace std::literals::string_literals;
  for (auto c : in) {
    if (std::isdigit(c)) {
      if (o == number_.end()) {
        throw std::domain_error{"Too many digits in input. " + std::string{in}};
      }
      if (o != number_.begin() || c != '1') {
        *(o++) = c;
      }
    }
  }
  if (o != number_.end()) {
    throw std::domain_error{"Too few digits in input. " + std::string{in}};
  }
  if (number_[0] == '0') {
    throw std::domain_error{"Area codes don't start with 0. " +
                            std::string{in}};
  }
  if (number_[3] == '0' || number_[3] == '1') {
    throw std::domain_error{"Exchanges don't start with 0. " + std::string{in}};
  }
}
