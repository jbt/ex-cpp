#include "darts.h"

namespace missed_target {
  static_assert(darts::score(-9, 9) == 0);
}
namespace on_the_outer_circle {
  static_assert(darts::score(0, 10) == 1);
}

namespace on_the_middle_circle {
  static_assert(darts::score(-5, 0) == 5);
}

namespace on_the_inner_circle {
  static_assert(darts::score(0, -1) == 10);
}

namespace exactly_on_centre {
  static_assert(darts::score(0, 0) == 10);
}

namespace near_the_centre {
  static_assert(darts::score(-0.1, -0.1) == 10);
}

namespace just_within_the_inner_circle {
  static_assert(darts::score(0.7, 0.7) == 10);
}

namespace just_outside_the_inner_circle {
  static_assert(darts::score(0.8, -0.8) == 5);
}

namespace just_within_the_middle_circle {
  static_assert(darts::score(-3.5, 3.5) == 5);
}

namespace just_outside_the_middle_circle {
  static_assert(darts::score(-3.6, -3.6) == 1);
}

namespace just_within_the_outer_circle {
  static_assert(darts::score(-7.0, 7.0) == 1);
}

namespace just_outside_the_outer_circle {
  static_assert(darts::score(7.1, -7.1) == 0);
}

namespace asymmetric_position_between_the_inner_and_middle_circles {
  static_assert(darts::score(0.5, -4) == 5);
}
