#if !defined(DARTS_H)
#define DARTS_H

#include <cstdint>

/*! Darts exercize
 */
namespace darts {

  /*! @return the distance from (0,0) squared
   *  @param x Horizontal cartesian coordinate
   *  @param y Vertical cartesian coordinate
   */
  constexpr auto radius_squared(double x, double y)
  {
    return x * x + y * y;
  }

  /*! @return the points earned from a single throw
   *  @param x Horizontal position hit by the dart
   *  @param y Vertical position hit by the dart
   */
  constexpr auto score(double x, double y) -> std::uint8_t
  {
    auto r2 = radius_squared(x, y);
    if (r2 > 100.) {
      return 0U;
    }
    if (r2 > 25.) {
      return 1U;
    }
    if (r2 > 1.) {
      return 5U;
    }
    return 10U;
  }
} // namespace darts

#endif // DARTS_H
