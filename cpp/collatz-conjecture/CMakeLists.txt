project(
  collatz-conjecture
  LANGUAGES CXX
)

find_package(Catch2 REQUIRED)
find_package(Boost REQUIRED)
find_package(Threads REQUIRED)

file(
  GLOB
  srcs
  *.h
  *.cpp
)
file(
  GLOB
  test_srcs
  *_test.cpp
)
list(REMOVE_ITEM srcs ${test_srcs})

add_library(
  collatz-conjecture_exercize
  ${srcs}
)
target_compile_features(
  collatz-conjecture_exercize
  PUBLIC
    cxx_std_23
)
target_compile_options(
  collatz-conjecture_exercize
  PUBLIC
    -fsanitize=undefined
    -O0 -g3 -ggdb3 -fno-inline
)
target_compile_options(
  collatz-conjecture_exercize
  PRIVATE
    -Wall
    -Wextra
    -Wpedantic
    -Werror
)
target_link_libraries(
  collatz-conjecture_exercize
  PUBLIC
    Boost::headers
    Threads::Threads
    ubsan
)

include(clang-tools)
clang_tooling(collatz-conjecture_exercize)

foreach(t few all slow)
  add_executable(
    collatz-conjecture_${t}_runner
    ${test_srcs}
    ../lasagna/test/tests-main.cpp
  )
  target_link_libraries(
    collatz-conjecture_${t}_runner
    PUBLIC
      collatz-conjecture_exercize
      # Boost::headers
      Catch2::Catch2
  )
  add_custom_command(
    OUTPUT ${t}.cert
    COMMAND timeout ${MAX_TEST_TIME} $<TARGET_FILE:collatz-conjecture_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.cert
    DEPENDS collatz-conjecture_${t}_runner
    COMMENT "collatz-conjecture_${t} passed"
  )
  add_custom_command(
    OUTPUT ${t}.memcheck.cert
    COMMAND timeout ${MAX_VALGRIND_TEST_TIME} valgrind --exit-on-first-error=yes --error-exitcode=99 --leak-check=full --track-origins=yes --quiet $<TARGET_FILE:collatz-conjecture_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.memcheck.cert
    DEPENDS collatz-conjecture_${t}_runner
    COMMENT "collatz-conjecture_${t} passed under valgrind"
  )
endforeach()
target_compile_definitions(
  collatz-conjecture_all_runner
  PUBLIC
    EXERCISM_RUN_ALL_TESTS=1
)
target_compile_definitions(
  collatz-conjecture_slow_runner
  PUBLIC
    EXERCISM_INCLUDE_BENCHMARK=1
    CATCH_CONFIG_ENABLE_BENCHMARKING=1
)

foreach(t few all)
  add_custom_target(
    check_collatz-conjecture_${t}
    ALL
    DEPENDS ${t}.cert ${t}.memcheck.cert
    COMMENT "collatz-conjecture ${t} tests."
  )
endforeach()
add_custom_target(
  check_collatz-conjecture_slow
  ALL
  DEPENDS slow.cert
  COMMENT "collatz-conjecture Slow/benchmark tests."
)
add_dependencies(check_collatz-conjecture_all  check_collatz-conjecture_few)
add_dependencies(check_collatz-conjecture_slow check_collatz-conjecture_all)
