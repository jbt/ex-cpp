#include "collatz_conjecture.h"

#include <stdexcept>

namespace {
  auto next_step(std::uintmax_t i) -> std::uintmax_t
  {
    return (i % 2) ? 3UL * i + 1UL : i / 2UL;
  }
} // namespace

auto collatz_conjecture::steps(std::intmax_t start) -> std::uint32_t
{
  if (start < 1) {
    throw std::domain_error{"Positive integers only."};
  }
  auto i = static_cast<std::uintmax_t>(start);
  std::uint32_t step;
  for (step = 0; i != 1ULL; ++step) {
    i = next_step(i);
  }
  return step;
}
