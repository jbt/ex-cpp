#if !defined(COLLATZ_CONJECTURE_H)
#define COLLATZ_CONJECTURE_H

#include <cstdint>

///< The Collatz Conjecture
namespace collatz_conjecture {

  /*! @return number of steps for given number to become 1
   *  @param i
   *    The starting number
   */
  auto steps(std::intmax_t i) -> std::uint32_t;
} // namespace collatz_conjecture

#endif // COLLATZ_CONJECTURE_H
