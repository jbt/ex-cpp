#include "grains.h"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)
namespace square_1 {
  static_assert(1ULL == grains::square(1));
}

namespace square_2 {
  static_assert(2ULL == grains::square(2));
}

namespace square_3 {
  static_assert(4ULL == grains::square(3));
}

namespace square_4 {
  static_assert(8ULL == grains::square(4));
}

namespace square_16 {
  static_assert(32768ULL == grains::square(16));
}

namespace square_32 {
  static_assert(2147483648ULL == grains::square(32));
}

namespace square_64 {
  static_assert(9223372036854775808ULL == grains::square(64));
}

namespace total {
  static_assert(18446744073709551615ULL == grains::total());
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
