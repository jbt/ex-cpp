#pragma once

#include <cassert>
#include <cstdint>

#include <algorithm>
#include <ranges>

namespace grains {

  /**
   * @brief A type that can represent the count of grains on a board.
   * @note There's 64 squares on the board, each with 2X grains as the previous.
   *       There's 64 bits in this integer, each valued 2X the previous.
   * @details fastest unsigned integer type with width of at least 64 bits
   * respectively
   * @see https://en.cppreference.com/w/cpp/types/integer
   */
  using grain_count = std::uint_fast64_t;

  constexpr grain_count square(int one_based_ordinal)
  {
    assert(one_based_ordinal >= 1);
    assert(one_based_ordinal <= 64);
    return grain_count{1} << (one_based_ordinal - 1);
  }

  constexpr grain_count total()
  {
    namespace v = std::views;
    auto squares = v::iota(1, 65);
    auto grains = squares | v::transform(square);
    return std::ranges::fold_left(grains, grain_count{}, std::plus{});
  }
} // namespace grains
