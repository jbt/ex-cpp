#pragma once

#include <algorithm>
#include <array>
#include <map>
#include <ranges>
#include <string_view>
#include <vector>

/*! Grade School exercize
 */
namespace grade_school {

  /*! Tracking students' names and grade levels
   */
  class school {
  public:
    /*! The students in a given grade
     */
    using grade_t = std::vector<std::string_view>;

    /*! @param which Grade level (0=Kindergarten)
     *  @return The students in that grade
     */
    auto grade(short which) const -> grade_t const&;

    /*! @return The students in the school, sorted by grade
     */
    [[nodiscard]] constexpr auto roster() const
    {
      auto has_students = [](auto&& p) { return !p.second.empty(); };
      namespace v = std::ranges::views;
      return v::zip(v::iota(0), roster_) | v::filter(has_students);
    }

    /*! Add a student to a grade
     *  @param name     The student's name. Must outlive *this
     *  @param in_grade The student's grade level.
     */
    void add(std::string_view name, short in_grade);

  private:
    static constexpr auto GRADES = 13UL;
    std::array<grade_t, GRADES> roster_; // index 0 is kindergarten
  };

  /*! The data type of the view returned by roster()
   */
  using roster_t = decltype(school{}.roster());
} // namespace grade_school

auto operator==(std::vector<std::string> const& a,
                std::vector<std::string_view> const& b) -> bool;
auto operator==(std::map<int, std::vector<std::string>> const& m,
                grade_school::roster_t v) -> bool;
