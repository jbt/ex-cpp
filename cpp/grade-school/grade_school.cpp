#include "grade_school.h"
#include <range/v3/view/filter.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/zip.hpp>

namespace gs = grade_school;

void gs::school::add(std::string_view name, short in_grade)
{
  auto& g = roster_.at(in_grade);
  auto spot = std::lower_bound(g.begin(), g.end(), name);
  g.insert(spot, name);
}

auto gs::school::grade(short which) const -> grade_t const&
{
  return roster_.at(which);
}

auto operator==(std::vector<std::string> const& a,
                std::vector<std::string_view> const& b) -> bool
{
  return std::ranges::equal(a, b);
}
auto operator==(std::map<int, std::vector<std::string>> const& m,
                grade_school::roster_t v) -> bool
{
  auto a = m.cbegin();
  for (auto const& b : v) {
    if (a == m.cend()) {
      return false;
    }
    if (a->first != b.first) {
      return false;
    }
    if (a->second != b.second) {
      return false;
    }
    ++a;
  }
  return a == m.cend();
}
