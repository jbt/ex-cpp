#include "zebra_puzzle.h"

#include <cassert>

#include <algorithm>
#include <iostream>

#if __cpp_lib_string_contains

using namespace std::literals;

namespace {
  // clang-format off
  constexpr auto attributes =
      std::array{
             "1"sv,       "2"sv,        "3"sv,       "4"sv,        "5"sv,
          "Blue"sv,     "Red"sv,    "Green"sv,   "Ivory"sv,   "Yellow"sv,
           "Dog"sv,     "Fox"sv,    "Horse"sv,   "Snail"sv,    "Zebra"sv,
        "Coffee"sv,    "Milk"sv,       "OJ"sv,     "Tea"sv,    "Water"sv,
         "Chess"sv,   "Dance"sv, "Football"sv,   "Paint"sv,     "Read"sv,
    "Englishman"sv,"Japanese"sv,"Norwegian"sv,"Spaniard"sv,"Ukrainian"sv
    };
  constexpr auto axes = std::array{
    "House"sv, "Color"sv, "Pet"sv, "Drink"sv, "Hobby"sv, "Nation"sv
    };
  // clang-format on
  constexpr auto index_of(std::string_view at) -> unsigned
  {
    return std::ranges::find(attributes, at) - attributes.begin();
  }
  constexpr auto axis_start_index(unsigned i) -> unsigned
  {
    return i - i % 5U;
  }
  constexpr auto axis_end_index(unsigned i) -> unsigned
  {
    return i + 5U - i % 5U;
  }
  constexpr auto axis_start_index(std::string_view ax) -> unsigned
  {
    auto i = std::ranges::find(axes, ax) - axes.begin();
    return i * 5;
  }
  constexpr auto axis_of(unsigned i) -> std::string_view
  {
    return axes.at(i / 5);
  }
  constexpr auto coord(unsigned i, unsigned j) -> std::array<unsigned, 2>
  {
    auto k = std::min(i, j);
    auto l = std::max(i, j);
    if (k < 5) {
      return {k, l - 5U};
    } else {
      auto row = 30U - l + 4;
      assert(row < 25);
      assert(k < 30);
      return {row, k - 5U};
    }
  }
  static_assert(coord(5, 29)[0] == 5);
  static_assert(coord(5, 29)[1] == 0);

} // namespace

namespace zp = zebra_puzzle;

auto zp::solve() -> Solution
{
  Grid grid;
  // 1. There are five houses.
  // 2. The Englishman lives in the red house.
  grid.is("Englishman", "Red");
  // 3. The Spaniard owns the dog.
  grid.is("Spaniard", "Dog");
  // 4. The person in the green house drinks coffee.
  grid.is("Green", "Coffee");
  // 5. The Ukrainian drinks tea.
  grid.is("Ukrainian", "Tea");
  // 7. The snail owner likes to go dancing.
  grid.is("Snail", "Dance");
  // 8. The person in the yellow house is a painter.
  grid.is("Yellow", "Paint");
  // 9. The person in the middle house drinks milk.
  grid.is("3", "Milk");
  // 10. The Norwegian lives in the first house.
  grid.is("Norwegian", "1");
  // 13. The person who plays football drinks orange juice.
  grid.is("Football", "OJ");
  // 14. The Japanese person plays chess.
  grid.is("Japanese", "Chess");

  auto i = 0U;
  auto j = 5U;
  while (!grid.filled()) {
    // 6. The green house is immediately to the right of the ivory house.
    grid.right_of("Green", "Ivory");
    // 11. The person who enjoys reading lives in the house next to the person
    //     with the fox.
    grid.adjacent("Read", "Fox");
    // 12. The painter's house is next to the house with the horse.
    grid.adjacent("Paint", "Horse");
    // 15. The Norwegian lives next to the blue house.
    grid.adjacent("Norwegian", "Blue");
    grid.experiment(i, j);
  }
  grid.print();
  Solution sol;
  sol.drinksWater = grid.get("Water", "Nation");
  sol.ownsZebra = grid.get("Zebra", "Nation");
  return sol;
}

using G = zp::Grid;
G::Grid()
{
  for (auto &row : rows_) {
    row.fill(' ');
  }
}
auto G::is(std::string_view a, std::string_view b, std::string_view why) -> bool
{
  std::string w{a};
  w.push_back('=');
  w.append(b).append(" because ").append(why);
  auto i = index_of(a);
  auto j = index_of(b);
  return is(i, j, w);
}
auto G::is_not(std::string_view a, std::string_view b, std::string_view why)
    -> bool
{
  auto i = index_of(a);
  auto j = index_of(b);
  std::string w{a};
  w.append("!=").append(b).append(" because ").append(why);
  return is_not(i, j, w);
}
auto G::is_not(unsigned i, unsigned j, std::string w) -> bool
{
  if (axis_of(i) == axis_of(j)) {
    return false;
  }
  auto c = coord(i, j);
  if (is_not(c, w)) {
    process_of_elimination(i, j, w);
    process_of_elimination(j, i, w);
    w.insert(0UL, "Chaining 'is': ");
    for_equivalent(i, [this, j, &w](auto k) { is_not(k, j, w); });
    for_equivalent(j, [this, i, &w](auto k) { is_not(k, i, w); });
    return true;
  }
  return false;
}
auto G::is(unsigned i, unsigned j, std::string w) -> bool
{
  if (i / 5 == j / 5) {
    return false;
  }
  auto c = coord(i, j);
  if (is(c, w)) {
    x_mutually_exclusive(i, j, w);
    copy_other_axes(i, j, w);
    equivalent(i, j);
    equivalent(j, i);
    w.insert(0UL, "Chaining 'is': ");
    for_equivalent(i, [this, j, &w](auto k) { is(k, j, w); });
    for_equivalent(j, [this, i, &w](auto k) { is(k, i, w); });
    return true;
  }
  return false;
}
auto G::is(Coord const &c, std::string_view why) -> bool
{
  auto &t = tile(c);
  switch (t) {
  case 'O': return false;
  case ' ': {
    t = 'O';
    // std::clog << why << '\n';
    return true;
  }
  case 'X': throw Contradiction{std::string{why}};
  default: std::abort();
  }
}
auto G::is_not(Coord const &c, std::string_view why) -> bool
{
  auto &t = tile(c);
  switch (t) {
  case 'X': return false;
  case ' ': {
    t = 'X';
    return true;
  }
  case 'O': throw Contradiction{std::string{why}};
  default: std::abort();
  }
}
void G::x_mutually_exclusive(unsigned i, unsigned j, std::string_view why)
{
  if (j < i) {
    x_mutually_exclusive(j, i, why);
    return;
  }
  std::string w{"Marking with X tiles that are mutually-exclusive with an O: "};
  w.append(why);
  for (auto k = axis_start_index(i); k < axis_end_index(i); ++k) {
    if (k != i) {
      is_not(k, j, w);
    }
  }
  for (auto k = axis_start_index(j); k < axis_end_index(j); ++k) {
    if (k != j) {
      is_not(i, k, w);
    }
  }
}
void G::copy_other_axes(unsigned i, unsigned j, std::string_view why)
{
  std::string w;
  w.append("Copying values across other axes due to equivalence: ").append(why);
  for (auto k = 0UL; k < attributes.size(); ++k) {
    if (k == (i - i % 5) || k == (j - j % 5)) {
      k += 4;
      continue;
    }
    auto ac = coord(i, k);
    auto &a = tile(ac);
    auto bc = coord(j, k);
    auto &b = tile(bc);
    if (a == b) {
      // noop
    } else if (b == 'O') {
      is(ac, w);
    } else if (a == 'O') {
      is(bc, w);
    } else if (b == 'X') {
      is_not(ac, w);
    } else if (a == 'X') {
      is_not(bc, w);
    } else {
      std::abort();
    }
  }
}
auto G::right_of(std::string_view a, std::string_view b) -> bool
{
  auto result = is_not(a, b, "They're in adjacent houses.");
  auto ah = get(a, "House");
  auto bh = get(b, "House");
  std::string w{a};
  w.append(" is in the house right of ")
      .append(b)
      .append(" and prior to considering that ")
      .append(a)
      .append(" could only have been in one of: ")
      .append(ah)
      .append(" while ")
      .append(b)
      .append(" could only have been in one of ")
      .append(bh);
  for (std::string h = "1"; h[0] <= '5'; h[0]++) {
    if (ah.find(h[0] + 1) == std::string::npos) {
      result = is_not(b, h, w) || result;
    }
    if (bh.find(h[0] - 1) == std::string::npos) {
      result = is_not(a, h, w) || result;
    }
  }
  return result;
}
auto G::adjacent(std::string_view a, std::string_view b) -> bool
{
  auto result = is_not(a, b, "They're in adjacent houses.");
  auto ah = get(a, "House");
  auto bh = get(b, "House");
  std::string w{a};
  w.append(" and ")
      .append(b)
      .append(" are in adjacent houses and prior to considering that ")
      .append(a)
      .append(" could only have been in one of: ")
      .append(ah)
      .append(" while ")
      .append(b)
      .append(" could only have been in one of ")
      .append(bh);
  auto check = [this, &result, &w](auto &at, auto &h, auto &hs) {
    if (hs.find(h.front() - 1) > hs.size() &&
        hs.find(h.front() + 1) > hs.size()) {
      result = is_not(at, h, w) || result;
    }
  };
  for (std::string h = "1"; h[0] <= '5'; h[0]++) {
    check(a, h, bh);
    check(b, h, ah);
  }
  return result;
}
auto G::get(std::string_view at, std::string_view ax) const -> std::string
{
  auto i = index_of(at);
  auto j = axis_start_index(ax);
  std::string result;
  for (auto k = j; k < j + 5; ++k) {
    if (tile(coord(i, k)) != 'X') {
      result.append(attributes.at(k));
    }
  }
  return result;
}
void G::print() const
{
  std::clog << "  ";
  constexpr auto spaces = "     "sv;
  for (auto i = 1U; i < axes.size(); ++i) {
    auto a = axes[i];
    std::clog << a.substr(0U, 4U) << spaces.substr(std::min(4UL, a.size()));
  }
  std::clog << "\n  ";
  for (auto i = 5U; i < attributes.size(); ++i) {
    std::clog.put(attributes[i].front());
  }
  std::clog.put('\n');
  for (auto r = 0U; r < rows_.size(); ++r) {
    auto a = (r < 5U) ? axes.front() : axis_of(34U - r);
    auto m = r % 5U;
    std::clog.put(m < std::min(a.size(), 4UL) ? a[m] : ' ');
    if (r < 5U) {
      std::clog.put(attributes.at(r).front());
    } else {
      std::clog << attributes.at(34 - r).front();
    }
    auto &row = rows_[r];
    std::clog << std::string_view{row.data(), row.size()} << '\n';
  }
}
auto G::tile(std::array<unsigned, 2> const &c) -> char &
{
  return rows_.at(c[0]).at(c[1]);
}
auto G::tile(std::array<unsigned, 2> const &c) const -> char
{
  return rows_.at(c[0]).at(c[1]);
}
void G::equivalent(unsigned i, unsigned j)
{
  auto p = std::make_pair(static_cast<char>(i), static_cast<char>(j));

  auto it = std::lower_bound(equiv_.begin(), equiv_.end(), p);
  equiv_.insert(it, p);
}
void G::for_equivalent(unsigned i, std::function<void(unsigned)> f)
{
  auto p = std::make_pair(static_cast<char>(i), '\0');
  auto it = std::lower_bound(equiv_.begin(), equiv_.end(), p);
  for (auto j = std::distance(equiv_.begin(), it);
       j < static_cast<long>(equiv_.size()) &&
       static_cast<unsigned>(equiv_[j].first) == i;
       ++j) {
    f(equiv_[j].second);
  }
}
void G::process_of_elimination(unsigned i, unsigned j, std::string_view why)
{
  auto k = j - j % 5;
  unsigned hit = 99;
  for (auto e = k + 5; k < e; ++k) {
    auto &c = tile(coord(i, k));
    switch (c) {
    case 'O': return;
    case ' ':
      if (hit < 30) {
        return;
      } else {
        hit = k;
      }
      break;
    case 'X': continue;
    default: std::abort();
    }
  }
  if (hit < 30) {
    std::string w{"process of elimination "};
    w.append(why);
    is(i, hit, w);
  }
}
auto G::experiment(unsigned &i, unsigned &j) -> bool
{
  do {
    if (++j >= 30U) {
      if (++i > 24U) {
        return false;
      }
      j = i + 5 - i % 5;
    }
  } while (tile(coord(i, j)) != ' ');
  try {
    auto g = *this;
    g.is_not(i, j, "checking");
  } catch (Contradiction const &) {
    std::clog << "brute is(" << i << ',' << j << ")\n";
    return is(i, j, "X results in contradiction");
  }
  try {
    auto g = *this;
    g.is(i, j, "checking");
  } catch (Contradiction const &) {
    std::clog << "brute is_not(" << i << ',' << j << ")\n";
    return is_not(i, j, "O results in contradiction");
  }
  return false;
}
auto G::filled() const -> bool
{
  for (auto r = 0U; r < rows_.size(); ++r) {
    auto &row = rows_[r];
    auto s = r / 5;
    auto t = 5 - s;
    auto v = t * 5;
    ;
    std::string_view const u{row.data(), v};

    if (u.contains(' ')) {
      return false;
    }
  }
  return true;
}

#endif // __cpp_lib_string_contains
