#if !defined(ZEBRA_PUZZLE_H)
#define ZEBRA_PUZZLE_H

#include <array>
#include <functional>
#include <string>
#include <string_view>
#include <vector>

/*! Zebra Puzzle exercize
 */
namespace zebra_puzzle {
  /*! Exception type for logical contradiction
   */
  struct Contradiction {
    std::string why; ///< Text explaining the situation
  };
  /*! A row, column coordinate
   */
  using Coord = std::array<unsigned, 2>;
  /*! Pretty standard logic puzzle grid-of-grids
   */
  class Grid {
    std::array<std::array<char, 25>, 25> rows_;
    std::vector<std::pair<char, char>> equiv_;

    char &tile(Coord const &);
    char tile(Coord const &) const;
    bool is(Coord const &, std::string_view why);
    bool is(unsigned i, unsigned j, std::string why);
    bool is_not(unsigned i, unsigned j, std::string why);
    bool is_not(Coord const &, std::string_view why);
    void x_mutually_exclusive(unsigned i, unsigned j, std::string_view why);
    void copy_other_axes(unsigned i, unsigned j, std::string_view why);
    void equivalent(unsigned i, unsigned j);
    void for_equivalent(unsigned i, std::function<void(unsigned)> f);
    void process_of_elimination(unsigned i, unsigned j, std::string_view why);

  public:
    /*! Construct an empty grid.
     */
    Grid();
    /*! These attributes do apply to the same person/house/pet
     *  @param a an attribute
     *  @param b an attribute
     *  @param reason How we know this to be true
     *  @return whether a change was made to *this
     */
    bool is(std::string_view a, std::string_view b,
            std::string_view reason = "rule");
    /*! These attributes do not apply to the same person/house/pet
     *  @param a an attribute
     *  @param b an attribute
     *  @param reason How we know this to be true
     *  @return whether a change was made to *this
     */
    bool is_not(std::string_view a, std::string_view b,
                std::string_view reason = "rule");
    /*! @param a attribute applying to a houes immediately to the right of b
     *  @param b attribute applying to a houes immediately to the left of a
     *  @return whether a change was made to *this
     */
    bool right_of(std::string_view a, std::string_view b);
    /*! These attributes apply to adjacent houses in either order
     *  @param a An attribute's name
     *  @param b A different attribute's name. Maybe same axis.
     *  @return whether a change was made to *this
     */
    bool adjacent(std::string_view a, std::string_view b);
    /*! Brute force
     *  @param i an attribute index
     *  @param j an attribute index greater than i and not sharing an axis
     *  @return true if a modification to *this occurred
     */
    bool experiment(unsigned &i, unsigned &j);

    /*! @param at The name of an attribute
     *  @param ax The name of an axis
     *  @return The name of the value of the ax axis match for the attribute at.
     *      If it's not yet decided, concatenation of all values not yet
     * eliminated.
     */
    std::string get(std::string_view at, std::string_view ax) const;
    /*! Dump the grid to output in human-readable form for debugging.
     */
    void print() const;
    /*! @return whether there are no more spaces left to fill
     */
    bool filled() const;
  };
  /*! Canned answer
   */
  struct Solution {
    std::string drinksWater; ///< Person of which nationality drinks water?
    std::string ownsZebra;   ///< Person of which nationality owns a zebra?
  };
  /*! Get two selected points from the solution
   *  @return Answers to the 2 original questions
   */
  auto solve() -> Solution;
} // namespace zebra_puzzle

#endif // ZEBRA_PUZZLE_H
