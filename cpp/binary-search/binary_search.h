#if !defined(BINARY_SEARCH_H)
#define BINARY_SEARCH_H

#include <vector>

namespace binary_search {
  auto find(std::vector<int> const &v, int i) -> std::size_t;
} // namespace binary_search

#endif // BINARY_SEARCH_H
