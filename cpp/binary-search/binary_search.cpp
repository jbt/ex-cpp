#include "binary_search.h"

#include <stdexcept>

auto binary_search::find(std::vector<int> const &v, int i) -> std::size_t
{
  auto b = 0UL;
  auto e = v.size();
  while (b < e) {
    auto m = b + (e - b) / 2;
    auto &val = v[m];
    if (val < i) {
      b = m + 1;
    } else if (val > i) {
      e = m;
    } else {
      return m;
    }
  }
  throw std::domain_error{std::to_string(i)};
}
