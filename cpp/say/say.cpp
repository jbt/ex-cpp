#include "say.h"

#include <array>
#include <stdexcept>
#include <string_view>

namespace {
  void build(std::string& out, say::Uint n, int depth = 0);
  void hundreds(std::string& out, say::Uint n);
  void two_digit(std::string& out, say::Uint n);
} // namespace

auto say::in_english(unsigned long long n) -> std::string
{
  if (!n) {
    return "zero";
  }
  std::string rv;
  build(rv, n);
  return rv;
}

namespace {
  using namespace std::literals;
  // clang-format off
  constexpr std::array small{
      "zero"sv,    "one"sv,      "two"sv,   "three"sv,    "four"sv,
      "five"sv,    "six"sv,    "seven"sv,   "eight"sv,    "nine"sv,
       "ten"sv, "eleven"sv,   "twelve"sv,"thirteen"sv,"fourteen"sv,
   "fifteen"sv,"sixteen"sv,"seventeen"sv,"eighteen"sv,"nineteen"sv
  };
  constexpr std::array tens{
    "DONTDO"sv,   "teen"sv,"twenty"sv,
    "thirty"sv,  "forty"sv, "fifty"sv,
     "sixty"sv,"seventy"sv,"eighty"sv,
    "ninety"sv
  };
  constexpr std::array groups{
                ""sv,
       " thousand"sv,
        " million"sv,
        " billion"sv,
       " trillion"sv,
    " quadrillion"sv,
    " quintillion"sv
  };
  // clang-format on
  void two_digit(std::string& out, say::Uint n)
  {
    auto t = n / 10;
    if (t > 1) {
      out.append(tens.at(t));
      n = n % 10;
      if (n) {
        out.push_back('-');
      } else {
        return;
      }
    }
    if (n < small.size()) {
      out.append(small.at(n));
      return;
    }
  }
  void hundreds(std::string& out, say::Uint n)
  {
    auto h = n / 100;
    if (h) {
      out.append(small.at(h)).append(" hundred");
      n = n % 100;
      if (n) {
        out.push_back(' ');
      } else {
        return;
      }
    }
    two_digit(out, n);
  }
  void build(std::string& out, say::Uint n, int depth)
  {
    if (depth > 3) {
      throw std::domain_error{"This seems unnecessarily restrictive."};
    }
    if (n > 999) {
      build(out, n / 1000, depth + 1);
      n %= 1000;
      if (n) {
        out.push_back(' ');
      } else {
        return;
      }
    }
    hundreds(out, n);
    out.append(groups.at(depth));
  }
} // namespace
