#if !defined(SAY_H)
#define SAY_H

#include <string>

namespace say {
  using Uint = unsigned long long;
  auto in_english(Uint) -> std::string;
} // namespace say

#endif // SAY_H
