#pragma once

#include <boost/date_time/posix_time/posix_time.hpp>

/*! Gigasecond exercize
 */
namespace gigasecond {
  /*! A point in time, granularity of one second
   */
  using TimePoint = boost::posix_time::ptime;

  /*! Add a gigasecond to before
   *  @param before the beginning of the time period
   *  @return one gigasecond later
   */
  constexpr auto advance(TimePoint before) -> TimePoint
  {
    return before + boost::posix_time::seconds(1'000'000'000);
  }
} // namespace gigasecond
