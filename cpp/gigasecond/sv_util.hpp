#pragma once

#include <stdexcept>
#include <string>
#include <string_view>

namespace sv
{
   constexpr unsigned to_u( std::string_view sv )
   {
      unsigned rv = 0U;
      for ( auto c : sv )
      {
         if ( c >= '0' && c <= '9' )
         {
            rv = rv * 10U + static_cast<unsigned>( c - '0' );
         }
         else
         {
            using namespace std::literals;
            throw std::invalid_argument{ "expected digit, got '"s + c + "' in " + __func__ };
         }
      }
      return rv;
   }
}
