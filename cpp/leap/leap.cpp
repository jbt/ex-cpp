#include "leap.h"

static_assert(leap::is_leap_year(1996));

static_assert(!leap::is_leap_year(1997));

static_assert(!leap::is_leap_year(1900));

static_assert(leap::is_leap_year(2000));

static_assert(leap::is_leap_year(2400));
