#include "leap.h"
#ifdef EXERCISM_TEST_SUITE
#include <catch2/catch.hpp>
#else
#include "test/catch.hpp"
#endif

TEST_CASE("a_known_leap_year")
{
    CHECK(leap::is_leap_year(1996));
}

#if defined(EXERCISM_RUN_ALL_TESTS)
TEST_CASE("any_old_year")
{
    CHECK(!leap::is_leap_year(1997));
}

TEST_CASE("turn_of_the_20th_century")
{
    CHECK(!leap::is_leap_year(1900));
}

TEST_CASE("turn_of_the_21st_century")
{
    CHECK(leap::is_leap_year(2000));
}

TEST_CASE("turn_of_the_25th_century")
{
    CHECK(leap::is_leap_year(2400));
}
#endif
