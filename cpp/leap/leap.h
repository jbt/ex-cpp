#pragma once

#include <cstdint>

namespace leap {
  constexpr bool is_leap_year(std::int16_t year)
  {
    if (year % 4)
      return false;
    if (year % 400 == 0)
      return true;
    if (year % 100 == 0)
      return false;
    return true;
  }
} // namespace leap
