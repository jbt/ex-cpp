#pragma once

#include <stdexcept>
#include <string>
#include <string_view>

namespace rna_transcription {
  constexpr char to_rna(char dna)
  {
    switch (dna) {
    case 'G': return 'C';
    case 'C': return 'G';
    case 'T': return 'A';
    case 'A': return 'U';
    default: throw std::invalid_argument{__func__};
    }
  }
  std::string to_rna(std::string_view strand);
} // namespace rna_transcription
