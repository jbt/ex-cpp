#include "rna_transcription.h"

#include <algorithm>

namespace rt = rna_transcription;

static_assert('G' == rt::to_rna('C'));
static_assert('C' == rt::to_rna('G'));
static_assert('A' == rt::to_rna('T'));
static_assert('U' == rt::to_rna('A'));

auto rt::to_rna(std::string_view strand) -> std::string
{
  std::string result(strand.size(), ' ');
  std::transform(strand.begin(), strand.end(), result.begin(),
                 [](char c) { return rt::to_rna(c); });
  return result;
}
