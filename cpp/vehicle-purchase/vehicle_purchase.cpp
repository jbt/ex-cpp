#include "vehicle_purchase.h"

namespace vp = vehicle_purchase;

// needs_license determines whether a license is needed to drive a type of
// vehicle. Only "car" and "truck" require a license
auto vp::needs_license(std::string kind) -> bool
{
  return kind == "car" || kind == "truck";
}

// choose_vehicle recommends a vehicle for selection. It always recommends the
// vehicle that comes first in lexicographical ordering
auto vp::choose_vehicle(std::string option1, std::string option2) -> std::string
{
  return std::min(option1, option2) + " is clearly the better choice.";
}

// calculate_resell_price calculates how much a vehicle can resell for at a
// certain age
auto vp::calculate_resell_price(double original_price, double age) -> double
{
  if (age < 3.) {
    return original_price * .8;
  } else if (age < 10.) {
    return original_price * .7;
  } else {
    return original_price * .5;
  }
}
