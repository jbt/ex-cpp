#include "space_age.h"

namespace age_in_seconds {
  constexpr space_age::space_age age(1000000);

  static_assert(1000000 == age.seconds());
} // namespace age_in_seconds

namespace {
  constexpr auto close(double a, double b) -> bool
  {
    if (a < b) {
      return close(b, a);
    }
    return a - b < .005;
  }
} // namespace

namespace age_in_earth_years {
  constexpr space_age::space_age age(1000000000);

  static_assert(close(31.69, age.on_earth()));
} // namespace age_in_earth_years

namespace age_in_mercury_years {
  constexpr space_age::space_age age(2134835688);

  static_assert(close(67.65, age.on_earth()));
  static_assert(close(280.88, age.on_mercury()));
} // namespace age_in_mercury_years

namespace age_in_venus_years {
  constexpr space_age::space_age age(189839836);

  static_assert(close(6.02, age.on_earth()));
  static_assert(close(9.78, age.on_venus()));
} // namespace age_in_venus_years

namespace age_in_mars_years {
  constexpr space_age::space_age age(2329871239);

  static_assert(close(73.83, age.on_earth()));
  static_assert(close(39.25, age.on_mars()));
} // namespace age_in_mars_years

namespace age_in_jupiter_years {
  constexpr space_age::space_age age(901876382);

  static_assert(close(28.58, age.on_earth()));
  static_assert(close(2.41, age.on_jupiter()));
} // namespace age_in_jupiter_years

namespace age_in_saturn_years {
  constexpr space_age::space_age age(3000000000);

  static_assert(close(95.06, age.on_earth()));
  static_assert(close(3.23, age.on_saturn()));
} // namespace age_in_saturn_years

namespace age_in_uranus_years {
  constexpr space_age::space_age age(3210123456);

  static_assert(close(101.72, age.on_earth()));
  static_assert(close(1.21, age.on_uranus()));
} // namespace age_in_uranus_years

namespace age_in_neptune_year {
  constexpr space_age::space_age age(8210123456);

  static_assert(close(260.16, age.on_earth()));
  static_assert(close(1.58, age.on_neptune()));
} // namespace age_in_neptune_year
