#pragma once

/*! Space Age exercism
 */
namespace space_age {
  /*! Human ages in SPAAAAAAACE
   */
  class space_age {
  public:
    /*! Construct from the absolute age
     *  @param seconds number of seconds for which the human has been alive
     */
    constexpr space_age(long seconds)
    : secs_{seconds}
    {
    }

    /*! @defgroup years Access the age in local (local) years
     *  @{
     */

    /*! @return absolute age in seconds
     */
    constexpr long seconds() const { return secs_; }

    /*! @return age in Earth years
     */
    constexpr double on_earth() const
    {
      return static_cast<double>(secs_) / 31557600.;
    }

    /*! @return age in Mercury years
     */
    constexpr double on_mercury() const { return on_earth() / 0.2408467; }
    /*! @return age in Venus years
     */
    constexpr double on_venus() const { return on_earth() / .61519726; }
    /*! @return age in Mars years
     */
    constexpr double on_mars() const { return on_earth() / 1.8808158; }
    /*! @return age in Jupiter years
     */
    constexpr double on_jupiter() const { return on_earth() / 11.862615; }
    /*! @return age in Jupiter years
     */
    constexpr double on_saturn() const { return on_earth() / 29.447498; }
    /*! @return age in Saturn years
     */
    constexpr double on_uranus() const { return on_earth() / 84.016846; }
    /*! @return age in Neptune years
     */
    constexpr double on_neptune() const { return on_earth() / 164.79132; }

    /*! @}
     */

  private:
    long const secs_;
  };
} // namespace space_age
