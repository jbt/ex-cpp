#ifndef ELLENS_ALIEN_GAME_HPP
#define ELLENS_ALIEN_GAME_HPP

/*! Targets to shoot at in Ellen's "Alien" game.
 */
namespace targets {
/*! An extraterrestrial we hope
 */
class Alien {
    int health = 3;
public:
    /*! X position, as a public _data_ member?
     */
    short x_coordinate;
    /*! Y position, as a public _data_ member?
     */
    short y_coordinate;

    /*! Spawn a new alien at a given location
     * @param x position to spawn at
     * @param y position to spawn at
     */
    explicit Alien(short x, short y);

    /*! @return the alien's current HP
     */
    int get_health() const;
    /*! @return get_health() > 0
     */
    bool is_alive() const;

    /*! Remove 1 HP
     *  @return is_alive() post-hit
     */
    bool hit();
    /*! Move the alien to a new location
     * @param x_new X coordinate of the new location
     * @param y_new Y coordinate of the new location
     * @return true
     */
    bool teleport(short x_new,short y_new);
    /*! @return whether *this is at the same location as another
     * @param another an alien which may be co-located
     */
    bool collision_detection(Alien const& another) const;
};
}

#endif // ELLENS_ALIEN_GAME_HPP
