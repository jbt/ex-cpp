#include "ellens_alien_game.hpp"

#include <utility>

using A = targets::Alien;

A::Alien(short x, short y)
: x_coordinate{x}
, y_coordinate{y}
{
}

auto A::get_health() const -> int
{
  return health;
}
auto A::is_alive() const -> bool
{
  return health;
}

auto A::hit() -> bool
{
  if (health) {
    --health;
    return true;
  }
  return false;
}
auto A::teleport(short new_x, short new_y) -> bool
{
  x_coordinate = new_x;
  y_coordinate = new_y;
  return true;
}
auto A::collision_detection(A const &o) const -> bool
{
  // clang-format off
    return std::make_pair(  x_coordinate,   y_coordinate)
        == std::make_pair(o.x_coordinate, o.y_coordinate)
        ;
}
