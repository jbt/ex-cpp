#include <array>

/*! interest_rate returns the interest rate for the provided balance.
 */
auto interest_rate(double balance) -> double
{
  using B = std::array<double, 2>;
  for (auto [cutoff, rate] : {B{0., 3.213}, B{1e3, 0.5}, B{5e3, 1.621}}) {
    if (balance < cutoff) {
      return rate;
    }
  }
  return 2.475;
}

/*! yearly_interest calculates the yearly interest for the provided balance.
 */
auto yearly_interest(double balance) -> double
{
  return interest_rate(balance) * balance / 100.;
}

/*! annual_balance_update calculates the annual balance update, taking into
 *  account the interest rate.
 */
auto annual_balance_update(double balance) -> double
{
  return balance + yearly_interest(balance);
}

/*! years_until_desired_balance calculates the minimum number of years required
 *  to reach the desired balance.
 */
auto years_until_desired_balance(double balance, double target_balance) -> int
{
  if (balance <= 0 && target_balance > balance) {
    return -1;
  }
  for (auto y = 0; true; ++y) {
    if (balance >= target_balance) {
      return y;
    }
    balance = annual_balance_update(balance);
  }
}
