#include "simple_linked_list.h"

#include <stdexcept>

using List = simple_linked_list::List;

auto List::size() const -> std::size_t
{
  return current_size;
}

void List::push(int entry)
{
  auto p = new Element{entry};
  p->next = head;
  head = p;
  if (!tail) {
    tail = head;
  }
  ++current_size;
}

auto List::pop() -> int
{
  auto p = head;
  auto result = head->data;
  head = head->next;
  if (!head) {
    tail = nullptr;
  }
  delete p;
  --current_size;
  return result;
}

void List::reverse()
{
  auto t = tail;
  tail = rev(head);
  head = t;
}

List::~List()
{
  for (auto p = head; p;) {
    auto n = p->next;
    delete p;
    p = n;
  }
}

auto List::rev(Element* new_tail) -> Element*
{
  if (new_tail && new_tail->next) {
    rev(new_tail->next)->next = new_tail;
    new_tail->next = nullptr;
  }
  return new_tail;
}
