#if !defined(SIMPLE_LINKED_LIST_H)
#define SIMPLE_LINKED_LIST_H

#include <cstddef>

namespace simple_linked_list {

  /*! old-fashioned forward list
   */
  class List {
  public:
    List() = default;
    ~List();

    // Moving and copying is not needed to solve the exercise.
    // If you want to change these, make sure to correctly
    // free / move / copy the allocated resources.
    List(List const &) = delete;
    List &operator=(List const &) = delete;
    List(List &&) = delete;
    List &operator=(List &&) = delete;

    /*! @return number of contained elements
     */
    std::size_t size() const;

    /*! Add new element to the front
     * @param entry the data to be added
     */
    void push(int entry);

    /*! Remove the first/front element
     * @return The data that used to be contained
     */
    int pop();

    /*! Reverse the order of the items in the list
     */
    void reverse();

  private:
    struct Element {
      Element(int data)
      : data{data} {};
      int data{};
      Element *next{nullptr};
    };

    Element *head{nullptr};
    Element *tail{nullptr};
    std::size_t current_size{0};

    static Element *rev(Element *);
  };

} // namespace simple_linked_list

#endif
