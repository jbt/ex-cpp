#if !defined(ALL_YOUR_BASE_H)
#define ALL_YOUR_BASE_H

#include <cstdint>

#include <stdexcept>

namespace all_your_base {
  constexpr auto to_native(unsigned in_base, auto const &digit_values)
      -> std::uintmax_t
  {
    if (in_base < 2) {
      throw std::invalid_argument{std::to_string(in_base) +
                                  " is not an acceptable radix"};
    }
    std::uintmax_t result{0UL};
    for (auto digit_value : digit_values) {
      if (digit_value >= in_base) {
        throw std::invalid_argument{std::to_string(digit_value) +
                                    " is not a valid digit in base-" +
                                    std::to_string(in_base)};
      }
      result *= in_base;
      result += digit_value;
    }
    return result;
  };
  template <class DigitValueCont>
  constexpr auto to_base(unsigned out_base, std::uintmax_t value)
      -> DigitValueCont
  {
    if (out_base < 2) {
      throw std::invalid_argument{std::to_string(out_base) +
                                  " is not an acceptable radix"};
    }
    using Digit = DigitValueCont::value_type;
    // Apparent we represent zero by writing nothing?
    //  if (!value) {
    //    return {static_cast<Digit>(0U)};
    //  }
    DigitValueCont result{};
    while (value) {
      auto digit_value = static_cast<Digit>(value % out_base);
      result.insert(result.begin(), digit_value);
      value /= out_base;
    }
    return result;
  };
  template <class DigitValueCont>
  constexpr auto convert(unsigned in_base, DigitValueCont const &digit_values,
                         unsigned out_base) -> DigitValueCont
  {
    auto native = to_native(in_base, digit_values);
    return to_base<DigitValueCont>(out_base, native);
  }
} // namespace all_your_base

#endif // ALL_YOUR_BASE_H
