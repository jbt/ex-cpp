#include "spiral_matrix.h"

// #include <iostream>
#include <ranges>
#include <string_view>
#include <tuple>

namespace {
  class Ring {
    spiral_matrix::Matrix &out;
    std::size_t const in;

    auto coord(unsigned i) -> std::tuple<unsigned, unsigned, std::string_view>
    {
      auto const on = out.size();
      auto n = on - in * 2U;
      if (i < n) {
        return {in + i, in, "Top"};
      }
      i -= n;
      if (!--n) {
        return {0U, 0U, ""};
      }
      auto const back = on - in - 1U;
      if (i < n) {
        return {back, in + i + 1U, "Right"};
      }
      i -= n;
      if (i < n) {
        return {back - i - 1U, back, "Bottom"};
      }
      i -= n;
      if (!--n) {
        return {0U, 0U, ""};
      }
      if (i < n) {
        return {in, back - i - 1U, "Left"};
      }
      return {0U, 0U, ""};
    }

  public:
    Ring(auto &m, auto n)
    : out{m}
    , in{n}
    {
    }
    auto at(unsigned idx) -> std::uint32_t *
    {
      auto [x, y, l] = coord(idx);
      // std::clog << out.size() << ':' << in << '@' << idx << '=' << x << ','
      // << y << l << '\n';
      return l.empty() ? nullptr : out.at(y).data() + x;
    }
  };
} // namespace

auto spiral_matrix::spiral_matrix(std::size_t dim) -> Matrix
{
  std::vector<std::vector<std::uint32_t>> result(dim);
  for (auto &r : result) {
    r.resize(dim);
  }
  auto val = 0U;
  for (auto i = 0U; i < dim / 2U + dim % 2U; ++i) {
    Ring ring{result, i};
    for (auto j = 0U; auto p = ring.at(j); ++j) {
      *p = ++val;
    }
  }
  return result;
}
