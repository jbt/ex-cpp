#if !defined(SPIRAL_MATRIX_H)
#define SPIRAL_MATRIX_H

#include <cstdint>

#include <vector>

namespace spiral_matrix {
  using Matrix = std::vector<std::vector<std::uint32_t>>;
  auto spiral_matrix(std::size_t dim) -> Matrix;
} // namespace spiral_matrix

#endif // SPIRAL_MATRIX_H
