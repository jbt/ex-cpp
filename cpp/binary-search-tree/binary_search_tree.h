#if !defined(BINARY_SEARCH_TREE_H)
#define BINARY_SEARCH_TREE_H

#include <functional>
#include <memory>

namespace binary_search_tree {

  /*! @brief Helper class to iterate through a BST
   */
  template <class Node>
  class node_iterator {
    Node *n_;

  public:
    /*! Iterate over the tree, starting at n
     * @param n
     *   Starting node, or default/nullptr if it's an end iterator (sentinel)
     */
    node_iterator(Node *n = {})
    : n_{n}
    {
    }

    /*! @return whether two iterators are truly equal (same node, same tree)
     *  @param r right-hand side
     */
    auto operator==(node_iterator const &r) const -> bool { return n_ == r.n_; }

    /*! @return whether two iterators are not eactly equal
     *  @param r right-hand side
     */
    auto operator!=(node_iterator const &r) const -> bool
    {
      return !(*this == r);
    }

    /*! @return The data being ultimately pointed at
     */
    auto operator*() const -> Node::value_t & { return n_->data(); }

    /*! Iterate
     * @return *this
     */
    auto operator++() -> node_iterator &
    {
      n_ = n_->next();
      return *this;
    }
  };

  /*! @brief Node of an unbalanced BST
   */
  template <class Value>
  class binary_tree {
  public:
    using value_t = Value; ///< The type of a datum contained in the tree
    using Node = binary_tree<Value>;      ///< This node's type
    using iterator = node_iterator<Node>; ///< (mutation) iterator returned by
                                          ///< begin() and end()

    /*! @brief node ctor
     *  @param v
     *    the value to be contained in the node
     *  @param p
     *    the parent of this node. Default (nullptr) means it's the root of the
     * tree
     */
    explicit binary_tree(Value const &v, Node *p = nullptr)
    : value_{v}
    , parent_{p}
    {
    }

    /*! @brief insert a value as a descendant node in an appropriate (sorted)
     * place
     *  @param v
     *    the value to be inserted
     */
    void insert(Value const &v)
    {
      auto &child = select(v);
      if (child) {
        child->insert(v);
      } else {
        child = std::make_unique<Node>(v, this);
      }
    }

    /*! Data accessor
     *  @return the data contained in this node
     */
    auto data() -> Value & { return value_; }

    /*! Implementing some basic container methods
     * @return Iterator pointing to minimum node of this tree
     */
    auto begin() -> iterator { return {min()}; }

    /*! Implementing some basic container methods
     * @return Sentinel end iterator
     */
    auto end() -> iterator { return {}; }

    /*! @return Node containing the minimum value of this (sub-)tree
     */
    auto min() -> Node *
    {
      return left_ ? left_->min() : this;
    } ///< Node with the least value of this tree

    /*! @return Node containing the maximum value of this (sub-)tree
     */
    auto max() -> Node *
    {
      return rite_ ? rite_->max() : this;
    } ///< Node with the greatest value of this tree

    /*! @return Node containing the next-highest value of this (sub-)tree,
     *    or nullptr if \code this == max() \endcode
     */
    auto next() -> Node *
    {
      if (auto r = right()) {
        return r->min();
      }
      auto x = this;
      while (x->parent_ && x->parent_->right() == x) {
        x = x->parent_;
      }
      return x->parent_;
    }

    /*! These really souldn't be public methods.
     * @return left child
     */
    auto left() const { return left_.get(); }

    /*! These really souldn't be public methods.
     * @return right child
     */
    auto right() const { return rite_.get(); }

  private:
    Value value_;
    std::unique_ptr<Node> left_;
    std::unique_ptr<Node> rite_;
    Node *parent_;

    auto select(Value const &v) -> std::unique_ptr<binary_tree> &
    {
      return v <= value_ ? left_ : rite_;
    }
    auto senior_child() -> Node * { return rite_ ? rite_.get() : left_.get(); }
  };
} // namespace binary_search_tree

#endif // BINARY_SEARCH_TREE_H
