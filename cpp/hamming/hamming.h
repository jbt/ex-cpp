#pragma once

#include <numeric>
#include <stdexcept>
#include <string_view>

namespace hamming {
  constexpr long compute(std::string_view a, std::string_view b)
  {
    if (a.size() != b.size()) {
      throw std::domain_error("To calculate a hamming distance, the strings "
                              "must be of the same length.");
    }
    // clang-format off
    return std::inner_product(
          a.begin(), a.end()
        , b.begin()
        , 0L
        , std::plus{}
        , std::not_equal_to{}
        );
    // clang-format on
  }
} // namespace hamming

static_assert(3L == hamming::compute("abcdef", "abc123"), "Basic usage");
