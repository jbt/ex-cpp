#include "crypto_square.h"

#include <algorithm>
#include <ranges>

namespace rg = std::ranges;
namespace vw = rg::views;

namespace {
  constexpr auto sqrt_rndup(std::size_t area) -> std::size_t
  {
    auto result = 0UL;
    auto sq = 0UL;
    for (auto odd = 1UL; sq < area; odd += 2) {
      sq += odd;
      ++result;
    }
    return result;
  }
  auto normalize(std::string_view in)
  {
    auto letters = in | vw::filter(&::isalnum) | vw::transform(&::tolower);
    return std::string(letters.begin(), letters.end());
  }
} // namespace

using Ciph = crypto_square::cipher;

Ciph::cipher(std::string_view plain)
: normalized_{normalize(plain)}
, width_{sqrt_rndup(normalized_.size())}
{
}
auto Ciph::height() const -> std::size_t
{
  auto maybe = width() - 1UL;
  if (width() * maybe < letter_count()) {
    return width();
  }
  return maybe;
}
auto Ciph::normalized_cipher_text() const -> std::string
{
  if (empty()) {
    return {};
  }
  auto separators = width() - 1U;
  std::string obfuscated(width() * height() + separators, ' ');
  auto o = obfuscated.begin();
  for (auto y = 0U; y < width(); ++y) {
    for (auto x = 0U; x < height(); ++x) {
      *(o++) = at(x, y);
    }
    ++o; // test cases want spaces between rows
  }
  return obfuscated;
}
auto Ciph::at(unsigned x, unsigned y) const -> char
{
  auto i = x * width() + y;
  return i < letter_count() ? normalized_.at(i) : ' ';
}
auto Ciph::empty() const -> bool
{
  return !letter_count();
}
auto Ciph::letter_count() const -> std::size_t
{
  return normalized_.size();
}

static_assert(sqrt_rndup(0) == 0);
static_assert(sqrt_rndup(1) == 1);
static_assert(sqrt_rndup(2) == 2);
static_assert(sqrt_rndup(3) == 2);
static_assert(sqrt_rndup(4) == 2);
static_assert(sqrt_rndup(5) == 3);
static_assert(sqrt_rndup(6) == 3);
static_assert(sqrt_rndup(7) == 3);
static_assert(sqrt_rndup(8) == 3);
static_assert(sqrt_rndup(9) == 3);
static_assert(sqrt_rndup(10) == 4);
