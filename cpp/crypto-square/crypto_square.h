#if !defined(CRYPTO_SQUARE_H)
#define CRYPTO_SQUARE_H

#include <string>
#include <string_view>

/*! The Crypto Square exercize
 * https://exercism.org/tracks/cpp/exercises/crypto-square
 */
namespace crypto_square {
  /*! A message
   */
  class cipher {
    std::string const normalized_;
    std::size_t const width_;

    auto letter_count() const -> std::size_t;
    auto empty() const -> bool;

  public:
    /*! Construct
     * @param plain the plain-text message to be munged
     */
    explicit cipher(std::string_view plain);
    /*! @return the result
     */
    auto normalized_cipher_text() const -> std::string;
    /*! @return The width of the crypto rectangle
     */
    auto width() const { return width_; }
    /*! @return The height of the crypto rectangle
     */
    auto height() const -> std::size_t;
    /*! @return A letter from the rectangle
     *  @param x The column index of the letter being fetched
     *  @param y The column row   of the letter being fetched
     */
    auto at(unsigned x, unsigned y) const -> char;
  };
} // namespace crypto_square

#endif // CRYPTO_SQUARE_H
