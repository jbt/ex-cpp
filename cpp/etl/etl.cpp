#include "etl.h"

#include <iostream>

using LS = etl::LetterScoring;
auto etl::transform(std::map<int, std::vector<char>> const &old) -> LS
{
  LS newer;
  for (auto &[score, letters] : old) {
    for (auto letter : letters) {
      LS::at(newer, letter) = score;
    }
  }
  return newer;
}

LS::LetterScoring()
{
  map_.fill(0);
}
auto LS::operator==(std::map<char, int> const &rhs) const -> bool
{
  auto d = 'a';
  for (auto [c, i] : rhs) {
    while (d < c) {
      if (at(*this, d++)) {
        return false;
      }
    }
    if (at(*this, c) != i) {
      return false;
    }
    d = c + 1;
  }
  while (++d <= 'z') {
    if (at(*this, d)) {
      return false;
    }
  }
  return true;
}

auto etl::operator<<(std::ostream &s, LS const &ls) -> std::ostream &
{
  s << '[';
  for (auto c = 'a'; c <= 'z'; ++c) {
    if (auto i = LS::at(ls, c)) {
      s << c << '=' << i << ';';
    }
  }
  return s << ']';
}
