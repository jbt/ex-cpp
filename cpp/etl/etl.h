#if !defined(ETL_H)
#define ETL_H

#include <cassert>
#include <cctype>

#include <array>
#include <iosfwd>
#include <map>
#include <vector>

/*! ETL exercize
 */
namespace etl {
  /*! Letter-scoring transformation
   */
  class LetterScoring {
    std::array<short, 26> map_;

  public:
    LetterScoring();
    /*! Check whether two representations are equivalent
     *  @param rhs the right-hand side of the operator
     *  @return true iff they carry exactly the same data
     */
    auto operator==(std::map<char, int> const &rhs) const -> bool;
    /*! @param me What should've been a deduced this
     *  @param c  letter getting mapped from
     *  @return a reference to the integer corresponding to c
     */
    template <class Self>
    static constexpr auto at(Self &&me, char c) -> auto &
    {
      assert(std::isalpha(c));
      return me.map_[std::tolower(c) - 'a'];
    }
  };

  /*! Transform old format of letter scoring to new.
   *  @param old The data to transform
   *  @return The transformed data
   */
  auto transform(std::map<int, std::vector<char>> const &old) -> LetterScoring;

  /*! Debug print some letter scores
   *  @param s an output stream to write to
   *  @param ls The letter scores to write
   *  @return s, post-write
   */
  std::ostream &operator<<(std::ostream &s, LetterScoring const &ls);
} // namespace etl

#endif // ETL_H
