#include "isogram.h"

#include <cctype>

#include <bitset>
#include <optional>

namespace {
  auto idx(char c) -> std::optional<std::size_t>
  {
    if (std::isupper(c)) {
      return c - 'A';
    }
    if (std::islower(c)) {
      return c - 'a';
    }
    return {};
  }
} // namespace
auto isogram::is_isogram(std::string_view word) -> bool
{
  std::bitset<26> seen;
  for (auto c : word) {
    if (auto i = idx(c)) {
      if (seen.test(*i)) {
        return false;
      }
      seen.set(*i);
    }
  }
  return true;
}
