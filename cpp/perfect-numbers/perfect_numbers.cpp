#include "perfect_numbers.h"
namespace pn = perfect_numbers;
using C = pn::classification;

// clang-format off
static_assert(C::deficient== pn::classify(     2));
static_assert(C::deficient== pn::classify(     3));
static_assert(C::deficient== pn::classify(     4));
static_assert(C::deficient== pn::classify(     5));
static_assert(C::perfect  == pn::classify(     6));
static_assert(C::deficient== pn::classify(     7));
static_assert(C::deficient== pn::classify(     8));
static_assert(C::deficient== pn::classify(     9));
static_assert(C::deficient== pn::classify(    10));
static_assert(C::deficient== pn::classify(    11));
static_assert(C::abundant == pn::classify(    12));
static_assert(C::perfect  == pn::classify(    28));
static_assert(C::abundant == pn::classify(    30));
static_assert(C::deficient== pn::classify(    32));
static_assert(C::deficient== pn::classify(   932));
static_assert(C::deficient== pn::classify(  9932));
static_assert(C::deficient== pn::classify( 99992));
static_assert(C::deficient== pn::classify(509999));
static_assert(C::deficient== pn::classify(519999));
static_assert(C::deficient== pn::classify(520999));
static_assert(C::deficient== pn::classify(525999));
static_assert(C::deficient== pn::classify(527999));
static_assert(C::deficient== pn::classify(528999));
static_assert(C::deficient== pn::classify(529099));
static_assert(C::deficient== pn::classify(529699));
// static_assert(C::deficient== pn::classify(529999));

static_assert(1 + 2 == pn::aliquot(4));
static_assert(1 == pn::aliquot(5));
static_assert(1 + 2 + 3 + 4 + 6 == pn::aliquot(12));
