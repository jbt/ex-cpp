project(
  perfect-numbers
  LANGUAGES CXX
)

find_package(Catch2 REQUIRED)
find_package(Boost REQUIRED)
find_package(Threads REQUIRED)

file(
  GLOB
  srcs
  *.h
  *.cpp
)
file(
  GLOB
  test_srcs
  *_test.cpp
)
list(REMOVE_ITEM srcs ${test_srcs})

add_library(
  perfect-numbers_exercize
  ${srcs}
)
target_compile_features(
  perfect-numbers_exercize
  PUBLIC
    cxx_std_23
)
target_compile_options(
  perfect-numbers_exercize
  PUBLIC
    -fsanitize=undefined
    -O0 -g3 -ggdb3 -fno-inline
)
target_compile_options(
  perfect-numbers_exercize
  PRIVATE
    -Wall
    -Wextra
    -Wpedantic
    -Werror
)
target_link_libraries(
  perfect-numbers_exercize
  PUBLIC
    Boost::headers
    Threads::Threads
    ubsan
)

include(clang-tools)
clang_tooling(perfect-numbers_exercize)

foreach(t few all slow)
  add_executable(
    perfect-numbers_${t}_runner
    ${test_srcs}
    ../lasagna/test/tests-main.cpp
  )
  target_link_libraries(
    perfect-numbers_${t}_runner
    PUBLIC
      perfect-numbers_exercize
      # Boost::headers
      Catch2::Catch2
  )
  add_custom_command(
    OUTPUT ${t}.cert
    COMMAND timeout ${MAX_TEST_TIME} $<TARGET_FILE:perfect-numbers_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.cert
    DEPENDS perfect-numbers_${t}_runner
    COMMENT "perfect-numbers_${t} passed"
  )
  add_custom_command(
    OUTPUT ${t}.memcheck.cert
    COMMAND timeout ${MAX_VALGRIND_TEST_TIME} valgrind --exit-on-first-error=yes --error-exitcode=99 --leak-check=full --track-origins=yes --quiet $<TARGET_FILE:perfect-numbers_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.memcheck.cert
    DEPENDS perfect-numbers_${t}_runner
    COMMENT "perfect-numbers_${t} passed under valgrind"
  )
endforeach()
target_compile_definitions(
  perfect-numbers_all_runner
  PUBLIC
    EXERCISM_RUN_ALL_TESTS=1
)
target_compile_definitions(
  perfect-numbers_slow_runner
  PUBLIC
    EXERCISM_INCLUDE_BENCHMARK=1
    CATCH_CONFIG_ENABLE_BENCHMARKING=1
)

foreach(t few all)
  add_custom_target(
    check_perfect-numbers_${t}
    ALL
    DEPENDS ${t}.cert ${t}.memcheck.cert
    COMMENT "perfect-numbers ${t} tests."
  )
endforeach()
add_custom_target(
  check_perfect-numbers_slow
  ALL
  DEPENDS slow.cert
  COMMENT "perfect-numbers Slow/benchmark tests."
)
add_dependencies(check_perfect-numbers_all  check_perfect-numbers_few)
add_dependencies(check_perfect-numbers_slow check_perfect-numbers_all)
