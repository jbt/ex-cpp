#pragma once

#include <cstdint>

#include <stdexcept>

/*! Perfect Numbers exercize
 */
namespace perfect_numbers {
  /*! Categorization a number could have as considered here
   */
  enum class classification {
    perfect,
    abundant,
    deficient
  };

  /*! @return the aliquot sum of a number
   *  @param n The number to get the sum of
   *  @pre   n > 1
   */
  constexpr std::intmax_t aliquot(std::intmax_t n) {
    auto rv = 1LL;
    auto up = n;
    for (auto i = 2LL; i < up; ++i) {
      if (n % i == 0) {
        rv += i;
        if ((up = n / i) > i) {
          rv += up;
        }
      }
    }
    return rv;
  }

  /*! @return the classification of a given number
   *  @param n the number to classify
   */
  constexpr classification classify(std::intmax_t n) {
    if (n < 1) {
      throw std::domain_error{std::to_string(n)};
    } else if (n == 1) {
      return classification::deficient;
    }
    auto a = aliquot(n);
    if (n == a) {
      return classification::perfect;
    } else if (n < a) {
      return classification::abundant;
    } else {
      return classification::deficient;
    }
  }
}  // namespace perfect_numbers
