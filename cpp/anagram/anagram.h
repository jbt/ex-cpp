#if !defined(ANAGRAM_H)
#define ANAGRAM_H

#include <cstdint>

#include <string>
#include <string_view>
#include <vector>

namespace anagram {

  /*! Finds anagrams of given word
   */
  class anagram {
    std::uint64_t hash_;
    std::string word_;

    auto hash(std::string_view) -> std::uint64_t;

  public:
    /*! Construct matcher
     * @param word
     *   What to look for anagrams of.
     */
    anagram(std::string_view word);

    /*! Filter down a list of words to just the matches
     * @param rng
     *   The words to check.
     * @return Just the matches
     */
    auto matches(std::initializer_list<std::string_view> rng)
        -> std::vector<std::string>;
  };
} // namespace anagram

#endif // ANAGRAM_H
