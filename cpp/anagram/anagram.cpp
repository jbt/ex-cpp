#include "anagram.h"

#include <algorithm>

namespace {
  auto char_shift(char c) -> std::uint64_t
  {
    if (std::islower(c)) {
      return c - 'a';
    } else if (std::isupper(c)) {
      return c - 'A';
    } else {
      return 26;
    }
  }
  auto char_iequal(char a, char b) -> bool
  {
    return a == std::tolower(b);
  }
} // namespace

anagram::anagram::anagram(std::string_view word)
: hash_{hash(word)}
{
  word_.resize(word.size());
  std::transform(word.begin(), word.end(), word_.begin(), ::tolower);
}

auto anagram::anagram::hash(std::string_view word) -> std::uint64_t
{
  std::uint64_t result = 0UL;
  for (auto c : word) {
    result += 1ULL << (char_shift(c) * 2);
  }
  return result;
}

auto anagram::anagram::matches(std::initializer_list<std::string_view> rng)
    -> std::vector<std::string>
{
  std::vector<std::string> rv;
  for (auto s : rng) {
    if (std::equal(word_.begin(), word_.end(), s.begin(), char_iequal)) {
    } else if (hash(s) == hash_) {
      rv.emplace_back(s);
    }
  }
  return rv;
}
