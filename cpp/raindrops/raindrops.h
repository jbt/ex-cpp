#pragma once

#include <array>
#include <string_view>

namespace raindrops {
  /*! Constexpr text of PlingPlangPlong etc.
   */
  class dropped_string {
  public:
    /*! Construct from the number
     *  @param number The number which drops
     */
    constexpr explicit dropped_string(int number)
    {
      using condition_t = std::pair<int, std::string_view>;
      auto conditions = {condition_t{3, "Pling"}, condition_t{5, "Plang"},
                         condition_t{7, "Plong"}};
      for (auto cond : conditions) {
        if (number % cond.first == 0) {
          append(cond.second);
        }
      }
      if (size_ == 0L) {
        append_base_10(number);
      }
    }

    /*! @return The text itself
     */
    constexpr operator std::string_view() const
    {
      return {buffer_.data(), static_cast<std::size_t>(size_)};
    }
    /*! @param sv A raw string view
     *  @return whether *this is equivalent
     */
    constexpr bool operator==(std::string_view sv) const
    {
      return static_cast<std::string_view>(*this) == sv;
    }

  private:
    static constexpr long capacity = sizeof("PlingPlangPlong");
    using buffer_t = std::array<char, capacity>;

    buffer_t buffer_ = {};
    long size_ = 0L;

    constexpr void append(std::string_view text)
    {
      for (auto i = 0U; i < text.size(); ++i) {
        buffer_[size_++] = text[i];
      }
    }
    constexpr void append_base_10(int number)
    {
      auto higher = number / 10;
      if (higher) {
        append_base_10(higher);
      }
      buffer_[size_++] = '0' + number % 10;
    }
  };

  constexpr dropped_string convert(int number)
  {
    return dropped_string{number};
  }

  std::ostream& operator<<(std::ostream&, dropped_string const&);
} // namespace raindrops
