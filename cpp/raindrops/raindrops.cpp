#include "raindrops.h"

auto raindrops::operator<<(std::ostream& os, dropped_string const& ds)
    -> std::ostream&
{
  return os << static_cast<std::string_view>(ds);
}

// And now the static test suite

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

namespace one_yields_itself {
  static_assert(raindrops::convert(1) == "1");
}

namespace three_yields_pling {
  static_assert(raindrops::convert(3) == "Pling");
}

namespace five_yields_plang {
  static_assert(raindrops::convert(5) == "Plang");
}

namespace seven_yields_plong {
  static_assert(raindrops::convert(7) == "Plong");
}

namespace six_yields_pling {
  static_assert(raindrops::convert(6) == "Pling");
}

namespace nine_yields_pling {
  static_assert(raindrops::convert(9) == "Pling");
}

namespace ten_yields_plang {
  static_assert(raindrops::convert(10) == "Plang");
}

namespace fourteen_yields_plong {
  static_assert(raindrops::convert(14) == "Plong");
}

namespace fifteen_yields_plingplang {
  static_assert(raindrops::convert(15) == "PlingPlang");
}

namespace twenty_one_yields_plingplong {
  static_assert(raindrops::convert(21) == "PlingPlong");
}

namespace twenty_five_yields_plang {
  static_assert(raindrops::convert(25) == "Plang");
}

namespace thirty_five_yields_plangplong {
  static_assert(raindrops::convert(35) == "PlangPlong");
}

namespace forty_nine_yields_plong {
  static_assert(raindrops::convert(49) == "Plong");
}

namespace fifty_two_yields_itself {
  static_assert(raindrops::convert(52) == "52");
}

namespace one_hundred_five_yields_plingplangplong {
  static_assert(raindrops::convert(105) == "PlingPlangPlong");
}

namespace big_prime_yields_itself {
  static_assert(raindrops::convert(12121) == "12121");
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
