#if !defined(ARMSTRONG_NUMBERS_H)
#define ARMSTRONG_NUMBERS_H

namespace armstrong_numbers {
  constexpr unsigned decimal_digit_count(unsigned n)
  {
    unsigned result = 1;
    for (; (n /= 10); ++result) {
    }
    return result;
  }
  constexpr unsigned raise(unsigned base, unsigned power)
  {
    auto result = 1U;
    for (; power; --power) {
      result *= base;
    }
    return result;
  }
  constexpr bool is_armstrong_number(unsigned n)
  {
    auto exp = decimal_digit_count(n);
    unsigned sum = 0U;
    for (auto i = n; i; i /= 10) {
      auto digit = i % 10;
      sum += raise(digit, exp);
    }
    return sum == n;
  }
} // namespace armstrong_numbers

#endif // ARMSTRONG_NUMBERS_H
