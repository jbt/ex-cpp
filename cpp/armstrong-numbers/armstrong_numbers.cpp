#include "armstrong_numbers.h"

namespace zero_is_an_armstrong_number {
  static_assert(0 % 10 == 0);
  static_assert(armstrong_numbers::raise(0, 1U) == 0U);
  static_assert(armstrong_numbers::decimal_digit_count(0) == 1U);
  static_assert(armstrong_numbers::is_armstrong_number(0));
} // namespace zero_is_an_armstrong_number

namespace single_digit_numbers_are_armstrong_numbers {
  static_assert(armstrong_numbers::is_armstrong_number(5));
}

namespace there_are_no_2_digit_armstrong_numbers {
  static_assert(!armstrong_numbers::is_armstrong_number(10));
}

namespace three_digit_number_that_is_an_armstrong_number {
  static_assert(armstrong_numbers::is_armstrong_number(153));
}

namespace three_digit_number_that_is_not_an_armstrong_number {
  static_assert(!armstrong_numbers::is_armstrong_number(100));
}

namespace four_digit_number_that_is_an_armstrong_number {
  static_assert(armstrong_numbers::is_armstrong_number(9474));
}

namespace four_digit_number_that_is_not_an_armstrong_number {
  static_assert(!armstrong_numbers::is_armstrong_number(9475));
}

namespace seven_digit_number_that_is_an_armstrong_number {
  static_assert(armstrong_numbers::is_armstrong_number(9926315));
}

namespace seven_digit_number_that_is_not_an_armstrong_number {
  static_assert(!armstrong_numbers::is_armstrong_number(9926314));
}
