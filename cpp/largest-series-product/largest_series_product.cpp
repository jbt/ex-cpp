#include "largest_series_product.h"

#include <numeric>
#include <stdexcept>

namespace {
  auto add_digit(std::uintmax_t acc, char digit) -> std::uintmax_t
  {
    if (digit < '0' || digit > '9') {
      throw std::domain_error{"Not a digit."};
    }
    return acc * (digit - '0');
  }
  auto sum(auto b, auto e) -> std::uintmax_t
  {
    return std::accumulate(b, e, 1ULL, add_digit);
  }
} // namespace
auto largest_series_product::largest_product(std::string_view digits,
                                             unsigned series_len)
    -> std::uintmax_t
{
  if (series_len > digits.size()) {
    throw std::domain_error{std::string{digits} + "," +
                            std::to_string(series_len)};
  }
  auto cur = sum(digits.begin(), digits.begin() + series_len);
  auto rv = cur;
  unsigned j = series_len;
  for (auto i = 0U; j < digits.size(); ++i, ++j) {
    if (digits[i] > '0') {
      cur /= (digits[i] - '0');
      cur = add_digit(cur, digits[j]);
    } else {
      cur = sum(digits.begin() + i + 1, digits.begin() + j + 1);
    }
    rv = std::max(cur, rv);
  }
  return rv;
}
