#if !defined(LARGEST_SERIES_PRODUCT_H)
#define LARGEST_SERIES_PRODUCT_H

#include <cstdint>
#include <string_view>

/*! Largest Series Product exercize
 */
namespace largest_series_product {
  /*! @return the largest product from sub-series of a given length
   *  @param  digits     The digits to use sub-series from
   *  @param  series_len Length of sub-series to consider
   */
  std::uintmax_t largest_product(std::string_view digits, unsigned series_len);
} // namespace largest_series_product

#endif // LARGEST_SERIES_PRODUCT_H
