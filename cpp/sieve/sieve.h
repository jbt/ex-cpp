#pragma once

#include <vector>

/*! Sieve (of Eratosthenes) exercize
 */
namespace sieve {
  /*!  @return Ordered list of prime numbers <= up_to
   *   @param up_to The limit to the prime search
   */
  auto primes(std::size_t up_to) -> std::vector<int>;
} // namespace sieve
