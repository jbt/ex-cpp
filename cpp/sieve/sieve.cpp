#include "sieve.h"

auto sieve::primes(std::size_t up_to) -> std::vector<int>
{
  if (up_to < 2)
    return {};
  std::vector<int> result;
  std::vector<bool> sieve(up_to + 1UL, false);
  for (auto prime = 2UL; prime <= up_to; ++prime) {
    if (sieve.at(prime)) {
      // not prime.
      continue;
    }
    result.push_back(prime);
    for (auto multiple = 2 * prime; multiple <= up_to; multiple += prime) {
      sieve.at(multiple) = true;
    }
  }
  return result;
}
