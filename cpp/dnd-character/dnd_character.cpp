#include "dnd_character.h"

#include <algorithm>
#include <ranges>

namespace dd = dnd_character;
namespace rg = std::ranges;
namespace vw = rg::views;

namespace {
  auto diceforbadlazies() -> dd::Die&
  {
    static dd::Die d;
    return d;
  }
} // namespace

auto dd::ability() -> I
{
  return ability(diceforbadlazies());
}
auto dd::ability(Die& d) -> I
{
  std::array<I, 4> rolls{};
  rg::generate(rolls, [&]() { return d.roll(); });
  auto discard = rg::min_element(rolls);
  std::iter_swap(discard, rolls.begin());
  auto result = rg::fold_left(rolls | vw::drop(1), 0, std::plus{});
  return static_cast<I>(result);
}

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

dd::Die::Die()
: prng_{std::random_device{}()}
, dist_(1, 6)
{
}
auto dd::Die::roll() -> I
{
  return dist_(prng_);
}
dd::Character::Character()
: Character(diceforbadlazies())
{
}
dd::Character::Character(Die& d)
: strength{ability(d)}
, dexterity{ability(d)}
, constitution{ability(d)}
, intelligence{ability(d)}
, wisdom{ability(d)}
, charisma{ability(d)} // Your character's initial hitpoints are 10 + your
                       // character's constitution modifier.
, hitpoints{static_cast<I>(modifier(constitution) + 10)}
{
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
