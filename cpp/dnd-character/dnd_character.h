#pragma once

#include <cstdint>
#include <random>

namespace dnd_character {

  /*! An integer.
   *  @details All numbers in this namespace are integers
   *    well within the range representable by i8
   */
  using I = std::int8_t;

  /*! @return A constitution modifier
   *  @param score A character's constitution
   *  @details "You find your character's constitution modifier by
   *      subtracting 10 from your character's constitution,
   *      divide by 2 and round down."
   */
  constexpr auto modifier(I score) -> I
  {
    auto down = score < 10 ? score % 2 : 0;
    return (score - 10) / 2 - down;
  }

  /*! Specifically a d6
   */
  class Die {
    std::default_random_engine prng_;
    std::uniform_int_distribution<I> dist_;

  public:
    /*! Default constructor
     */
    Die();

    /*! @return 1d6 result
     */
    auto roll() -> I;
  };

  /*! @return An ability score, freshly rolled
   *  @param die a die one may roll multiple times
   *  @details 4d6 with advantage
   */
  auto ability(Die& die) -> I;

  /*! Very bad convenience wrapper for ability(Die&)
   *  @return the rolled ability score
   *  @note In order to support this sort of use case without
   *    wasting too much physical entropy,
   *    I'm calling for entropy at startup, which sucks
   */
  auto ability() -> I;

  /*! A character sheet
   */
  struct Character {
    /*! Roll up the character
     *  @param die A d6 to use
     */
    Character(Die& die);

    /*! Bad convenience wrapper for the other ctor
     */
    Character();

    I strength;     ///< STR
    I dexterity;    ///< DEX
    I constitution; ///< CON
    I intelligence; ///< INT
    I wisdom;       ///< WIS
    I charisma;     ///< CHR
    I hitpoints;    ///< HP
  };

} // namespace dnd_character
