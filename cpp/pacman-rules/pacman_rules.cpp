/// eat_ghost returns a boolean value if Pac-Man is able to eat the ghost.
/// @return true only if Pac-Man has a power pellet active
///   and is touching a ghost.
auto can_eat_ghost(bool power_pellet_active, bool touching_ghost) -> bool
{
  return power_pellet_active && touching_ghost;
}

#if defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

/// scored returns a boolean value if Pac-Man scored.
/// @return true if Pac-Man is touching a power pellet or a dot.
auto scored(bool touching_power_pellet, bool touching_dot) -> bool
{
  return touching_power_pellet || touching_dot;
}

/// lost returns a boolean value if Pac-Man loses.
/// @return true if Pac-Man is touching a ghost and
///   does not have a power pellet active.
auto lost(bool power_pellet_active, bool touching_ghost) -> bool
{
  return touching_ghost && !power_pellet_active;
}

/// won returns a boolean value if Pac-Man wins.
/// @return true if Pac-Man
///   has eaten all of the dots and has not lost
auto won(bool has_eaten_all_dots, bool power_pellet_active, bool touching_ghost)
    -> bool
{
  return has_eaten_all_dots && !lost(power_pellet_active, touching_ghost);
}
