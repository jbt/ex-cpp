#if !defined(LUHN_H)
#define LUHN_H

#include <cstdint>
#include <string_view>

namespace luhn {
  constexpr bool is_digit(char c)
  {
    return c >= '0' && c <= '9';
  }
  constexpr std::uint8_t to_num(char c)
  {
    return c - '0';
  }
  constexpr bool valid(std::string_view input)
  {
    std::uint8_t expected = 11;
    bool doubling = true;
    unsigned sum = 0;
    bool at_least_one = false;
    for (auto c = input.rbegin(); c != input.rend(); ++c) {
      if (*c == ' ') {
        continue;
      }
      if (is_digit(*c)) {
        auto i = to_num(*c);
        if (expected == 11) {
          expected = i;
          continue;
        }
        at_least_one = true;
        if (doubling) {
          i *= 2;
          if (i > 9) {
            i -= 9;
          }
          doubling = false;
        } else {
          doubling = true;
        }
        sum += i;
      } else {
        return false;
      }
    }
    if (!at_least_one) {
      return false;
    }
    auto actual = (10 - sum % 10) % 10;
    return actual == expected;
  }
} // namespace luhn

#endif // LUHN_H
