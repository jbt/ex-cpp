#include "food_chain.h"

#include <array>
#include <sstream>

namespace fc = food_chain;

namespace {
  void add_verse(std::ostream& str, short num);
}

auto fc::verse(short num) -> std::string
{
  std::ostringstream oss;
  add_verse(oss, num);
  return oss.str();
}
auto fc::verses(short start, short end) -> std::string
{
  std::ostringstream oss;
  for (auto num = start; num <= end; ++num) {
    add_verse(oss, num);
    oss.put('\n');
  }
  return oss.str();
}
auto fc::sing() -> std::string
{
  return verses(1, 8);
}

namespace {
  using namespace std::literals;
  // clang-format off
    constexpr auto animal = std::array{
      "fly"sv
    , "spider"sv
    , "bird"sv
    , "cat"sv
    , "dog"sv
    , "goat"sv
    , "cow"sv
    , "horse"sv
    };
    constexpr auto comment = std::array{
      ""sv
    , "It wriggled and jiggled and tickled inside her.\n"sv
    , "How absurd to swallow a bird!\n"sv
    , "Imagine that, to swallow a cat!\n"sv
    , "What a hog, to swallow a dog!\n"sv
    , "Just opened her throat and swallowed a goat!\n"sv
    , "I don't know how she swallowed a cow!\n"sv
    };
  // clang-format on
  void add_verse(std::ostream& str, short num)
  {
    str << "I know an old lady who swallowed a "sv << animal.at(num - 1)
        << ".\n"sv;
    if (num == 8) {
      str << "She's dead, of course!\n";
      return;
    }
    str << comment.at(num - 1);
    while (--num) {
      str << "She swallowed the " << animal.at(num) << " to catch the "
          << animal.at(num - 1);
      if (num == 2) {
        str << " that wriggled and jiggled and tickled inside her";
      }
      str << ".\n";
    }
    str << "I don't know why she swallowed the fly. Perhaps she'll die.\n"sv;
  }
} // namespace
