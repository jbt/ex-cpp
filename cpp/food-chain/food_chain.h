#if !defined(FOOD_CHAIN_H)
#define FOOD_CHAIN_H

#include <string>

namespace food_chain {
  /*! @return The text of one verse of the song
   *  @param num The verse number. 1-based.
   *  @throw std::out_of_bounds If there is no verse of the given number
   */
  std::string verse(short num);
  /*! @return The text of verses [start,end], separated by newline as in the
   * song.
   *  @param start The first verse to include
   *  @param end   The last  verse to include
   */
  std::string verses(short start, short end);
  /*! @return The text of the song.
   */
  std::string sing();
} // namespace food_chain

#endif // FOOD_CHAIN_H
