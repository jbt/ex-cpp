#include "robot_simulator.hpp"

#include <iomanip>
#include <iostream>

using Bot = robot_simulator::Robot;

void Bot::turn_left()
{
  if (dir_ == Bearing::NORTH) {
    dir_ = Bearing::WEST;
  } else {
    dir_ = static_cast<Bearing>(static_cast<int>(dir_) - 1);
  }
}
void Bot::turn_right()
{
  if (dir_ == Bearing::WEST) {
    dir_ = Bearing::NORTH;
  } else {
    dir_ = static_cast<Bearing>(static_cast<int>(dir_) + 1);
  }
}
void Bot::advance()
{
  // clang-format off
  switch (dir_) {
  case Bearing::NORTH: y_++; break;
  case Bearing::SOUTH: y_--; break;
  case Bearing::EAST : x_++; break;
  case Bearing::WEST : x_--; break;
  }
  // clang-format on
}
void Bot::execute_sequence(std::string_view command_sequence)
{
  for (auto c : command_sequence) {
    switch (c) {
    case 'L': turn_left(); break;
    case 'R': turn_right(); break;
    case 'A': advance(); break;
    default: {
      auto i = static_cast<unsigned short>(c); // NOLINT
      std::clog << "Invalid command: 0x";
      if (c <= 0xF) {
        std::clog.put('0');
      }
      std::clog << std::hex << i << ' ' << c << '\n';
    }
    }
  }
}
