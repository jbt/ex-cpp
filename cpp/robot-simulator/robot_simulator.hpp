#if !defined(ROBOT_SIMULATOR_H)
#define ROBOT_SIMULATOR_H

#include <string_view>
#include <utility>

/*! Robot Simulator exercize
 */
namespace robot_simulator {

  /*! A primary cardinal direction
   */
  enum class Bearing {
    NORTH,
    EAST,
    SOUTH,
    WEST,
  };

  /*! Tracking the robot's physical state
   */
  class Robot {
    Bearing dir_ = Bearing::NORTH;
    int x_, y_;

  public:

    /*! Construct a robot from initial state
     * @param head The direction the robot is initially facing
     * @param pos  Where the robot initially is positioned (translationally)
     */
    explicit Robot(std::pair<int,int> pos = {0,0}, Bearing head = Bearing::NORTH)
    : dir_{head}
    , x_{pos.first}
    , y_{pos.second}
    {}

    /*! @return Where, on the grid, the robot is
     */
    std::pair<int,int> get_position() const { return {x_,y_}; }

    /*! @return direction the robot is facing
     */
    Bearing get_bearing() const { return dir_; }

    /*! 90 degrees to the left
     */
    void turn_left();
    /*! 90 degrees to the right
     */
    void turn_right();
    /*! Forward by 1 unit on the grid
     */
    void advance();
    /*! Execute a batch move commands
     * @param command_sequence
     *    L=Left
     *    R=Right
     *    A=Advance
     */
    void execute_sequence(std::string_view command_sequence);
  };
}  // namespace robot_simulator

#endif // ROBOT_SIMULATOR_H
