#pragma once

#include <string>
#include <string_view>

namespace two_fer {
  using namespace std::literals;

  ///< What to say when gifting the free extra cookie.
  auto two_fer(std::string_view recipient_name = "you"sv) -> std::string;
} // namespace two_fer
