#include "two_fer.h"

auto two_fer::two_fer(std::string_view name) -> std::string
{
  std::string rv{"One for "sv};
  rv.append(name).append(", one for me.");
  return rv;
}
