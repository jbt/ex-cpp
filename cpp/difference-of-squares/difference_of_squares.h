#pragma once

/*! Difference of Squares exercize
 */
namespace difference_of_squares {
  /*! @return Square of ( sum of 1..N )
   *  @param N the upper bound on natural numbers to include in the sum
   */
  constexpr long square_of_sum(long N)
  {
    // https://en.wikipedia.org/wiki/1_%2B_2_%2B_3_%2B_4_%2B_%E2%8B%AF#Partial_sums
    auto sum = N * (N + 1) / 2;
    return sum * sum;
  }
  /*! @return Sum of i^2 for i= 1..N
   *  @param N the upper bound on natural numbers to include
   */
  constexpr long sum_of_squares(long N)
  {
    // https://brilliant.org/wiki/sum-of-n-n2-or-n3/
    return N * (N + 1) * (2 * N + 1) / 6;
  }
  /*! @return square_of_sum - sum_of_squares
   *  @param N the upper bound on natural numbers to involve in both operands
   */
  constexpr long difference(long N)
  {
    return square_of_sum(N) - sum_of_squares(N);
  }
} // namespace difference_of_squares
