#include "difference_of_squares.h"

namespace sq = difference_of_squares;

// NOLINTBEGIN

static_assert((1 + 2 + 3 + 4) * (1 + 2 + 3 + 4) == sq::square_of_sum(4),
              "10*10==100");
static_assert(1 * 1 + 2 * 2 + 3 * 3 + 4 * 4 == sq::sum_of_squares(4),
              "1+4+9+16==30");
static_assert(100 - 30 == sq::difference(4), "100-30==70");

// NOLINTEND
