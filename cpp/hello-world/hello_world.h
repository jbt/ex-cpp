#pragma once

#include <string_view>

namespace hello_world {

  constexpr auto hello()
  {
    using namespace std::literals;
    return "Hello, World!"sv;
  }

} // namespace hello_world
