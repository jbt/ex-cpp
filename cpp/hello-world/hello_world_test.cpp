#include "hello_world.h"

#ifdef EXERCISM_TEST_SUITE
#include <catch2/catch.hpp>
#else
#include "test/catch.hpp"
#endif

TEST_CASE("Hello")
{
    CHECK("Hello, World!" == hello_world::hello());
}


