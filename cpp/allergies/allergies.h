#if !defined(ALLERGIES_H)
#define ALLERGIES_H

#include <algorithm>
#include <array>
#include <bitset>
#include <string>
#include <string_view>
#include <unordered_set>

/*! Allergies exercize
 */
namespace allergies {
  /*! Decoding an allergy score
   */
  class allergy_test {
    constexpr static std::array<std::string_view, 8> allergens{
        "eggs",     "peanuts",   "shellfish", "strawberries",
        "tomatoes", "chocolate", "pollen",    "cats"};

    std::bitset<8> score_;

  public:
    /*! Construct the test
     *  @param score The person's allergy score
     */
    constexpr explicit allergy_test(unsigned score)
    : score_{score}
    {
    }
    /*! @return whether the person is allergic to a particular thing
     *  @param allergen The exact, case-sensitive name of a supported allergen
     */
    constexpr bool is_allergic_to(std::string_view allergen) const
    {
      auto it = std::ranges::find(allergens, allergen);
      auto i = it - allergens.begin();
      return score_.test(i);
    }
    /*! @return All of the allergens this score indicates the person is allergic
     * to.
     */
    std::unordered_set<std::string> get_allergies() const;
  };
} // namespace allergies

#endif // ALLERGIES_H
