#include "allergies.h"

auto allergies::allergy_test::get_allergies() const
    -> std::unordered_set<std::string>
{
  std::unordered_set<std::string> result;
  for (auto i = 0U; i < allergens.size(); ++i) {
    if (score_.test(i)) {
      result.emplace(allergens.at(i));
    }
  }
  return result;
}
