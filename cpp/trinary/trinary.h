#pragma once

#include <cstdint>
#include <string_view>

namespace trinary {
  using integer = std::uint_fast64_t;

  // notably does not actually return a decimal representation, but rather a
  // native, primitive one.
  constexpr integer to_decimal(std::string_view str_rep)
  {
    auto rv = integer{0ul};
    for (auto digit : str_rep) {
      rv *= 3;
      switch (digit) {
      case '1': ++rv; break;
      case '2': rv += 2; break;
      default: return integer{0ul};
      case '0':; // nothing needs to happen
      }
    }
    return rv;
  }
} // namespace trinary
