#include "trinary.h"

namespace t_1_yields_decimal_1 {
  static_assert(1 == trinary::to_decimal("1"));
}

namespace t_2_yields_decimal_2 {
  static_assert(2 == trinary::to_decimal("2"));
}

namespace t_10_yields_decimal_3 {
  static_assert(3 == trinary::to_decimal("10"));
}

namespace t_11_yields_decimal_4 {
  static_assert(4 == trinary::to_decimal("11"));
}

namespace t_100_yields_decimal_9 {
  static_assert(9 == trinary::to_decimal("100"));
}

namespace t_112_yields_decimal_14 {
  static_assert(14 == trinary::to_decimal("112"));
}

namespace t_222_yields_decimal_26 {
  static_assert(26 == trinary::to_decimal("222"));
}

namespace t_1122000120_yields_decimal_32091 {
  static_assert(32091 == trinary::to_decimal("1122000120"));
}

namespace invalid_yields_decimal_0 {
  static_assert(0 == trinary::to_decimal("carrot"));
}
