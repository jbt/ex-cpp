#if !defined(CLOCK_H)
#define CLOCK_H

#include <cstdint>
#include <string>

namespace date_independent {

  ///< A time of day. Resolution is minutes.
  class clock {
    short const min_;

    explicit clock(int);

  public:
    /*! Construct a clock for a given time.
     * @param hour_of_day The big    hand
     * @param min_of_hour The little hand
     * @return clock
     */
    static auto at(short hour_of_day, short min_of_hour) -> clock;

    /*! Implicit conversion to string.
     * @return std::string
     */
    operator std::string() const;

    /*! @return the hour of the day [0,24)
     */
    [[nodiscard]] auto hour() const -> std::int8_t;

    /*! @return the minute of the hour [0,60)
     */
    [[nodiscard]] auto min() const -> std::int8_t;

    /*! Add minutes to the time. Does wrap.
     * @param minutes to add (negative to subtract)
     * @return result of time shift
     */
    [[nodiscard]] auto plus(int minutes) const -> clock;

    /*! @param rhs the right-hand side of a comparison
     *  @return comparison result (one of greater,less,equal)
     */
    auto operator<=>(clock const &rhs) const = default;
  };
} // namespace date_independent

#endif // CLOCK_H
