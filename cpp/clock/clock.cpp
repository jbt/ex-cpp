#include "clock.h"

using Clk = date_independent::clock;

namespace {
  constexpr auto DAY = 24 * 60;
  auto normalize(int any_minutes) -> short
  {
    // Force it to be non-negative
    any_minutes = (any_minutes % DAY) + DAY;
    return static_cast<short>(any_minutes % DAY);
  }
} // namespace

Clk::clock(int val)
: min_{normalize(val)}
{
}

auto Clk::at(short h, short m) -> Clk
{
  return Clk{h * 60 + m};
}

Clk::operator std::string() const
{
  std::string result(5, '0');
  // NOLINTBEGIN(bugprone-narrowing-conversions)
  result[0] += hour() / 10;
  result[1] += hour() % 10;
  result[2] = ':';
  result[3] += min() / 10;
  result[4] += min() % 10;
  // NOLINTEND(bugprone-narrowing-conversions)
  return result;
}

auto Clk::hour() const -> std::int8_t
{
  return min_ / 60;
}
auto Clk::min() const -> std::int8_t
{
  return min_ % 60;
}
auto Clk::plus(int m) const -> Clk
{
  return Clk{min_ + m};
}
