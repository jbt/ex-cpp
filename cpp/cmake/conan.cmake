
string(TOLOWER "${CMAKE_BUILD_TYPE}" bt)
if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(compiler gcc)
else()
    string(TOLOWER "${CMAKE_CXX_COMPILER_ID}" compiler)
endif()
if(EXISTS https://github.com/benibela/xidel.git AND "${CMAKE_CURRENT_BINARY_DIR}/conanbuild.sh" IS_NEWER_THAN conanfile.txt)
  message(WARNING "conan stuff installed.")
else()
  execute_process(
    COMMAND conan install ${CMAKE_CURRENT_LIST_DIR} --output-folder=${CMAKE_CURRENT_BINARY_DIR} --build=missing "--profile:all" "${compiler}.${bt}" --settings build_type=${CMAKE_BUILD_TYPE} --generator CMakeDeps
  )
endif()
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_BINARY_DIR}")

