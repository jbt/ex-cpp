project(
  @exercize@
  LANGUAGES CXX
)

find_package(Catch2 REQUIRED)
find_package(Boost REQUIRED)
find_package(Threads REQUIRED)

file(
  GLOB
  srcs
  *.h
  *.cpp
)
file(
  GLOB
  test_srcs
  *_test.cpp
)
list(REMOVE_ITEM srcs ${test_srcs})

add_library(
  @exercize@_exercize
  ${srcs}
)
target_compile_features(
  @exercize@_exercize
  PUBLIC
    cxx_std_23
)
target_compile_options(
  @exercize@_exercize
  PUBLIC
    -fsanitize=undefined
    -O0 -g3 -ggdb3 -fno-inline
)
target_compile_options(
  @exercize@_exercize
  PRIVATE
    -Wall
    -Wextra
    -Wpedantic
    -Werror
)
target_link_libraries(
  @exercize@_exercize
  PUBLIC
    Boost::headers
    Threads::Threads
    ubsan
)

include(clang-tools)
clang_tooling(@exercize@_exercize)

foreach(t few all slow)
  add_executable(
    @exercize@_${t}_runner
    ${test_srcs}
    ../lasagna/test/tests-main.cpp
  )
  target_link_libraries(
    @exercize@_${t}_runner
    PUBLIC
      @exercize@_exercize
      # Boost::headers
      Catch2::Catch2
  )
  add_custom_command(
    OUTPUT ${t}.cert
    COMMAND timeout ${MAX_TEST_TIME} $<TARGET_FILE:@exercize@_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.cert
    DEPENDS @exercize@_${t}_runner
    COMMENT "@exercize@_${t} passed"
  )
  add_custom_command(
    OUTPUT ${t}.memcheck.cert
    COMMAND timeout ${MAX_VALGRIND_TEST_TIME} valgrind --exit-on-first-error=yes --error-exitcode=99 --leak-check=full --track-origins=yes --quiet $<TARGET_FILE:@exercize@_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.memcheck.cert
    DEPENDS @exercize@_${t}_runner
    COMMENT "@exercize@_${t} passed under valgrind"
  )
endforeach()
target_compile_definitions(
  @exercize@_all_runner
  PUBLIC
    EXERCISM_RUN_ALL_TESTS=1
)
target_compile_definitions(
  @exercize@_slow_runner
  PUBLIC
    EXERCISM_INCLUDE_BENCHMARK=1
    CATCH_CONFIG_ENABLE_BENCHMARKING=1
)

foreach(t few all)
  add_custom_target(
    check_@exercize@_${t}
    ALL
    DEPENDS ${t}.cert ${t}.memcheck.cert
    COMMENT "@exercize@ ${t} tests."
  )
endforeach()
add_custom_target(
  check_@exercize@_slow
  ALL
  DEPENDS slow.cert
  COMMENT "@exercize@ Slow/benchmark tests."
)
add_dependencies(check_@exercize@_all  check_@exercize@_few)
add_dependencies(check_@exercize@_slow check_@exercize@_all)
