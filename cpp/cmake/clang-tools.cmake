find_program(TIDY   clang-tidy  )
find_program(FORMAT clang-format)

execute_process(
  COMMAND "${TIDY}" --version
  OUTPUT_VARIABLE tidy_version_string
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
execute_process(
  COMMAND "${FORMAT}" --version
  OUTPUT_VARIABLE format_version_string
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

string(REGEX REPLACE "^.*version ([0-9]+\.[0-9]+).*$" "\\1" format_version "${format_version_string}")
string(REGEX REPLACE "^.*version ([0-9]+\.[0-9]+).*$" "\\1"   tidy_version   "${tidy_version_string}")

if(format_version LESS 18.0)
  message(WARNING "clang-format version ${format_version} is too old. Will not use.")
  set(FORMAT OFF)
endif()
if(tidy_version LESS 17.0)
  message(WARNING "clang-tidy version ${tidy_version} is too old. Will not use.")
  set(TIDY OFF)
endif()

function(clang_tooling target)
  set(cd "${CMAKE_CURRENT_BINARY_DIR}/certs")
  file(MAKE_DIRECTORY "${cd}")
  if(TIDY)
    set_target_properties(
      ${target}
      PROPERTIES
        CXX_CLANG_TIDY "${TIDY};--config-file=${CMAKE_SOURCE_DIR}/.clang-tidy;--fix"
    )
  endif()
  if(FORMAT)
    get_target_property(
      srcs
      ${target}
      SOURCES
    )
    foreach(src ${srcs})
      string(FIND "${src}" _test.cpp test_suffix)
      cmake_path(HASH src hash)
      set(c "${cd}/${hash}.format")
      add_custom_command(
        OUTPUT "${c}"
        COMMAND test "${c}" -nt "${src}" || clang-format -i --style=file ${src}
        COMMAND ${CMAKE_COMMAND} -E touch "${c}"
        DEPENDS ${src} ${target}
        COMMENT " "
      )
      list(APPEND format_certs "${c}")
    endforeach()
    add_custom_target(
      format_${target}
      ALL
      DEPENDS ${format_certs}
      # COMMAND ${CMAKE_COMMAND} -E echo "${target} sources formatted."
      COMMENT "${target} sources formatted."
      WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    )
    list(APPEND formatting "format_${target}")
  endif()
endfunction()
