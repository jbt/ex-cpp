
add_custom_target(submit
  ALL
  COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/submit.sh"
  WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  DEPENDS ${test_certs} doc ${formatting}
  COMMENT "Checking for exercism submission..."
)
