#ifndef KNAPSACK_H
#define KNAPSACK_H

#include <vector>

namespace knapsack {
  /*! Something which could be placed in the knapsack
   */
  struct Item {
    unsigned weight; ///< The item's physical weight (cost)
    unsigned value;  ///< utils for having the thing in the knapsack

    /*! Sort order
     *  @param rhs the right-hand side of the operator
     *  @return whether *this should come before rhs
     */
    bool operator<(Item const &rhs) const;

    /*! @return milli-utils per unit of weight
     */
    long rate() const;
  };

  /*! @return the maximum value one can pack
   *  @param max_weight The maximum acceptable weight
   *  @param items The items available to pack
   */
  unsigned maximum_value(unsigned max_weight, std::vector<Item> items);

} // namespace knapsack

#endif // KNAPSACK_H
