#include "knapsack.h"

#include <algorithm>
#include <span>

namespace k = knapsack;

namespace {
  auto pack(unsigned w, std::span<k::Item> is) -> unsigned
  {
    if (is.empty() || !w) {
      return 0U;
    }
    auto result = pack(w, is.subspan(1UL));
    if (is.front().weight <= w) {
      auto u = is.front().value + pack(w - is.front().weight, is.subspan(1UL));
      result = std::max(u, result);
    }
    return result;
  }
} // namespace

auto k::maximum_value(unsigned max_w, std::vector<Item> v) -> unsigned
{
  std::sort(v.begin(), v.end());
  auto result = 0U;
  for (auto s = std::span<Item>{v}; s.size(); s = s.subspan(1UL)) {
    auto u = pack(max_w, s);
    result = std::max(result, u);
  }
  return result;
}

auto k::Item::operator<(Item const& rhs) const -> bool
{
  if (rate() != rhs.rate()) {
    return rate() > rhs.rate();
  }
  return weight > rhs.weight;
}
auto k::Item::rate() const -> long
{
  return static_cast<long>(value) * 1000 // NOLINT
         / static_cast<long>(weight);
}
