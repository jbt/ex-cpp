#pragma once

#include <functional>
#include <iterator>
#include <vector>

namespace list_ops {
  void append(auto& into, auto const& from) {
    for (auto& f : from) {
      into.push_back(f);
    }
  }
  template<class Cont>
  auto concat(Cont const& two_dim) -> auto {
    typename Cont::value_type result;
    for (auto& one_dim : two_dim) {
      append(result, one_dim);
    }
    return result;
  }
  template<class Cont>
  auto filter(Cont const& fwd, auto f) -> Cont {
    Cont result;
    for (auto& e : fwd) {
      if (f(e)) {
        result.push_back(e);
      }
    }
    return result;
  }
  template<class Cont>
  auto reverse(Cont const& fwd) -> Cont {
    Cont result{fwd.rbegin(), fwd.rend()};
    return result;
  }
  std::size_t length(auto const& lst) {
    auto result = 0UL;
    for (auto& _ : lst) {
      ++result;
    }
    return result;
  }
  template<class Out=int>
  auto map(auto& in, auto f) {
    std::vector<Out> out;
    for (auto const& e : in) {
      out.push_back(f(e));
    }
    return out;
  }
  auto foldl(auto& rng, auto init, auto f) {
    auto v = init;
    for (auto& e : rng) {
      v = f(v, e);
    }
    return v;
  }
  auto foldr(auto& rng, auto init, auto f) {
    auto v = init;
    for (auto& e : reverse(rng)) {
      v = f(v, e);
    }
    return v;
  }
}  // namespace list_ops

