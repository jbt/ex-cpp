#pragma once

#include <algorithm>
#include <array>
#include <initializer_list>
#include <iostream>

/*! The Sublist exercize
 */
namespace sublist {
  /*! The different relations these ordered lists can be classified as here
   */
  enum class List_comparison { unequal, equal, sublist, superlist };
  /*! Describe the inverse relation
   *  @details if a is to b (given) then b is to a (returned)
   *    e.g. if a is a sublist of b, then b is a superlist of a
   *  @param c The relation from one perspective
   *  @return The relation from the inverse perspective
   */
  constexpr List_comparison invert(List_comparison c)
  {
    using C = List_comparison;
    if (c == C::sublist) {
      return C::superlist;
    } else if (c == C::superlist) {
      return C::sublist;
    } else {
      return c;
    }
  }
  /*! @return the relationship a has to b
   *  @param a An ordered list: haystack
   *  @param b Another ordered list: needle
   */
  constexpr List_comparison sublist(std::initializer_list<int> a,
                                    std::initializer_list<int> b)
  {
    if (a.size() < b.size()) {
      return invert(sublist(b, a));
    }
    using C = List_comparison;
    auto found = std::ranges::search(a, b);
    if (found.size() == a.size()) {
      return C::equal;
    }
    if (found.size() == b.size()) {
      return C::superlist;
    }
    return C::unequal;
  }
} // namespace sublist
