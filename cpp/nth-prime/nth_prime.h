#if !defined(NTH_PRIME_H)
#define NTH_PRIME_H

#include <cstdint>

namespace nth_prime {
  unsigned long nth(unsigned);
} // namespace nth_prime

#endif // NTH_PRIME_H
