#include "nth_prime.h"

#include <algorithm>
#include <stdexcept>
#include <vector>

namespace rg = std::ranges;

auto nth_prime::nth(unsigned n) -> unsigned long
{
  if (!n) {
    throw std::domain_error{"0th prime?"};
  }
  std::vector<unsigned long> primes;
  primes.reserve(n);
  primes.push_back(2);
  while (primes.size() < n) {
    for (auto cand = primes.back() + 1U; cand; ++cand) {
      auto div = [cand](auto p) { return cand % p == 0; };
      if (rg::none_of(primes, div)) {
        primes.push_back(cand);
        break;
      }
    }
  }
  return primes.back();
}
