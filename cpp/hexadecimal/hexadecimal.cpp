#include "hexadecimal.h"

static_assert(hexadecimal::convert("DecafBad") == 0xDecafBad, "Basic Usage");
static_assert(
    hexadecimal::convert("Invalid") == 0,
    "I'd prefer it didn't compile by default, but the test case is how it is.");

static_assert(hexadecimal::convert<hexadecimal::errors_are::exception>(
                  "DecafBad") == 0xdecafbad);

namespace hex_1_is_decimal_1 {
  static_assert(0x1 == hexadecimal::convert("1"));
}

namespace hex_c_is_decimal_12 {
  static_assert(0xc == hexadecimal::convert("c"));
}

namespace hex_10_is_decimal_16 {
  static_assert(0x10 == hexadecimal::convert("10"));
}

namespace hex_af_is_decimal_175 {
  static_assert(0xaf == hexadecimal::convert("af"));
}

namespace hex_100_is_decimal_256 {
  static_assert(0x100 == hexadecimal::convert("100"));
}

namespace hex_19ace_is_decimal_105166 {
  static_assert(0x19ace == hexadecimal::convert("19ace"));
}

namespace invalid_hex_is_decimal_0 {
  static_assert(0 == hexadecimal::convert("carrot"));
}

namespace black {
  static_assert(0x000000 == hexadecimal::convert("000000"));
}

namespace white {
  static_assert(0xffffff == hexadecimal::convert("ffffff"));
}

namespace yellow {
  static_assert(0xffff00 == hexadecimal::convert("ffff00"));
}
