#pragma once

#include <cstdint>
#include <experimental/array>
#include <stdexcept>
#include <string_view>

namespace hexadecimal {
  enum class errors_are : char { zero, exception };

  template <errors_are const error_policy = errors_are::zero>
  constexpr std::size_t convert(std::string_view hex_chars)
  {
    struct char_conv_range {
      char lower_;
      char upper_;
      std::size_t offset_;
    };
    constexpr auto hex_ranges = std::experimental::make_array(
        char_conv_range{'0', '9', 0UL}, char_conv_range{'A', 'F', 10UL},
        char_conv_range{'a', 'f', 10UL});
    auto rv = std::size_t{};
    for (auto c : hex_chars) {
      rv <<= 4;
      auto invalid = true;
      for (auto& range : hex_ranges) {
        if (c >= range.lower_ && c <= range.upper_) {
          rv += c - range.lower_ + range.offset_;
          invalid = false;
        }
      }
      if (invalid) {
        if (error_policy == errors_are::zero) {
          return 0UL;
        } else {
          throw std::invalid_argument{__func__};
        }
      }
    }
    return rv;
  }
} // namespace hexadecimal
