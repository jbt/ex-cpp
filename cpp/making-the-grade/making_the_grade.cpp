#include <cassert>

#include <algorithm>
#include <array>
#include <ranges>
#include <sstream>
#include <string>
#include <vector>

namespace rg = std::ranges;
namespace vw = rg::views;

// Round down all provided student scores.
auto round_down_scores(std::vector<double> student_scores) -> std::vector<int>
{
  auto result = student_scores |
                vw::transform([](double d) { return static_cast<int>(d); });
  // My compiler doesn't seem to have this yet
  // return {std::from_range_t, result};
  return {result.begin(), result.end()};
}

// Count the number of failing students out of the group provided.
auto count_failed_students(std::vector<int> student_scores)
{
  return rg::count_if(student_scores, [](auto i) { return i <= 40; });
}

// Determine how many of the provided student scores were 'the best' based on
// the provided threshold.
auto above_threshold(std::vector<int> student_scores, int threshold)
    -> std::vector<int>
{
  auto result = student_scores |
                vw::filter([threshold](auto i) { return i >= threshold; });
  return {result.begin(), result.end()};
}

// Create a list of grade thresholds based on the provided highest grade.
auto letter_grades(int highest_score) -> std::array<int, 4>
{
  auto w = (highest_score - 40) / 4;
  // clang-format off
  return {
      41,
      41 + w,
      41 + w * 2,
      41 + w * 3,
  };
  // clang-format on
}

auto rank_string(std::tuple<int, std::string, int> s) -> std::string
{
  std::ostringstream oss;
  // clang-format off
    oss << std::get<0>(s) << ". "
        << std::get<1>(s) << ": "
        << std::get<2>(s)
        ;
  // clang-format on
  return oss.str();
}

// Organize the student's rank, name, and grade information in ascending order.
auto student_ranking(std::vector<int> student_scores,
                     std::vector<std::string> student_names)
    -> std::vector<std::string>
{
  assert(student_scores.size() == student_names.size());
  auto r = vw::zip(vw::iota(1), student_names, student_scores) |
           vw::transform(rank_string);
  std::vector<std::string> result(student_scores.size());
  rg::copy(r, result.begin());
  return result;
}

// Create a string that contains the name of the first student to make a perfect
// score on the exam.
auto perfect_score(std::vector<int> student_scores,
                   std::vector<std::string> student_names) -> std::string
{
  for (auto const& [score, name] : vw::zip(student_scores, student_names)) {
    if (score == 100) {
      return name;
    }
  }
  return {};
}
