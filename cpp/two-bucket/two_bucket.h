#ifndef TWO_BUCKET_H
#define TWO_BUCKET_H

namespace two_bucket {

  /*! Which specific bucket is being referred to
   */
  enum class bucket_id { one, two };

  /*! Output of measure
   */
  struct measure_result {
    int num_moves; ///< How many moves were necessary, including filling the
                   ///< start
    bucket_id goal_bucket;   ///< Which bucket ended up with the target volume
    int other_bucket_volume; ///< What volume was in the non-target bucket
  };

  /*! @return Info about the result of all the moves
   *  @param bucket1_capacity The size of the bucket_id::one
   *  @param bucket2_capacity The size of the bucket_id::two
   *  @param target_volume End condition: at least one bucket must have this
   * much water at the end
   *  @param start_bucket Which bucket should be filled first.
   */
  measure_result measure(int bucket1_capacity, int bucket2_capacity,
                         int target_volume, bucket_id start_bucket);

} // namespace two_bucket

#endif // TWO_BUCKET_H
