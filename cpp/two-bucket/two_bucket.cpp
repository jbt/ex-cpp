#include "two_bucket.h"

#include <iostream>
#include <limits>
#include <optional>
#include <set>
#include <vector>

namespace tb = two_bucket;

namespace {
  struct Bucket {
    int const cap;
    int vol;
    auto operator<=>(Bucket const&) const = default;
  };
  auto empty(Bucket const& b) -> bool
  {
    return b.vol == 0;
  }
  auto full(Bucket const& b) -> bool
  {
    return b.vol == b.cap;
  }

  enum class Move : char {
    EmptyStart = '0',
    EmptyOther,
    FillStart,
    FillOther,
    PourStartIntoOther,
    PourOtherIntoStart
  };
  auto inc(Move& m) -> bool
  {
    if (m == Move::PourOtherIntoStart) {
      return false;
    }
    m = static_cast<Move>(static_cast<char>(m) + 1);
    return true;
  }
  void invert(tb::bucket_id& i)
  {
    if (tb::bucket_id::one == i) {
      i = tb::bucket_id::two;
    } else {
      i = tb::bucket_id::one;
    }
  }
  class State {
    Bucket start;
    Bucket other;

  public:
    State(int cap1, int cap2)
    : start{cap1, cap1}
    , other{cap2, 0}
    {
    }
    [[nodiscard]] auto move(Move) const -> std::optional<State>;
    [[nodiscard]] auto done(int target_vol) const
        -> std::optional<tb::measure_result>;
    auto display(std::ostream&) const -> std::ostream&;
    auto operator<=>(State const&) const = default;
  };
} // namespace

auto tb::measure(int bucket1_capacity, int bucket2_capacity, int target_volume,
                 bucket_id start_bucket) -> measure_result
{
  std::set<State> seen;
  std::vector<State> paths, next;
  if (start_bucket == bucket_id::one) {
    paths.emplace_back(bucket1_capacity, bucket2_capacity);
  } else {
    paths.emplace_back(bucket2_capacity, bucket1_capacity);
  }
  for (auto moves = 1; paths.size() && moves < std::numeric_limits<int>::max();
       ++moves) {
    for (auto& path : paths) {
      path.display(std::cout << moves << ' ') << '\n';
      if (auto result = path.done(target_volume)) {
        result->num_moves = moves;
        if (start_bucket == bucket_id::two) {
          invert(result->goal_bucket);
        }
        return *result;
      }
      for (auto m = Move::EmptyStart; inc(m);) {
        if (auto p = path.move(m)) {
          if (seen.emplace(*p).second) {
            next.push_back(*p);
          }
        }
      }
    }
    paths.swap(next);
    next.clear();
  }
  throw std::runtime_error{"Goal not possible."};
}

namespace {
  auto State::done(int target_vol) const -> std::optional<tb::measure_result>
  {
    if (target_vol == start.vol) {
      return tb::measure_result{0, tb::bucket_id::one, other.vol};
    } else if (target_vol == other.vol) {
      return tb::measure_result{0, tb::bucket_id::two, start.vol};
    } else {
      return std::nullopt;
    }
  }
  void pour(Bucket& f, Bucket& t)
  {
    auto a = std::min(t.cap - t.vol, f.vol);
    f.vol -= a;
    t.vol += a;
  }
  auto State::move(Move m) const -> std::optional<State>
  {
    auto n = *this;
    switch (m) {
    case Move::EmptyStart: n.start.vol = 0; break;
    case Move::EmptyOther: n.other.vol = 0; break;
    case Move::FillStart: n.start.vol = start.cap; break;
    case Move::FillOther: n.other.vol = other.cap; break;
    case Move::PourOtherIntoStart: pour(n.other, n.start); break;
    case Move::PourStartIntoOther: pour(n.start, n.other); break;
    }
    // After an action, you may not arrive at a state where the initial starting
    // bucket is empty and the other bucket is full.
    if (empty(start) && full(other)) {
      return {};
    }
    return n;
  }
  auto State::display(std::ostream& s) const -> std::ostream&
  {
    return s << start.vol << '/' << start.cap << " | " << other.vol << '/'
             << other.cap;
  }
} // namespace
