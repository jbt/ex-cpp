#if !defined(COMPLEX_NUMBERS_H)
#define COMPLEX_NUMBERS_H

#include <cmath>

namespace complex_numbers {

  /*! A complex number
   */
  class Complex {
    double const real_;
    double const imag_;

  public:
    /*! Construct from its parts
     * @param real      The `a` in `a + bi`
     * @param imaginary The `b` in `a + bi`
     */
    constexpr explicit Complex(double real, double imaginary = 0.0)
    : real_{real}
    , imag_{imaginary}
    {
    }

    /*! @return the real part
     */
    constexpr double real() const { return real_; }

    /*! @return the imaginary part
     */
    constexpr double imag() const { return imag_; }

    /*! @return *this plus rhs
     *  @param rhs The right-hand side of the operator (*this is lhs)
     */
    constexpr Complex operator+(Complex rhs) const
    {
      return Complex{real() + rhs.real(), imag() + rhs.imag()};
    }

    /*! @return negation of *this
     */
    constexpr Complex operator-() const { return Complex{-real(), -imag()}; }

    /*! @return subtraction
     *  @param rhs The right-hand operand
     */
    constexpr Complex operator-(Complex rhs) const { return *this + (-rhs); }

    /*! @return Complex product
     * @param rhs The right-hand operator
     */
    constexpr Complex operator*(Complex rhs) const
    {
      return Complex{real() * rhs.real() - imag() * rhs.imag(),
                     real() * rhs.imag() + imag() * rhs.real()};
    }

    /*! @return conjugate of this
     */
    constexpr Complex conj() const { return Complex{real(), -imag()}; }

    /*! @return a/(a^2 + b^2) - b/(a^2 + b^2) * i.
     */
    constexpr Complex recip() const { return (*this / hyp()).conj(); }

    /*! @return Complex quotient
     *  @param rhs The right-hand operand, i.e. the divisor
     */
    constexpr Complex operator/(Complex rhs) const
    {
      return *this * rhs.recip();
    }

    /*! @return |*this|
     * @note Not constexpr, because I don't care to re-implement sqrt
     */
    double abs() const { return std::sqrt(hyp()); }

    /*! @return scalar multiplication
     *  @param rhs the right-hand operand
     *  @note trig functions are also not constexpr
     */
    constexpr Complex operator*(double rhs) const
    {
      return Complex{real() * rhs, imag() * rhs};
    }

    /*! @return scalar division
     *  @param rhs the right-hand operand
     */
    constexpr Complex operator/(double rhs) const
    {
      return *this * (1.0 / rhs);
    }

    /*! e, the base of the natural log, raised to *this
     * @return e^a * (cos(b) + i * sin(b))
     */
    Complex exp() const
    {
      auto ea = std::exp(real());
      return Complex{ea * std::cos(imag()), ea * std::sin(imag())};
    }

    /*! @return *this plus a real number
     *  @param rhs The right-hand operand
     */
    constexpr Complex operator+(double rhs) const
    {
      return Complex{real() + rhs, imag()};
    }

    /*! @return *this minus a real number
     *  @param rhs The right-hand operand
     */
    constexpr Complex operator-(double rhs) const { return *this + -rhs; }

    /*! @return Naive comparison
     * @param rhs the right-hand operand
     */
    constexpr bool operator<=>(Complex const &rhs) const = default;

  private:
    constexpr double hyp() const { return real() * real() + imag() * imag(); }
  };

  constexpr Complex operator+(double d, Complex c)
  {
    return c + d;
  }
  constexpr Complex operator-(double d, Complex c)
  {
    return Complex{d} - c;
  }
  constexpr Complex operator*(double d, Complex c)
  {
    return c * d;
  }
  constexpr Complex operator/(double d, Complex c)
  {
    return Complex{d} / c;
  }
} // namespace complex_numbers

#endif // COMPLEX_NUMBERS_H
