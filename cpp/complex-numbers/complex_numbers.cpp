#include "complex_numbers.h"

using C = complex_numbers::Complex;

namespace {
  constexpr C p1_2{1., 2.};
  constexpr C p5_3{5., 3.};
  constexpr C n1_13{-1, 13};
  constexpr C p0p2_n0p4{0.2, -0.4};
  constexpr C p4_1{4, 1};

  namespace acc {
    constexpr double r = p1_2.real();
    static_assert(r == 1., "real");
    static_assert(p1_2.imag() == 2., "imag");
  } // namespace acc
  namespace mul {
    constexpr C a = p1_2 * p5_3;
    static_assert(a == n1_13, "cmul");
  } // namespace mul
  namespace div {
    static_assert(p1_2.recip().real() == 0.20);
    static_assert(p1_2.recip() == p0p2_n0p4);
    static_assert((2.0 / p1_2).real() == 0.4);
    static_assert((2.0 / p1_2).imag() == -0.80);
  } // namespace div
  namespace sub {
    static_assert(p5_3 - p1_2 == p4_1);
    static_assert(p5_3 - 9 == C{-4., 3.});
  } // namespace sub
} // namespace
