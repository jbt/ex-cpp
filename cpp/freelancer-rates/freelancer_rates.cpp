#include "freelancer_rates.hpp"

#include <cmath>

auto daily_rate(double hourly_rate) -> double
{
  return hourly_rate * 8.;
}

auto apply_discount(double before_discount, double discount) -> double
{
  return before_discount * (100. - discount) / 100.;
}

auto monthly_rate(double hourly_rate, double discount) -> int
{
  auto full_monthly = daily_rate(hourly_rate) * 22.;
  return std::ceil(apply_discount(full_monthly, discount));
}

auto days_in_budget(int budget, double hourly_rate, double discount) -> int
{
  auto day_cost = apply_discount(daily_rate(hourly_rate), discount);
  return std::floor(budget / day_cost);
}
