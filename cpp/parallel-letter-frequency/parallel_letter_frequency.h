#if !defined(PARALLEL_LETTER_FREQUENCY_H)
#define PARALLEL_LETTER_FREQUENCY_H

#include <cstdint>

#include <array>
#include <string_view>
#include <vector>

/*! Parallel Letter Frequency exercize
 */
namespace parallel_letter_frequency {

  /*! An integer type large enough to count instances of a particular letter
   */
  using Count = std::uint16_t;
  /*! A type to track text. Has implications about encoding assumptions.
   */
  using Text = std::string_view;

  /*! Result of counting letters from a particular set of texts
   */
  class Freqs {
    std::array<Count, 26> counts_;

  public:
    Freqs();

    /*! @return the Count of occurrences for a single, particular letter
     *  @param c The letter to count
     */
    Count operator[](char c) const;

    /*! @return Whether any letter has been enountered at all.
     */
    bool empty() const;

    /*! Write access
     * @param c The letter to access the count for
     * @return a non-const pointer to a count, if c is a valid letter
     */
    Count *at(char c);

    /*! Add to our counts, the ones contained in another
     * @param other Another set of counts to include in ours
     */
    void add(Freqs const &other);
  };

  /*! @param in_out The Frequency object to add to
   *  @param text   The text to scan for letters
   */
  void frequency(Freqs &in_out, Text text);

  /*! @param texts The corpus to scan
   *  @return The per-letter counts across all texts
   */
  Freqs frequency(std::vector<Text> const &texts);
} // namespace parallel_letter_frequency

#endif
