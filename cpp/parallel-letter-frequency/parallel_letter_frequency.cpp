#include "parallel_letter_frequency.h"

#include <cassert>

#include <algorithm>
#include <optional>
#include <ranges>
#include <thread>

namespace ex = parallel_letter_frequency;
namespace rg = std::ranges;

void ex::frequency(Freqs& in_out, Text text)
{
  for (auto c : text) {
    if (auto cnt = in_out.at(c)) {
      ++*cnt;
    }
  }
}
auto ex::frequency(std::vector<Text> const& texts) -> Freqs
{
  std::vector<Freqs> fqs;
  std::vector<std::thread> thr;
  fqs.reserve(texts.size());
  thr.reserve(texts.size());
  for (auto text : texts) {
    fqs.emplace_back();
    auto fq = &(fqs.back());
    thr.emplace_back([fq, text]() { frequency(*fq, text); });
  }
  Freqs result;
  for (auto i = 0U; i < fqs.size(); ++i) {
    thr.at(i).join();
    result.add(fqs.at(i));
  }
  return result;
}

ex::Freqs::Freqs()
{
  counts_.fill(0);
}
auto ex::Freqs::empty() const -> bool
{
  return rg::all_of(counts_, [](auto x) { return 0 == x; });
}
void ex::Freqs::add(Freqs const& other)
{
  for (auto i = 0U; i < counts_.size(); ++i) {
    counts_[i] += other.counts_[i];
  }
}

namespace {
  auto idx(char c) -> std::optional<std::size_t>
  {
    // NOLINTBEGIN(bugprone-branch-clone)
    if (c < 'A') {
      return {};
    } else if (c <= 'Z') {
      return c - 'A';
    } else if (c < 'a') {
      return {};
    } else if (c <= 'z') {
      return c - 'a';
    } else {
      return {};
    }
    // NOLINTEND(bugprone-branch-clone)
  }
} // namespace
auto ex::Freqs::at(char c) -> Count*
{
  if (auto i = idx(c)) {
    return counts_.data() + i.value();
  }
  return {};
}
auto ex::Freqs::operator[](char c) const -> Count
{
  auto i = idx(c);
  assert(i.has_value());
  return counts_[i.value()];
}
