#include "atbash_cipher.h"

#include <algorithm>
#include <cctype>
#include <ranges>

#if _cpp_lib_ranges_chunk

namespace rg = std::ranges;
namespace vw = rg::views;

namespace {
  ///< Transform one alnum
  auto one(char c) -> char
  {
    if (c <= '9') {
      return c;
    }
    return 'z' - std::tolower(c) + 'a';
  }
  ///< decode to range
  auto dec(std::string_view msg)
  {
    // clang-format off
    return msg
      | vw::filter(::isalnum)
      | vw::transform(one);
    // clang-format on
  }
  ///< construct string from range
  auto to_s(auto r) -> std::string
  {
    std::string result;
    rg::copy(r, std::back_inserter(result));
    return result;
  }
} // namespace

auto atbash_cipher::encode(std::string_view plain) -> std::string
{
  auto r = dec(plain) | vw::chunk(5) | vw::join_with(' ');
  return to_s(r);
}
auto atbash_cipher::decode(std::string_view msg) -> std::string
{
  return to_s(dec(msg));
}

#endif // _cpp_lib_ranges_chunk
