#if !defined(ATBASH_CIPHER_H)
#define ATBASH_CIPHER_H

#include <string>
#include <string_view>

/*! Atbash Cipher exercize
 */
namespace atbash_cipher {
  /*! @param plain The unencoded message
   *  @return encoded message
   */
  auto encode(std::string_view plain) -> std::string;
  /*! @return The unencoded message
   *  @param msg encoded message
   */
  auto decode(std::string_view msg) -> std::string;
} // namespace atbash_cipher

#endif // ATBASH_CIPHER_H
