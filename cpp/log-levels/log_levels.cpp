#include <string>
#include <string_view>

namespace log_line {
  constexpr std::string_view sep{"]: "};
  auto message(std::string line) -> std::string
  {
    return line.substr(line.find(sep) + 3);
  }

  auto log_level(std::string line) -> std::string
  {
    return line.substr(1, line.find(sep) - 1UL);
  }

  auto reformat(std::string line) -> std::string
  {
    return message(line) + " (" + log_level(line) + ")";
  }

} // namespace log_line
