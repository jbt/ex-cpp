#include "reverse_string.hpp"

namespace {
  namespace an_empty_string {
    static_assert("" == reverse_string::reverse_string(""));
  }

  namespace a_word {
    static_assert("tobor" == reverse_string::reverse_string("robot"));
  }

  namespace a_capitalized_word {
    static_assert("nemaR" == reverse_string::reverse_string("Ramen"));
  }

  namespace a_sentence_with_punctuation {
    static_assert("!yrgnuh m'I" ==
                  reverse_string::reverse_string("I'm hungry!"));
  }

  namespace a_palindrome {
    static_assert("racecar" == reverse_string::reverse_string("racecar"));
  }
} // namespace
