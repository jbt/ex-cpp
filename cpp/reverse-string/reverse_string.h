#if !defined(REVERSE_STRING_H)
#define REVERSE_STRING_H

#include <string>

/*! Reverse String exercize
 */
namespace reverse_string {
  /*! @param in A string to reverse
   *  @return Copy of in, but reverse
   */
  inline auto reverse_string(std::string_view in) -> std::string
  {
    return {in.rbegin(), in.rend()};
  }
} // namespace reverse_string

#endif // REVERSE_STRING_H
