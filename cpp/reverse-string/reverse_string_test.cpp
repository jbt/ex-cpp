#include "reverse_string.h"
#ifdef EXERCISM_TEST_SUITE
#include <catch2/catch.hpp>
#else
#include "test/catch.hpp"
#endif

TEST_CASE("an_empty_string")
{
    CHECK("" == reverse_string::reverse_string(""));
}

TEST_CASE("a_word")
{
    CHECK("tobor" == reverse_string::reverse_string("robot"));
}

TEST_CASE("a_capitalized_word")
{
    CHECK("nemaR" == reverse_string::reverse_string("Ramen"));
}

TEST_CASE("a_sentence_with_punctuation")
{
    CHECK("!yrgnuh m'I" == reverse_string::reverse_string("I'm hungry!"));
}

TEST_CASE("a_palindrome")
{
   CHECK("racecar" == reverse_string::reverse_string("racecar"));
}
