#pragma once

#include <iostream>
#include <stdexcept>
#include <string>
#include <string_view>

namespace reverse_string
{
    /*! View into a string, but backwards
     */
    class reverse_string//_view
    {
    public:
        /*! @param forward The string to be reversed
         *  @note forward must outlive *this
         *  @note This reverses code units with no consideration to encoding
         *      e.g. breaks UTF-8
         */
        constexpr reverse_string( std::string_view forward )
        : forward_{forward}
        {}

        /*! @return a code unit
         *  @param i index in reverse order
         */
        constexpr char operator[]( std::size_t i ) const { return ref(i); }

        /*! @return a code unit
         *  @param i index in reverse order
         *  @throw std::out_of_range if i is not valid
         *  @note The throw turns into a compiler error in consteval
         */
        constexpr char at( std::size_t i ) const
        {
            if ( bounds_check(i) )
            {
                return ref( i );
            }
            else
            {
                throw std::out_of_range( std::to_string(i) );
            }
        }

        /*! @return count of code units in string
         */
        constexpr std::size_t size() const { return forward_.size(); }

        /*! @return Iterator to the (backwards) "first" code unit
         */
        constexpr auto begin() const { return forward_.rbegin(); }

        /*! @return Iterator to the (backwards) "last" code unit
         */
        constexpr auto end()   const { return forward_.rend()  ; }

        /*! @param rhs right-hand side
         *  @return true iff rhs is reversed from the underlying string
         */
        constexpr bool operator==( std::string_view rhs ) const
        {
            //std::mismatch is not constexpr
            if ( rhs.size() != size() )
                return false;
            auto i = begin();
            for ( auto c : rhs )
            {
                if ( c != *(i++) )
                    return false;
            }
            return true;
        }

    private:
        std::string_view const forward_;

        constexpr bool bounds_check( std::size_t index ) const
        {
            return index < size();
        }
        constexpr char ref( std::size_t index ) const
        {
            return forward_[size()-index-1ul];
        }
    };

    /*! Stream a backwards-string
     *  @param o Stream to write to
     *  @param s String to write
     *  @return o
     */
    inline std::ostream& operator<<( std::ostream& o, reverse_string const& s )
    {
        for ( auto c : s )
        {
            o << c;
        }
        return o;
    }
}

/*! @return equivalence
 *  @param s left-hand side
 *  @param r right-hand side
 */
constexpr bool operator==( std::string_view s, reverse_string::reverse_string r )
{
    return r == s;
}
