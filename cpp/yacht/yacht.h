#pragma once

#include <cstdint>

#include <array>
#include <string_view>

namespace yacht {
  /*! The value of a 1D6 roll
   */
  using Die = std::uint8_t;
  /*! The values of five D6 rolls
   */
  using Dice = std::array<Die, 5>;
  /*! The score the player gets from these dice if scored as the given category
   *  @param dice the values rolled on the dice
   *  @param score_as the chosen category
   *  @return The calculated score
   */
  short score(Dice dice, std::string_view score_as);
} // namespace yacht
