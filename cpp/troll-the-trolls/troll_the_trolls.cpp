#include "troll_the_trolls.hpp"

using A = hellmath::AccountStatus;

static_assert(valid_player_combination(A::troll, A::troll));
static_assert(!valid_player_combination(A::troll, A::guest));
static_assert(!valid_player_combination(A::troll, A::user));
static_assert(!valid_player_combination(A::troll, A::mod));

static_assert(!valid_player_combination(A::guest, A::troll));
static_assert(!valid_player_combination(A::guest, A::guest));
static_assert(!valid_player_combination(A::guest, A::user));
static_assert(!valid_player_combination(A::guest, A::mod));

static_assert(!valid_player_combination(A::user, A::troll));
static_assert(!valid_player_combination(A::user, A::guest));
static_assert(valid_player_combination(A::user, A::user));
static_assert(valid_player_combination(A::user, A::mod));

static_assert(!valid_player_combination(A::mod, A::troll));
static_assert(!valid_player_combination(A::mod, A::guest));
static_assert(valid_player_combination(A::mod, A::user));
static_assert(valid_player_combination(A::mod, A::mod));
