#pragma once

#include <utility>

#if __cpp_lib_unreachable
#define UNREACHABLE std::unreachable()
#else
#define UNREACHABLE return {}
#endif

namespace hellmath {
/*! The stati an account could have.
 */
enum class AccountStatus : char {
    troll = '0', ///< A user who has been flagged for bad behavior
    guest, ///< An anonymous user who has not logged in
    user, ///< Normal user
    mod ///< Moderators
};

/*! Actions which are restricted to some kinds of users.
 */
enum class Action : char {
    read = 'r',
    write,
    remove
};

/*! @return Whether a post should be displayed to a user
 *  @param  poster The user who created the post.
 *  @param  viewer The user who may or may not see it
 *  @details Helma has noticed that it is no use to ban troll accounts.
 *      Her strategy is to give them the illusion that their time is "well invested",
 *      but their posts are only shown to other trolls.
 */
constexpr bool display_post(AccountStatus poster,AccountStatus viewer)  {
    if ( poster == AccountStatus::troll )
        return viewer == AccountStatus::troll;
    return true;
}

/*! @return Whether a given user's role has a certain permission
 *  @param  action The action the user would like to take
 *  @param  status The user's role
 *  @details There are four types of accounts, each with different default permissions:
 *    Guests          : can read posts.
 *    Users and Trolls: can read and write posts.
 *    Moderators      : can read, write and remove posts, they have all the permissions.
 *  @pre Both action and status contain a valid value from their enums
*/
constexpr bool permission_check(Action action,AccountStatus status) {
    switch (status) {
    case AccountStatus::guest:
        return action == Action::read;
    case AccountStatus::user:
        [[fallthrough]];
    case AccountStatus::troll:
        return action != Action::remove;
    case AccountStatus::mod:
        return true;
    }
    UNREACHABLE;
}

/*! @return Whether p1's actions are strictly ahead of p2 in sort order
 *  @param p1 A player
 *  @param p2 Another player
 *  @details To handle emergencies, moderators are given the highest priority.
 *      Guests are queued behind normal users, and trolls get sorted behind everyone else.
 */
constexpr bool has_priority(AccountStatus p1,AccountStatus p2)  {
    return static_cast<int>(p1) > static_cast<int>(p2);
}

/*! @return Whether these players may start a game together
 *  @param p1 A player
 *  @param p2 The other player
 *  @details To keep the actual players in the game accountable for their actions,
 *      Hellmath denies access to guest users.
 *      As mentioned above, Helma wants trolls to troll other trolls.
 *      Game connections between other users are unrestricted.
 *  @pre Both p1 and p2 have a valid value from the enum
 */
constexpr bool valid_player_combination(AccountStatus p1,AccountStatus p2)  {
    if (has_priority(p1, p2)) {
        return valid_player_combination(p2, p1);
    }
    switch (p1) {
    case AccountStatus::troll:
        return p2 == AccountStatus::troll;
    case AccountStatus::guest:
        return false;
    default:
        return true;
    }
    UNREACHABLE;
}
}
