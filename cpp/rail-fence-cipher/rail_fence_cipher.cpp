#include "rail_fence_cipher.h"

#include <ranges>

#if __cpp_lib_ranges_stride

namespace rf = rail_fence_cipher;
namespace vw = std::ranges::views;

auto rf::encode(std::string_view plaintext, int num_rails) -> std::string
{
  if (num_rails < 1) {
    return {};
  }
  auto period = num_rails * 2 - 2;
  auto first_line = plaintext | vw::stride(period);
  auto result = std::string{first_line.begin(), first_line.end()};
  for (auto above = 1; above < num_rails - 1; ++above) {
    auto below = num_rails - above - 1;
    auto falling = plaintext | vw::drop(above) | vw::stride(period);
    auto skip_twice =
        below - 1; // Lines below you that aren't the bottom get hit twice
                   // between your falling & rising edges
    auto rising_skip = above            // position of falling
                       + 1              // skip your own first (falling) pass
                       + skip_twice * 2 // skip all those
                       + 1              // skip the bottom row, too
        ;
    auto rising = plaintext | vw::drop(rising_skip) | vw::stride(period);
    for (auto [f, r] : vw::zip(falling, rising)) {
      result.push_back(f);
      result.push_back(r);
    }
    if (falling.size() > rising.size()) {
      result.push_back(falling.back());
    }
  }
  auto last_line = plaintext | vw::drop(num_rails - 1) | vw::stride(period);
  result.append(last_line.begin(), last_line.end());
  return result;
}
namespace {
  void zig_zag(unsigned width, unsigned N, auto foo)
  {
    for (auto x = 0U; x < width;) {
      for (auto y = 0U; y < N && x < width; ++x, ++y) {
        // falling
        foo(y);
      }
      for (auto y = N - 2U; y && x < width; ++x, --y) {
        foo(y);
      }
    }
  }
} // namespace
auto rf::decode(std::string_view ciphertext, unsigned num_rails) -> std::string
{
  std::array<unsigned, 16> rails;
  rails.fill(0U);
  auto zz = [&](auto f) { zig_zag(ciphertext.size(), num_rails, f); };
  zz([&rails](auto y) { rails[y + 1]++; });
  // at this point rails carries lengths (widths) of each previous rail
  // for the first 2 rails, that's also an offset
  for (auto i = 1U; i < num_rails; ++i) {
    rails[i + 1] += rails[i];
  }
  // Now it's offsets into ciphertext
  std::string result;
  zz([&](auto y) {
    auto c = ciphertext.at(rails[y]++);
    result.push_back(c);
  });
  return result;
}

#endif // __cpp_lib_ranges_stride
