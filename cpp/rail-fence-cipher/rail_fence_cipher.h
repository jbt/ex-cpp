#pragma once

#include <string>

/*! Rails Fence Cipher exercize
 */
namespace rail_fence_cipher {
  /*! Apply the cipher
   *  @param plaintext original message
   *  @param num_rails how many rails to use in the cipher
   *  @return ciphered text
   */
  std::string encode(std::string_view plaintext, int num_rails);
  /*! Restore the text
   *  @param ciphertext text
   *  @param num_rails  how many rails originally used in the cipher
   *  @return plain text
   */
  std::string decode(std::string_view ciphertext, unsigned num_rails);

} // namespace rail_fence_cipher
