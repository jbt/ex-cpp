#pragma once

#include <string>
#include <string_view>

namespace run_length_encoding {
  auto encode(std::string_view raw) -> std::string;
  auto decode(std::string_view raw) -> std::string;
} // namespace run_length_encoding
