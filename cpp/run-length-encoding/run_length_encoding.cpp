#include "run_length_encoding.h"

#include <cstdlib>

#include <sstream>

namespace rle = run_length_encoding;

auto rle::encode(std::string_view raw) -> std::string
{
  std::ostringstream result;
  for (auto i = 0U; i < raw.size();) {
    auto j = raw.find_first_not_of(raw[i], i);
    auto d = j == std::string_view::npos ? raw.size() - i : j - i;
    if (d == 1UL) {
      result << raw[i];
      ++i;
    } else {
      result << d << raw[i];
      i += d;
    }
  }
  return result.str();
}
auto rle::decode(std::string_view raw) -> std::string
{
  std::string rv;
  auto b = raw.data();
  for (auto e = b + raw.size(); b < e; ++b) {
    if (std::isdigit(*b)) {
      char *n;
      auto count = std::strtol(b, &n, 10);
      b = n;
      rv.append(count, *b);
    } else {
      rv.push_back(*b);
    }
  }
  return rv;
}
