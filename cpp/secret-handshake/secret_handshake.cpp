
#include "secret_handshake.h"

#include <algorithm>
#include <array>
#include <bitset>
#include <ranges>

#if __cpp_lib_ranges_enumerate

namespace rg = std::ranges;
namespace vw = rg::views;

namespace {
  using namespace std::literals;
  std::array key{"wink"sv, "double blink"sv, "close your eyes"sv, "jump"sv};
  using Enumerated = std::tuple<std::size_t, std::string_view>;
  auto xfrm(Enumerated const &e)
  {
    return std::get<1>(e);
  }
} // namespace

auto secret_handshake::commands(unsigned long n) -> std::vector<std::string>
{
  std::bitset<5> const bits{n};
  auto indicated = [bits](Enumerated const &p) {
    return bits.test(std::get<0>(p));
  };
  // clang-format off
  auto rng = key
    | vw::enumerate
    | vw::filter(indicated)
    | vw::transform(xfrm)
    ;
  // clang-format on
  std::vector<std::string> result(rng.begin(), rng.end());
  if (bits.test(4)) {
    std::reverse(result.begin(), result.end());
  }
  return result;
}

#endif
