#pragma once

#include <string>
#include <vector>

/*! Diamond exercize
 */
namespace diamond {
  /*! @return ASCII art letter diamond
   *  @param mid The character of the middle row
   */
  std::vector<std::string> rows(char mid);
} // namespace diamond
