#include "diamond.h"

#include <algorithm>

auto diamond::rows(char mid) -> std::vector<std::string>
{
  std::vector<std::string> result;
  auto const sz = (mid - 'A') * 2UL + 1UL;
  result.resize(sz, std::string(sz, ' '));
  auto const mid_i = mid - 'A';
  for (char c = 'A'; c <= mid; ++c) {
    auto dx = c - 'A';
    auto dy = c - mid;
    result[mid_i - dy][mid_i - dx] = c;
    result[mid_i + dy][mid_i - dx] = c;
    result[mid_i - dy][mid_i + dx] = c;
    result[mid_i + dy][mid_i + dx] = c;
  }
  return result;
}
