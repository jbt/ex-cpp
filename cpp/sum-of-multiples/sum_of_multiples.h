#if !defined(SUM_OF_MULTIPLES_H)
#define SUM_OF_MULTIPLES_H

#include <queue>
#include <vector>

namespace sum_of_multiples { ///< Sum of Multiples exercize

  /*! A magical item which was collected
   */
  class Item {
    long next_;
    long const base_;

  public:
    /*! Construct a magic item
     * @param base_value
     *   The value of the item we'll take multiples of
     * @note This is intentionally not explicit
     */
    Item(long base_value);

    /*! Sort order
     * @param rhs
     *   The right-hand side of the < operator
     * @return Whether *this should come before rhs
     */
    auto operator<=>(Item const &rhs) const = default;

    /*! @return next multiple of base value
     */
    [[nodiscard]] auto peek() const -> long { return next_; }

    /*! Discard current multiple, move on to the one after that.
     * @param val
     *   Take no action if val is not the current value.
     */
    void pop(long val);
  };
  auto to(std::vector<Item> items, long const level) -> long;
} // namespace sum_of_multiples

#endif // SUM_OF_MULTIPLES_H
