#include "sum_of_multiples.h"

#include <algorithm>
#include <iostream>

namespace rg = std::ranges;
using Item = sum_of_multiples::Item;

auto sum_of_multiples::to(std::vector<Item> items, long const level) -> long
{
  if (items.empty()) {
    return 0L;
  }
  auto result = 0L;
  for (long val = 0; val < level;) {
    result += val;
    auto next_b = rg::min_element(items);
    val = next_b->peek();
    std::for_each(next_b, items.end(), [val](auto &i) { i.pop(val); });
  }
  return result;
}

Item::Item(long base)
: next_{base}
, base_{base}
{
}

void Item::pop(long val)
{
  if (next_ == val) {
    next_ += base_;
  }
}
