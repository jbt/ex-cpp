#include "series.h"

#include <algorithm>
#include <ranges>
#include <stdexcept>

#if __cpp_lib_ranges_slide

namespace vw = std::ranges::views;

auto series::slice(std::string_view digits, unsigned length)
    -> std::vector<std::string>
{
  if (!length) {
    throw std::domain_error{"Zero length."};
  }
  if (length > digits.size()) {
    throw std::domain_error{"Length too large."};
  }
  auto as_str = [](auto v) { return std::string(v.begin(), v.end()); };
  // clang-format off
  auto result = digits
    | vw::slide(length)
    | vw::transform(as_str);
  return {result.begin(), result.end()};
}

#endif
