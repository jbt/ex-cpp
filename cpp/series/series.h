#if !defined(SERIES_H)
#define SERIES_H

#include <string>
#include <string_view>
#include <vector>

/*! Series exercize
 */
namespace series {
  /*! Find all subslices of a given size of the string
   * @param  digits the thing to slice up
   * @param  length the length of the slices to return
   * @return all windows, in order
   * @throws std::domain_error if length == 0 || length > digits.size()
   */
  auto slice(std::string_view digits, unsigned length)
      -> std::vector<std::string>;
} // namespace series

#endif // SERIES_H
