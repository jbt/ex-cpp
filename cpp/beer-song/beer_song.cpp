#include "beer_song.h"

#include <sstream>
#include <string_view>

namespace bs = beer_song;
using namespace std::literals;

namespace {
  void verse(std::ostream& s, short which);
}

auto bs::verse(short which) -> std::string
{
  std::ostringstream result;
  ::verse(result, which);
  return result.str();
}
auto bs::sing(short start, short end) -> std::string
{
  std::ostringstream result;
  for (auto i = start; i >= end; --i) {
    ::verse(result, i);
    if (i > end) {
      result.put('\n');
    }
  }
  return result.str();
}

namespace {
  auto bottles(std::ostream& s, short int which) -> std::ostream&
  {
    if (which) {
      s << which;
    } else {
      s << "no more"sv;
    }
    s << " bottle"sv;
    if (which != 1) {
      s.put('s');
    }
    return s << " of beer"sv;
  }
  auto one(short which) -> std::string_view
  {
    return which > 1 ? "one"sv : "it"sv;
  }
  void verse(std::ostream& s, short which)
  {
    if (!which) {
      s << "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"sv;
      return;
    }
    bottles(s, which) << " on the wall, "sv;
    bottles(s, which) << ".\nTake " << one(which)
                      << " down and pass it around, "sv;
    bottles(s, which - 1) << " on the wall.\n"sv;
  }
} // namespace
