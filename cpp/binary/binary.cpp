#include "binary.h"

auto binary::convert(std::string_view str) -> std::uintmax_t
{
  std::uintmax_t result = 0U;
  for (auto d : str) {
    result <<= 1;
    switch (d) {
    case '1': result |= 1;
    case '0': break;
    default: return 0U;
    }
  }
  return result;
}
