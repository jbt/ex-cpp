#pragma once

#include <string_view>
#include <cstdint>

namespace binary
{
    constexpr std::size_t convert( std::string_view ones_and_zeros )
    {
        auto rv = std::size_t{};
        for ( auto c : ones_and_zeros )
        {
            rv <<= 1;
            switch ( c )
            {
            default:
                return 0;//Invalid character
            case '1':
                ++rv;
            case '0':
                ;
            }
        }
        return rv;
    }
}

static_assert( binary::convert("10010") == 0b10010, "Basic Usage" );
