#if !defined(BINARY_H)
#define BINARY_H

#include <cstdint>
#include <string_view>

/*! The binary exercize
 */
namespace binary {
  /*! Convert a string of '1' and '0' to a native integer representation
   *  @param str The string representation (in binary) of the number
   *  @return The integer as a native value
   */
  auto convert(std::string_view str) -> std::uintmax_t;
} // namespace binary

#endif // BINARY_H
