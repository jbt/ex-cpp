#!/bin/bash -e
if [ "$1" = '--verbose' ]
then
  export verbose=echo
else
  export verbose=true
fi
s=`dirname "${0}"`
mkdir -p certs
subm() {
  sleep 1
  echo "${d}" >&2 
  d="${1}"
  (
    cd "${d}" 
	git add .
    shopt -s nullglob
	exercism submit --timeout 999 *.{h,hpp,cpp} 2>&1
  ) > exercism-submit.out
}
find "${s}" -type d -name .exercism -printf '%h\n' | while read d
do
  sleep 1
  n=`basename "${d}"`
  short="certs/${n}.submitcheck"
  if grep '// *WIP' "${d}/"*.cpp
  then
    date > "${short}"
    continue
  fi
  if [ ! -f "${short}" ]
  then
    date > "${short}"
  else
    ts=`stat --format=%Y "${short}"`
    if [ -f certs/recheck.rate ]
    then
        export rate=`cat certs/recheck.rate`
    else
        export rate=0
    fi
    if [ $[ RANDOM * rate + ts ] -ge `date +%s` ]
    then
      ${verbose} "Skip ${n} for now."
    else
      rm -v "${short}"
      echo -n $((++rate)) > certs/recheck.rate
    fi
    continue
  fi
  cid=`ipfs add --ignore CMakeLists.txt --quieter --raw-leaves --cid-version 1 --recursive "${d}"`
  cert="certs/${cid}.submit"
  if [ -f "${cert}" ]
  then
    touch "${cert}"
    echo "${n} " `cat "${cert}"`
  elif subm "${d}"
  then
    echo "Submitted ${n}"
    sleep 2
  elif grep -q 'Error: No files you submitted have changed since your last submission' exercism-submit.out
  then
    git -C "${s}" add "${d}"
    echo "Done: ${n}" | tee -a submit.hit
    date > "${cert}"
    sleep 4
  elif grep '^HTTP/2.0 [3-9]' exercism-submit.out
  then
    break
  else
    grep -n . exercism-submit.out
    echo what
    exit 1
  fi
done
set -x
if [ -f submit.hit ]
then
  git -C "${s}" commit --all --file="${PWD}/submit.hit"
  timeout 999 git -C "${s}" push || sleep 9
  grep -n . submit.hit
  rm -v submit.hit
elif git -C "${s}" status --porcelain | grep -E '^.[M\?]'
then
  git -C "${s}" status --porcelain | grep -vF cobol | grep -F '?' | cut -d / -f 2 | while read n
  do
    rm -v "certs/${n}.submitcheck" && break
  done
else
  ls -tr certs/* | head -n 1 | xargs rm -v
fi
