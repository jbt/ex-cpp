#if !defined(PRIME_FACTORS_H)
#define PRIME_FACTORS_H

#include <vector>

/*! Prime Factors exercize
 */
namespace prime_factors {
  /*! @param n The number in question
   *  @return n's prime factors
   */
  std::vector<long long> of(long long n);
} // namespace prime_factors

#endif // PRIME_FACTORS_H
