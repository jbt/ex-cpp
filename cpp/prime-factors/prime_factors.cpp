#include "prime_factors.h"

auto prime_factors::of(long long n) -> std::vector<long long>
{
  std::vector<long long> result;
  for (auto p = 2; p <= n;) {
    if (n % p == 0) {
      result.push_back(p);
      n /= p;
    } else {
      ++p;
    }
  }
  return result;
}
