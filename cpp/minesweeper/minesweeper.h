#pragma once

#include <array>
#include <initializer_list>
#include <string>
#include <string_view>

/*! Minesweeper exercize
 */
namespace minesweeper {
  /*! Represents a Minesweeper grid
   */
  class Grid {
    std::size_t height_ = 0UL;
    std::size_t width_ = 0UL;
    std::string data_;

    void count_mines(int x, int y);
    bool mine(int x, int y) const;

  public:
    /*! Create the game's grid from static input
     *  @param in static input
     */
    explicit Grid(std::initializer_list<std::string_view> in);
    /*! Check for equivalence
     *  @param expect A 2-dimensional array-like structure to compare to
     *  @return true iff *this is equivalent to expect
     */
    bool operator==(auto const &expect) const
    {
      if (expect.size() != height_) {
        return false;
      }
      for (auto y = 0U; y < height_; ++y) {
        if (expect[y] != row(y)) {
          return false;
        }
      }
      return true;
    }
    /*! Read-only access for a row
     *  @param index The 0-based index of the row
     *  @return view into the row
     */
    std::string_view row(std::size_t index) const;
  };
  /*! Add those numbers into those squres
   *  @param inp The input
   *  @return annotated grid
   */
  auto annotate(std::initializer_list<std::string_view> inp) -> Grid;
} // namespace minesweeper
