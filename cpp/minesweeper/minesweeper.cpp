#include "minesweeper.h"

namespace ms = minesweeper;
using Grd = ms::Grid;

Grd::Grid(std::initializer_list<std::string_view> in)
{
  for (auto row : in) {
    data_.append(row);
    ++height_;
    width_ = row.size();
  }
  for (auto y = 0U; y < height_; ++y) {
    for (auto x = 0U; x < width_; ++x) {
      count_mines(x, y);
    }
  }
}
auto Grd::row(std::size_t index) const -> std::string_view
{
  return std::string_view{data_}.substr(index * width_, width_);
}
auto Grd::mine(int x, int y) const -> bool
{
  if (x < 0 || y < 0) {
    return false;
  }
  if (static_cast<unsigned>(x) >= width_) {
    return false;
  }
  if (static_cast<unsigned>(y) >= height_) {
    return false;
  }
  return row(y).at(x) == '*';
}
void Grd::count_mines(int x, int y)
{
  auto &c = data_.at(x + y * width_);
  if (c != ' ') {
    return;
  }
  c = 0;
  for (auto yy : {y - 1, y, y + 1}) {
    for (auto xx : {x - 1, x, x + 1}) {
      if (mine(xx, yy)) {
        ++c;
      }
    }
  }
  if (c) {
    c += '0';
  } else {
    c = ' ';
  }
}

auto ms::annotate(std::initializer_list<std::string_view> inp) -> Grd
{
  return Grd{inp};
}
