#pragma once

#include <boost/container/small_vector.hpp>

#include <algorithm>
#include <vector>

/*! Arcade-style-game related code?
 */
namespace arcade {

  /*! Frogger high-score info
   */
  class HighScores {
  private:
    std::vector<int> const &scores;

  public:
    /*! A container of scores.
     *  @note Optimized for sizes 3 or less
     */
    using Vec = boost::container::small_vector<int, 3>;

    /*! Construct from score history
     *  @param scores
     *      chronologically ordered game scores.
     *      Must outlive *this
     */
    explicit HighScores(std::vector<int> const &scores);

    /*! @return all scores, in order
     */
    std::vector<int> const &list_scores() const;
    /*! @return score of the most-recently finished game
     */
    int latest_score() const;
    /*! @return highest score
     */
    int personal_best() const;
    /*! @return The top-3 highest scores, in descending order
     */
    Vec top_three() const;
  };

} // namespace arcade

template <class Value, std::size_t N>
constexpr bool operator==(boost::container::small_vector<Value, N> const &a,
                          std::vector<Value> const &v)
{
  return std::ranges::equal(a, v);
}
