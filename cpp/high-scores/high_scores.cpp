#include "high_scores.h"

using HS = arcade::HighScores;

namespace rg = std::ranges;

HS::HighScores(std::vector<int> const &v)
: scores{v}
{
}

auto HS::list_scores() const -> std::vector<int> const &
{
  return scores;
}

auto HS::latest_score() const -> int
{
  if (scores.empty()) {
    return -1;
  }
  return scores.back();
}

auto HS::personal_best() const -> int
{
  return *rg::max_element(scores);
}

auto HS::top_three() const -> Vec
{
  if (scores.size() <= 3U) {
    Vec result(scores.begin(), scores.end());
    rg::sort(result, std::greater{});
    return result;
  }
  auto result = Vec{-1, -1, -1};
  auto min_last = [&result]() {
    for (auto i : {0U, 1U}) {
      if (result[i] < result.back()) {
        std::swap(result[i], result.back());
      }
    }
  };
  for (auto score : scores) {
    if (score > result.back()) {
      result.back() = score;
      min_last();
    }
  }
  if (result[1] > result[0]) {
    std::swap(result[0], result[1]);
  }
  return result;
}
