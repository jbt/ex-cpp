#pragma once

#include <cassert>

#include <memory>

/*! Linked List exercize
 */
namespace linked_list {
  /*! A doubly-linked list.
   */
  template<class Value>
  class List {
    /*! A node in the list
     */
    struct Node {
      std::unique_ptr<Node> next_ = {};
      Node* prev_ = nullptr;
      Value const val_;
      Node(Value v, Node* nxt)
      : prev_{nxt}
      , val_{v}
      {}
      std::size_t count() const {
        return 1UL + (next_ ? next_->count() : 0UL);
      }
      Node* remove(Value v) {
        if (!next_) {
          return nullptr;
        }
        if (next_->val_ == v) {
          next_.reset(next_->next_.release());
          if (next_) {
            next_->prev_ = this;
            return nullptr;
          } else {
            //I am the new tail
            return this;
          }
        } else {
          return next_->remove(v);
        }
      }
    };
    std::unique_ptr<Node> head_ = {};
    Node* tail_ = nullptr;

  public:
    /*! Append a value
     * @param v The value
     */
    void push(Value v) {
      auto nw = std::make_unique<Node>(v, tail_);
      auto tl = nw.get();
      if (empty()) {
        head_.swap(nw);
      } else {
        tail_->next_.swap(nw);
      }
      tail_ = tl;
    }
    /*! Prepend a value
     * @param v The value
     */
    void unshift(Value v) {
      auto nw = std::make_unique<Node>(v, nullptr);
      if (empty()) {
        tail_ = nw.get();
      } else {
        head_->prev_ = nw.get();
        nw->next_.swap(head_);
      }
      head_ = std::move(nw);
    }
    /*! Pop the back
     * @return The removed value
     */
    Value pop() {
      assert(!empty());
      auto result = tail_->val_;
      if (single()) {
        clear();
      } else {
        tail_ = tail_->prev_;
        tail_->next_.reset();
      }
      return result;
    }
    /*! Pop the front
     * @return The removed value
     */
    Value shift() {
      assert(!empty());
      auto result = head_->val_;
      if (single()) {
        clear();
      } else {
        head_.reset(head_->next_.release());
        head_->prev_ = nullptr;
      }
      return result;
    }
    /*! Remove all values
     */
    void clear() {
      head_.reset();
      tail_ = nullptr;
    }
    /*! Remove the first element whose value matches exactly
     * @param v The value to match against
     */
    void erase(Value v) {
      if (empty()) {
        return;
      }
      if (head_->val_ == v) {
        shift();
      } else if(auto t = head_->remove(v)) {
          tail_ = t;

      }
    }

    /*! @return whether the list is empty
     */
    bool empty() const {
      assert((!head_) == (!tail_));
      return !head_;
    }
    /*! @return whether the list contains a single element
     */
    bool single() const {
      return head_.get() == tail_;
    }
    /*! @return the size of the list
     * @note Currently O(N). Could easily be O(1)
     */
    std::size_t count() const {
      if (empty()) {
        return 0UL;
      }
      //TODO constant time
      return head_->count();
    }
  };
}  // namespace linked_list
