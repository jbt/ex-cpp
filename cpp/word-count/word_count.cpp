#include "word_count.h"

#include <cctype>

#include <algorithm>

auto word_count::words(std::string_view text) -> std::map<std::string, int>
{
  constexpr std::string_view DELIMITERS{" \n\t\r,:!?.&@$%^\""};
  std::map<std::string, int> result;
  for (auto i = 0U; i < text.size();) {
    if (text[i] == '\'') {
      ++i;
    }
    auto sp = text.find_first_of(DELIMITERS, i);
    auto key_v = text.substr(i, sp - i);
    if (key_v.size()) {
      if (key_v.back() == '\'') {
        key_v.remove_suffix(1);
      }
      std::string key{key_v};
      std::transform(key.begin(), key.end(), key.begin(), ::tolower);
      result[key]++;
    }
    i = text.find_first_not_of(DELIMITERS, sp);
  }
  return result;
}
