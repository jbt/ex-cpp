#if !defined(CIRCULAR_BUFFER_H)
#define CIRCULAR_BUFFER_H

#include <array>
#include <stdexcept>

/*! Circular Buffer exercize
 */
namespace circular_buffer {
  /*! A queue, implemented as a circular buffer with a fixed capacity
   */
  template <class Value, unsigned Capacity>
  class circular_buffer {
    std::array<Value, Capacity> data_;
    unsigned off_ = 0U;
    unsigned siz_ = 0U;

  public:
    /*! @return whether the queue contains zero elements
     */
    bool empty() const { return !siz_; }
    /*! @return whether the queue contains capacity() elements
     */
    bool full() const { return siz_ == capacity(); }
    /*! @return how many elements the queue CAN contain, at most
     */
    std::size_t capacity() const { return data_.size(); }
    /*! Pop the front element
     * @return the popped element
     */
    Value read()
    {
      if (empty()) {
        throw std::domain_error{"empty"};
      }
      --siz_;
      if (off_ + 1UL == capacity()) {
        off_ = 0;
        return data_.back();
      } else {
        return data_[off_++];
      }
    }
    /*! push a new element to the back of the queue
     * @param v the value of the new element
     */
    void write(Value &&v)
    {
      if (full()) {
        throw std::domain_error{"full"};
      }
      data_[(off_ + siz_++) % capacity()] = v;
    }
    /*! reset the queue back to empty
     */
    void clear() { siz_ = off_ = 0U; }
    /*! add the element, removing the eldest element if necessary
     * @param v the value of the element to add
     */
    void overwrite(Value &&v)
    {
      if (full()) {
        read();
      }
      write(std::move(v));
    }
  };
} // namespace circular_buffer

#endif // CIRCULAR_BUFFER_H
