#include "pangram.h"

using namespace std::literals;

namespace sentence_empty {
  static_assert(!pangram::is_pangram(""));
}

namespace pangram_with_only_lower_case {
  static_assert(
      pangram::is_pangram("the quick brown fox jumps over the lazy dog"sv));
}

namespace missing_character_x {
  static_assert(!pangram::is_pangram(
      "a quick movement of the enemy will jeopardize five gunboats"sv));
}

namespace another_missing_x {
  static_assert(
      !pangram::is_pangram("the quick brown fish jumps over the lazy dog"));
}

namespace pangram_with_underscores {
  static_assert(
      pangram::is_pangram("the_quick_brown_fox_jumps_over_the_lazy_dog"));
}

namespace pangram_with_numbers {
  static_assert(
      pangram::is_pangram("the 1 quick brown fox jumps over the 2 lazy dogs"));
}

namespace missing_letters_replaced_with_numbers {
  static_assert(
      !pangram::is_pangram("7h3 qu1ck brown fox jumps ov3r 7h3 lazy dog"));
}

namespace pangram_with_mixed_case_and_punctuation {
  static_assert(
      pangram::is_pangram("\"Five quacking Zephyrs jolt my wax bed.\""));
}

namespace upper_and_lower_should_not_be_counted_seperately {
  static_assert(
      !pangram::is_pangram("the quick brown fox jumps over with lazy FX"));
}
