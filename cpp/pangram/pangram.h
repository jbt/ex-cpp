#pragma once

#include <cstdint>
#include <string_view>

namespace pangram {
  constexpr bool is_pangram(std::string_view checked)
  {
    std::uint_fast32_t hit = 0U;
    for (auto c : checked) {
      if (c >= 'A' && c <= 'Z') {
        hit |= (1 << (c - 'A'));
      }
      if (c >= 'a' && c <= 'z') {
        hit |= (1 << (c - 'a'));
      }
    }
    return hit == 0b11111111111111111111111111;
  }
} // namespace pangram
