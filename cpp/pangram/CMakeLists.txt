project(
  pangram
  LANGUAGES CXX
)

find_package(Catch2 REQUIRED)
find_package(Boost REQUIRED)
find_package(Threads REQUIRED)

file(
  GLOB
  srcs
  *.h
  *.cpp
)
file(
  GLOB
  test_srcs
  *_test.cpp
)
list(REMOVE_ITEM srcs ${test_srcs})

add_library(
  pangram_exercize
  ${srcs}
)
target_compile_features(
  pangram_exercize
  PUBLIC
    cxx_std_23
)
target_compile_options(
  pangram_exercize
  PUBLIC
    -fsanitize=undefined
    -O0 -g3 -ggdb3 -fno-inline
)
target_compile_options(
  pangram_exercize
  PRIVATE
    -Wall
    -Wextra
    -Wpedantic
    -Werror
)
target_link_libraries(
  pangram_exercize
  PUBLIC
    Boost::headers
    Threads::Threads
    ubsan
)

include(clang-tools)
clang_tooling(pangram_exercize)

foreach(t few all slow)
  add_executable(
    pangram_${t}_runner
    ${test_srcs}
    ../lasagna/test/tests-main.cpp
  )
  target_link_libraries(
    pangram_${t}_runner
    PUBLIC
      pangram_exercize
      # Boost::headers
      Catch2::Catch2
  )
  add_custom_command(
    OUTPUT ${t}.cert
    COMMAND timeout ${MAX_TEST_TIME} $<TARGET_FILE:pangram_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.cert
    DEPENDS pangram_${t}_runner
    COMMENT "pangram_${t} passed"
  )
  add_custom_command(
    OUTPUT ${t}.memcheck.cert
    COMMAND timeout ${MAX_VALGRIND_TEST_TIME} valgrind --exit-on-first-error=yes --error-exitcode=99 --leak-check=full --track-origins=yes --quiet $<TARGET_FILE:pangram_${t}_runner>
    COMMAND ${CMAKE_COMMAND} -E touch ${t}.memcheck.cert
    DEPENDS pangram_${t}_runner
    COMMENT "pangram_${t} passed under valgrind"
  )
endforeach()
target_compile_definitions(
  pangram_all_runner
  PUBLIC
    EXERCISM_RUN_ALL_TESTS=1
)
target_compile_definitions(
  pangram_slow_runner
  PUBLIC
    EXERCISM_INCLUDE_BENCHMARK=1
    CATCH_CONFIG_ENABLE_BENCHMARKING=1
)

foreach(t few all)
  add_custom_target(
    check_pangram_${t}
    ALL
    DEPENDS ${t}.cert ${t}.memcheck.cert
    COMMENT "pangram ${t} tests."
  )
endforeach()
add_custom_target(
  check_pangram_slow
  ALL
  DEPENDS slow.cert
  COMMENT "pangram Slow/benchmark tests."
)
add_dependencies(check_pangram_all  check_pangram_few)
add_dependencies(check_pangram_slow check_pangram_all)
