#include "acronym.h"

auto acronym::acronym(std::string_view full) -> std::string
{
  std::string result;
  auto next = true;
  for (auto c : full) {
    if (std::isalnum(c)) {
      if (next) {
        result.push_back(std::toupper(c)); // NOLINT
      }
      next = false;
    } else {
      next = true;
    }
  }
  return result;
}
