#pragma once

#include <string>
#include <string_view>

namespace acronym
{
   std::string acronym( std::string_view full );
}
