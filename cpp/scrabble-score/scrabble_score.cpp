#include "scrabble_score.h"

// Since these tests are all evaluated at compile time
// leave nothing for the linker even
// I think it's probably appropriate to go ahead and include them in the main
// project source

namespace scores_an_empty_word_as_zero {
  static_assert(0 == scrabble_score::score(""));
}

namespace scores_a_very_short_word {
  static_assert(1 == scrabble_score::score("a"));
}

namespace scores_the_word_by_the_number_of_letters {
  static_assert(6 == scrabble_score::score("street"));
}

namespace scores_more_complicated_words_with_more {
  static_assert(22 == scrabble_score::score("quirky"));
}

namespace scores_case_insensitive_words {
  static_assert(41 == scrabble_score::score("OXYPHENBUTAZONE"));
}

namespace scores_z_word {
  static_assert(12 == scrabble_score::score("zoo"));
}
