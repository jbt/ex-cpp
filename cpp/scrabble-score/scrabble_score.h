#pragma once

#include <stdexcept>
#include <string_view>

/*! Scrabble Score exercize
 */
namespace scrabble_score {

  /*! @param letter The letter of a Scrabble tile
   *  @return That letter's score
   */
  constexpr int score(char letter)
  {
    switch (letter) {
    case ' ': return 0;
    case 'A':
    case 'E':
    case 'I':
    case 'O':
    case 'U':
    case 'L':
    case 'N':
    case 'R':
    case 'S':
    case 'T': return 1;
    case 'D':
    case 'G': return 2;
    case 'B':
    case 'C':
    case 'M':
    case 'P': return 3;
    case 'F':
    case 'H':
    case 'V':
    case 'W':
    case 'Y': return 4;
    case 'K': return 5;
    case 'J':
    case 'X': return 8;
    case 'Q':
    case 'Z': return 10;
    default:
      if (letter >= 'a' && letter <= 'z') { // Handle lowercase
        return score(static_cast<char>(letter - 'a' + 'A'));
      } else {
        throw std::invalid_argument(std::string{"Not a letter:"} + letter);
      }
    }
  }

  /*! @param word A word one has spelled with scrabble tiles
   *  @return That word's score, if no bonus is applied
   */
  constexpr int score(std::string_view word)
  {
    auto rv = 0;
    for (auto letter : word) {
      rv += score(letter);
    }
    return rv;
  }
} // namespace scrabble_score
