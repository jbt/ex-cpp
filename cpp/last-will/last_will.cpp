// Secret knowledge of the Zhang family:
namespace zhang {
  auto bank_number_part(int secret_modifier) -> int
  {
    int const zhang_part{8'541};
    return (zhang_part * secret_modifier) % 10000;
  }
  namespace red {
    auto code_fragment() -> int
    {
      return 512;
    }
  } // namespace red
  namespace blue {
    auto code_fragment() -> int
    {
      return 677;
    }
  } // namespace blue
} // namespace zhang

// Secret knowledge of the Khan family:
namespace khan {
  auto bank_number_part(int secret_modifier) -> int
  {
    int const khan_part{4'142};
    return (khan_part * secret_modifier) % 10000;
  }
  namespace red {
    auto code_fragment() -> int
    {
      return 148;
    }
  } // namespace red
  namespace blue {
    auto code_fragment() -> int
    {
      return 875;
    }
  } // namespace blue
} // namespace khan

// Secret knowledge of the Garcia family:
namespace garcia {
  auto bank_number_part(int secret_modifier) -> int
  {
    int const garcia_part{4'023};
    return (garcia_part * secret_modifier) % 10000;
  }
  namespace red {
    auto code_fragment() -> int
    {
      return 118;
    }
  } // namespace red
  namespace blue {
    auto code_fragment() -> int
    {
      return 923;
    }
  } // namespace blue
} // namespace garcia

namespace estate_executor {
  auto assemble_account_number(int secret_modifier) -> int
  {
    // clang-format off
    return zhang::bank_number_part(secret_modifier)
          + khan::bank_number_part(secret_modifier)
        + garcia::bank_number_part(secret_modifier)
             ;
  }
  auto assemble_code() -> int {
   return ( zhang::red::code_fragment()
           + khan::red::code_fragment()
         + garcia::red::code_fragment()
          )
        *
          ( zhang::blue::code_fragment()
           + khan::blue::code_fragment()
         + garcia::blue::code_fragment()
          )
        ;
  }
} // namespace estate_executor
