#include "meetup.h"

auto meetup::scheduler::last(DayOfWeek wd) const -> Date
{
  return g::last_day_of_the_week_in_month{wd, month_}.get_date(year_);
}
