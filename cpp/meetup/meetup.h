#if !defined(MEETUP_H)
#define MEETUP_H

#include <boost/date_time/gregorian/greg_calendar.hpp>
#include <boost/date_time/gregorian/greg_date.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>

#include <ranges>

namespace meetup {

  namespace g = boost::gregorian;
  using Date = g::date;
  using Days = g::date_duration;
  using Greg = g::gregorian_calendar;

  using DayOfMonth = Greg::day_type;
  using DayOfWeek = Greg::day_of_week_type;
  using WDay = DayOfWeek::weekday_enum;
  using Month = Greg::month_type;
  using Year = Greg::year_type;
  using YMD = Greg::ymd_type;

  /*! Finds dates for events with particular recurrence schedule formulae
   */
  class scheduler {
    Month const month_;
    Year const year_;
    using DoMi = unsigned short;

    constexpr Date find(DoMi b, DoMi e, DayOfWeek wd) const
    {
      namespace v = std::ranges::views;
      for (auto dom : v::iota(b, e)) {
        DayOfMonth day{dom};
        YMD date{year_, month_, day};
        if (Greg::day_of_week(date) == wd) {
          return Date{date};
        }
      }
      return Date{};
    }
    constexpr Date teenth(DayOfWeek wd) const
    {
      return find(static_cast<DoMi>(13), static_cast<DoMi>(20), wd);
    }
    constexpr Date first(DayOfWeek wd) const
    {
      return find(static_cast<DoMi>(1), static_cast<DoMi>(8), wd);
    }
    constexpr Date second(DayOfWeek wd) const { return first(wd) + Days(7); }
    constexpr Date third(DayOfWeek wd) const { return first(wd) + Days(7 * 2); }
    constexpr Date fourth(DayOfWeek wd) const
    {
      return first(wd) + Days(7 * 3);
    }
    Date last(DayOfWeek wd) const;

  public:
    /*! Construct for a given calendar month
     *  @param yr  Which year is being considered
     *  @param mon Which month of that year
     */
    constexpr explicit scheduler(Month mon, Year yr)
    : month_{mon}
    , year_{yr}
    {
    }

    //! @cond

    constexpr auto sunteenth() const { return teenth(WDay::Sunday); }
    constexpr auto monteenth() const { return teenth(WDay::Monday); }
    constexpr auto tuesteenth() const { return teenth(WDay::Tuesday); }
    constexpr auto wednesteenth() const { return teenth(WDay::Wednesday); }
    constexpr auto thursteenth() const { return teenth(WDay::Thursday); }
    constexpr auto friteenth() const { return teenth(WDay::Friday); }
    constexpr auto saturteenth() const { return teenth(WDay::Saturday); }

    constexpr auto first_sunday() const { return first(WDay::Sunday); }
    constexpr auto first_monday() const { return first(WDay::Monday); }
    constexpr auto first_tuesday() const { return first(WDay::Tuesday); }
    constexpr auto first_wednesday() const { return first(WDay::Wednesday); }
    constexpr auto first_thursday() const { return first(WDay::Thursday); }
    constexpr auto first_friday() const { return first(WDay::Friday); }
    constexpr auto first_saturday() const { return first(WDay::Saturday); }

    constexpr auto second_sunday() const { return second(WDay::Sunday); }
    constexpr auto second_monday() const { return second(WDay::Monday); }
    constexpr auto second_tuesday() const { return second(WDay::Tuesday); }
    constexpr auto second_wednesday() const { return second(WDay::Wednesday); }
    constexpr auto second_thursday() const { return second(WDay::Thursday); }
    constexpr auto second_friday() const { return second(WDay::Friday); }
    constexpr auto second_saturday() const { return second(WDay::Saturday); }

    constexpr auto third_sunday() const { return third(WDay::Sunday); }
    constexpr auto third_monday() const { return third(WDay::Monday); }
    constexpr auto third_tuesday() const { return third(WDay::Tuesday); }
    constexpr auto third_wednesday() const { return third(WDay::Wednesday); }
    constexpr auto third_thursday() const { return third(WDay::Thursday); }
    constexpr auto third_friday() const { return third(WDay::Friday); }
    constexpr auto third_saturday() const { return third(WDay::Saturday); }

    constexpr auto fourth_sunday() const { return fourth(WDay::Sunday); }
    constexpr auto fourth_monday() const { return fourth(WDay::Monday); }
    constexpr auto fourth_tuesday() const { return fourth(WDay::Tuesday); }
    constexpr auto fourth_wednesday() const { return fourth(WDay::Wednesday); }
    constexpr auto fourth_thursday() const { return fourth(WDay::Thursday); }
    constexpr auto fourth_friday() const { return fourth(WDay::Friday); }
    constexpr auto fourth_saturday() const { return fourth(WDay::Saturday); }

    auto last_sunday() const { return last(WDay::Sunday); }
    auto last_monday() const { return last(WDay::Monday); }
    auto last_tuesday() const { return last(WDay::Tuesday); }
    auto last_wednesday() const { return last(WDay::Wednesday); }
    auto last_thursday() const { return last(WDay::Thursday); }
    auto last_friday() const { return last(WDay::Friday); }
    auto last_saturday() const { return last(WDay::Saturday); }

    //! @endcond
  };
} // namespace meetup

#endif // MEETUP_H
