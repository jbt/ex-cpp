#include "queen_attack.h"

using Brd = queen_attack::chess_board;

Brd::operator std::string() const
{
  std::string board{"_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"
                    "_ _ _ _ _ _ _ _\n"};
  auto at = [&board](auto p) -> char & {
    return board.at(16 * p.first + p.second * 2);
  };
  at(white()) = 'W';
  at(black()) = 'B';
  return board;
}

namespace queens_in_default_positions {
  constexpr Brd board;

  static_assert(std::make_pair(0, 3).first == board.white().first);
  static_assert(std::make_pair(0, 3).second == board.white().second);
  static_assert(std::make_pair(7, 3).first == board.black().first);
  static_assert(std::make_pair(7, 3).second == board.black().second);
} // namespace queens_in_default_positions
namespace initialized_with_specific_positions {
  constexpr auto white = std::make_pair(3, 7);
  constexpr auto black = std::make_pair(6, 1);
  constexpr Brd board{white, black};

  static_assert(white.first == board.white().first);
  static_assert(white.second == board.white().second);
  static_assert(black.first == board.black().first);
  static_assert(black.second == board.black().second);
} // namespace initialized_with_specific_positions

namespace queens_cannot_attack {
  constexpr Brd board{std::make_pair(2, 3), std::make_pair(4, 7)};

  static_assert(!board.can_attack());
} // namespace queens_cannot_attack

namespace queens_can_attack_when_they_are_on_the_same_row {
  constexpr Brd board{std::make_pair(2, 4), std::make_pair(2, 7)};

  static_assert(board.can_attack());
} // namespace queens_can_attack_when_they_are_on_the_same_row

namespace queens_can_attack_when_they_are_on_the_same_column {
  constexpr Brd board{std::make_pair(5, 4), std::make_pair(2, 4)};

  static_assert(board.can_attack());
} // namespace queens_can_attack_when_they_are_on_the_same_column

namespace queens_can_attack_diagonally {
  constexpr Brd board{std::make_pair(1, 1), std::make_pair(6, 6)};

  static_assert(board.can_attack());
} // namespace queens_can_attack_diagonally

namespace queens_can_attack_another_diagonally {
  constexpr Brd board{std::make_pair(0, 6), std::make_pair(1, 7)};

  static_assert(board.can_attack());
} // namespace queens_can_attack_another_diagonally

namespace queens_can_attack_yet_another_diagonally {
  constexpr Brd board{std::make_pair(4, 1), std::make_pair(6, 3)};

  static_assert(board.can_attack());
} // namespace queens_can_attack_yet_another_diagonally

namespace queens_can_attack_on_the_nw_so_diagonal {
  constexpr Brd board{std::make_pair(1, 6), std::make_pair(6, 1)};

  static_assert(board.can_attack());
} // namespace queens_can_attack_on_the_nw_so_diagonal

namespace queens_cannot_attack_if_not_on_same_row_column_or_diagonal {
  constexpr Brd board{std::make_pair(1, 1), std::make_pair(3, 7)};

  static_assert(!board.can_attack());
} // namespace queens_cannot_attack_if_not_on_same_row_column_or_diagonal
