#if !defined(QUEEN_ATTACK_H)
#define QUEEN_ATTACK_H 1

#include <optional>
#include <stdexcept>
#include <string>
#include <utility>

/*! Queen Attack exercize
 */
namespace queen_attack {

  /*! A position on a chess board
   *  @todo There should be a !NDEBUG version of this that does bounds checking
   */
  using Position = std::pair<short, short>;
  /*! A chess board with the 2 queens, and only they, on it.
   */
  class chess_board {
    Position const white_;
    Position const black_;

  public:
    /*! Construct from queen positions
     *  @param w The position of the white queen
     *  @param b The position of the black queen
     */
    constexpr chess_board(Position w = {0, 3}, Position b = {7, 3})
    : white_{w}
    , black_{b}
    {
      if (w == b) {
        throw std::domain_error{"Two pieces cannot be on same tile."};
      }
    }

    /*! @return The position of the white queen
     */
    constexpr Position white() const { return white_; }
    /*! @return The position of the black queen
     */
    constexpr Position black() const { return black_; }
    /*! Slope of a line extending from the white queen to the black queen
     *  @return nullopt if it's a vertical line, as the slope is undefined in
     * that case
     */
    constexpr std::optional<short> slope() const
    {
      if (white().first == black().first) {
        return std::nullopt;
      }
      return (black().second - white().second) /
             (black().first - white().first);
    }

    /*! @return whether the next queen to move can take the other
     */
    constexpr bool can_attack() const
    {
      if (auto m = slope()) {
        return *m == 0 || *m == 1 || *m == -1;
      } else {
        return true; // vertical
      }
    }

    /*! @return an ASCII art visualization of the board
     */
    operator std::string() const;
  };
} // namespace queen_attack

#endif // QUEEN_ATTACK_H
