#pragma once

#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <variant>

/*! The Triangle exercize
 */
namespace triangle {

  /*! Numeric type for the length of a triangle's side.
   */
  using length = double;
  /*! Categorization of triangles.
   */
  enum class flavor { equilateral, isosceles, scalene };

  /*! A triangle with specific dimensions
   */
  class sized {
    std::array<length, 3> lengths_;

  public:
    /*! Construct from the lengths of the sides of the triangles, in any order.
     *  @param a one side's length
     *  @param b one side's length
     *  @param c one side's length
     */
    constexpr sized(length a, length b, length c)
    : lengths_{a, b, c}
    {
      auto order = [](auto& l, auto& r) {
        if (l > r) {
          std::swap(l, r);
        }
      };
      order(lengths_[0], lengths_[1]);
      order(lengths_[0], lengths_[2]);
      order(lengths_[1], lengths_[2]);
      if (shortest() < 0.) {
        throw std::domain_error("Negative numbers are not valid lengths for "
                                "the side of a triangle: " +
                                std::to_string(shortest()));
      }
      if (!std::isnormal(shortest())) {
        throw std::domain_error("Zero (and similar) not valid lengths for the "
                                "side of a triangle: " +
                                std::to_string(shortest()));
      }
      if (!std::isnormal(longest())) {
        throw std::domain_error("Infinity (and similar) not valid lengths for "
                                "the side of a triangle: " +
                                std::to_string(longest()));
      }
      if (shortest() + middlest() <= longest()) {
        throw std::domain_error("Violated triangle inequality.");
      }
    }

    /*! @return the least of the lengths of sides
     */
    constexpr length shortest() const { return lengths_[0]; }
    /*! @return the greatest of the lengths of sides
     */
    constexpr length longest() const { return lengths_[2]; }
    /*! @return the length of the side which is neither the longest nor shortest
     */
    constexpr length middlest() const { return lengths_[1]; }

    /*! @return whether two lengths roughly equal
     *  @param a One triangle side length
     *  @param b One triangle side length
     */
    constexpr bool equals(length a, length b) const
    {
      return std::abs(a - b) / shortest() < 1e-9;
    }
  };

  /*! @return what flavor (categorization) the triangle falls into
   *  @param to_test The triangle to check
   */
  constexpr flavor kind(sized to_test)
  {
    if (to_test.equals(to_test.shortest(), to_test.longest()))
      return flavor::equilateral;
    if (to_test.equals(to_test.shortest(), to_test.middlest()))
      return flavor::isosceles;
    if (to_test.equals(to_test.middlest(), to_test.longest()))
      return flavor::isosceles;
    return flavor::scalene;
  }

  /*! Convenience overload
   *  @param a A length of a side of the triangle
   *  @param b A length of a side of the triangle
   *  @param c A length of a side of the triangle
   *  @return what flavor (categorization) the triangle falls into
   */
  constexpr flavor kind(length a, length b, length c)
  {
    return kind({a, b, c});
  }

  /*! Support out-streaming, e.g. for Boost.Test assertions
   *    @param stream Stream to write to
   *    @param c The triangle flavor to represent
   *    @return stream
   */
  inline std::ostream& operator<<(std::ostream& stream, flavor c)
  {
    switch (c) {
    case flavor::equilateral: return stream << "Equilateral";
    case flavor::isosceles: return stream << "isosceles";
    case flavor::scalene: return stream << "scalene";
    default:
      throw std::logic_error(std::to_string(static_cast<short>(c)) +
                             " is not a triangle type.");
    }
  }

  /*! Constant provided for compatibility with test interface expectations */
  constexpr auto equilateral = flavor::equilateral;
  /*! Constant provided for compatibility with test interface expectations */
  constexpr auto isosceles = flavor::isosceles;
  /*! Constant provided for compatibility with test interface expectations */
  constexpr auto scalene = flavor::scalene;
} // namespace triangle
