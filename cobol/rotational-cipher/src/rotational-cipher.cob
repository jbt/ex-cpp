       IDENTIFICATION DIVISION.
       PROGRAM-ID. rotational-cipher.
      * Apply a rotational cipher
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * PRE: The rotation to apply.
      *POST: Unchanged
       01 WS-KEY PIC 9(2) USAGE COMP-5.
      * PRE: The text to encode
      *POST: Please ignore. (uppercased)
       01 WS-TEXT PIC X(128).
      * PRE: Ignored
      *POST: The ciphered text.
       01 WS-CIPHER PIC X(128).

      * Below variables are all internal

      * CONSTANT - the Latin alphabet
       01 abet PIC A(26) VALUE "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
      * The alphabet, post rotation
       01 shifted PIC A(26).
      * Just an 'pointer' into shifted
       01 shifted-ptr PIC 99 USAGE COMP-5.
      * 1-based position in shifted one finds 'A'
       01 a-pos PIC 99 USAGE COMP-5.
      * How many characters to copy in next statement
       01 runlen PIC 99 USAGE COMP-5.


       PROCEDURE DIVISION.
       ROTATIONAL-CIPHER.
           MOVE 1 TO shifted-ptr.
           COMPUTE a-pos = 1 + FUNCTION MOD(WS-KEY, 26).
           COMPUTE runlen = 26 - FUNCTION MOD(WS-KEY, 26).
           STRING abet(a-pos:runlen) 
                  INTO shifted 
                  WITH POINTER shifted-ptr.
           COMPUTE runlen = 26 - runlen.
           STRING abet(1:runlen)
                  INTO shifted
                  WITH POINTER shifted-ptr.
           MOVE FUNCTION UPPER-CASE(WS-TEXT) TO WS-CIPHER.
           INSPECT WS-CIPHER CONVERTING abet TO shifted.
