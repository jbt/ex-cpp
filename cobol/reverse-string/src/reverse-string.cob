       IDENTIFICATION DIVISION.
       PROGRAM-ID. reverse-string.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-STRING PIC X(64).
       01 inp REDEFINES WS-STRING
              PIC X OCCURS 64 TIMES 
              INDEXED BY i .
       
       01 buf_s PIC X(64).
       01 buf REDEFINES buf_s PIC X OCCURS 64 TIMES INDEXED BY j.

       PROCEDURE DIVISION.
       REVERSE-STRING.
      * Reverse a string and display result
           INITIALIZE buf_s.
           SET j TO 1.
           PERFORM VARYING i FROM 64 BY -1 UNTIL i = 0
              IF j > 1 OR inp(i) IS NOT EQUAL TO ' '
                 MOVE inp(i) TO buf(j)
                 ADD 1 TO j
           END-PERFORM.
           MOVE buf_s to WS-STRING.
           DISPLAY WS-STRING.
