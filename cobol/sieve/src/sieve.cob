       IDENTIFICATION DIVISION.
       PROGRAM-ID. SIEVE.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Pre : The highest number to include in WS-RESULT (if it's prime)
      * Post: Unchanged
       01 WS-LIMIT PIC 9999.
      * Pre : Ignored
      * Post: For indices < WS-COUNT, The prime numbers <= WS-LIMIT, beyond that please ignore
       01 WS-RESULT PIC 999 OCCURS 1000 TIMES. 
      * Pre : Ignored
      * Post: How many prime numbers are to be found in WS-RESULT
       01 WS-COUNT PIC 9999.

      * Internal
      *01 byte PIC 99 USAGE COMP-5.
       01 sieve-flags.
          05 flag PIC 9 USAGE COMP-5 OCCURS 8192 TIMES INDEXED BY i.

       01 cand PIC 9999 USAGE COMP-5.
       01 mult PIC 9999 USAGE COMP-5.

       PROCEDURE DIVISION.
       SIEVE.
           INITIALIZE sieve-flags.
           MOVE 0 TO WS-COUNT.
           MOVE 1 TO cand.
           PERFORM check UNTIL cand >= WS-LIMIT.
       check.
           ADD 1 TO cand.
           IF flag(cand) = 1
              EXIT PARAGRAPH.
           ADD 1 TO WS-COUNT.
           MOVE cand TO WS-RESULT(WS-COUNT).
           MULTIPLY cand BY 2 GIVING mult.
           PERFORM UNTIL mult > WS-LIMIT
              MOVE 1 TO flag(mult)
              ADD cand TO mult
           END-PERFORM.
