       IDENTIFICATION DIVISION.
       PROGRAM-ID. LEAP.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-YEAR PIC 9999.
       01 WS-RESULT PIC 9.
       PROCEDURE DIVISION.
       LEAP.
         INITIALIZE WS-RESULT.
         IF FUNCTION MOD(WS-YEAR, 400) = 0
      *    If a year is evenly-divisible by 400, it IS a leap year. e.g. 2000
           MOVE 1 TO WS-RESULT
         ELSE IF FUNCTION MOD(WS-YEAR, 100) = 0
      *    Otherwise, if it's evenly-divisible by 100, it is not a leap year. e.g. 2100
           MOVE 0 TO WS-RESULT
         ELSE IF FUNCTION MOD(WS-YEAR, 4) = 0
      *    In the 99% case, just check if it's evenly divisible by 4
           MOVE 1 TO WS-RESULT
         ELSE
           MOVE 0 TO WS-RESULT
         EXIT.
       LEAP-EXIT.
         EXIT.
