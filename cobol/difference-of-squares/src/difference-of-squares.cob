       IDENTIFICATION DIVISION.
       PROGRAM-ID. difference-of-squares.
      * Paragraphs related to squaring summing and subtracting
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      * PRE : The maximum natural number to include in the calculation
      * POST: Please ignore
       01 WS-NUMBER PIC 9(8) USAGE COMP-5.

      * PRE : Ignored
      * POST: Result of SQUARE-OF-SUM paragraph
       01 WS-SQUARE-OF-SUM PIC 9(18) USAGE COMP-5.

      * PRE : Ignored
      * POST: Result of SUM-OF-SQUARES paragraph
       01 WS-DIFFERENCE-OF-SQUARES PIC 9(18) USAGE COMP-5.

      * PRE : Ignored
      * POST: Result of DIFFERENCE-OF-SQUARES paragraph
       01 WS-SUM-OF-SQUARES PIC 9(8) USAGE COMP-5.

       01 num PIC 9(8) USAGE COMP-5.
       01 square PIC 9(9) USAGE COMP-5.

       PROCEDURE DIVISION.
       
       SQUARE-OF-SUM.
           INITIALIZE WS-SQUARE-OF-SUM.
           PERFORM VARYING num FROM 1 UNTIL num > WS-NUMBER
              ADD num TO WS-SQUARE-OF-SUM
           END-PERFORM.
           MULTIPLY WS-SQUARE-OF-SUM BY WS-SQUARE-OF-SUM.
       
       SUM-OF-SQUARES.
           INITIALIZE WS-SUM-OF-SQUARES.
           PERFORM VARYING num FROM 1 UNTIL num > WS-NUMBER
              MULTIPLY num BY num GIVING square
              ADD square TO WS-SUM-OF-SQUARES
           END-PERFORM.
       
       DIFFERENCE-OF-SQUARES.
           PERFORM SQUARE-OF-SUM.
           PERFORM SUM-OF-SQUARES.
           SUBTRACT 
             WS-SUM-OF-SQUARES 
             FROM WS-SQUARE-OF-SUM 
             GIVING WS-DIFFERENCE-OF-SQUARES.
