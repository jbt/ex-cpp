       IDENTIFICATION DIVISION.
       PROGRAM-ID. rna-transcription.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-COMPLEMENT PIC X(64).

       01 i USAGE INDEX.
          88 past-end VALUE 65 THRU 99.
       PROCEDURE DIVISION.
       RNA-TRANSCRIPTION.
           SET i TO 1.
           PERFORM UNTIL past-end
              EVALUATE WS-COMPLEMENT(i:1)
              WHEN 'G'
                 STRING 'C' INTO WS-COMPLEMENT WITH POINTER i
              WHEN 'C'
                 STRING 'G' INTO WS-COMPLEMENT WITH POINTER i
              WHEN 'T'
                 STRING 'A' INTO WS-COMPLEMENT WITH POINTER i
              WHEN 'A'
                 STRING 'U' INTO WS-COMPLEMENT WITH POINTER i
              WHEN OTHER
                 ADD 1 TO i
              END-EVALUATE
           END-PERFORM.
