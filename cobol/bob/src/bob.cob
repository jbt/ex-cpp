       IDENTIFICATION DIVISION.
       PROGRAM-ID. BOB.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-HEYBOB                PIC X(60).
       01 WS-RESULT                PIC X(40).

       01 i PIC 99 USAGE COMP-5.
          88 past-end VALUES 61 THRU 99.
       01 flags.
          05 has-upper PIC 9.
          05 has-lower PIC 9.
       01 last-char PIC X.
       PROCEDURE DIVISION.
       BOB.
           INITIALIZE flags.
           MOVE ' ' TO last-char.
           PERFORM classify VARYING i FROM 1 UNTIL past-end.
           EVALUATE TRUE ALSO TRUE ALSO TRUE
           WHEN last-char = ' ' ALSO TRUE ALSO TRUE
              MOVE 'Fine. Be that way!' TO WS-RESULT
           WHEN last-char = '?' ALSO has-lower = 1 ALSO TRUE
              MOVE 'Sure.' TO WS-RESULT
           WHEN last-char = '?' ALSO has-upper = 0 ALSO TRUE
              MOVE 'Sure.' TO WS-RESULT
           WHEN last-char = '?' ALSO has-lower = 0 ALSO has-upper = 1
              MOVE "Calm down, I know what I'm doing!" TO WS-RESULT
           WHEN TRUE ALSO has-upper = 1 ALSO has-lower = 0
              MOVE 'Whoa, chill out!' TO WS-RESULT
           WHEN OTHER
              MOVE 'Whatever.' TO WS-RESULT
           END-EVALUATE.

       classify.
           IF WS-HEYBOB(i:1) IS NOT EQUAL TO ' '
              MOVE WS-HEYBOB(i:1) TO last-char
           ELSE
              EXIT PARAGRAPH.
           EVALUATE WS-HEYBOB(i:1)
           WHEN 'a' MOVE 1 TO has-lower
           WHEN 'b' MOVE 1 TO has-lower
           WHEN 'c' MOVE 1 TO has-lower
           WHEN 'd' MOVE 1 TO has-lower
           WHEN 'e' MOVE 1 TO has-lower
           WHEN 'f' MOVE 1 TO has-lower
           WHEN 'g' MOVE 1 TO has-lower
           WHEN 'h' MOVE 1 TO has-lower
           WHEN 'i' MOVE 1 TO has-lower
           WHEN 'j' MOVE 1 TO has-lower
           WHEN 'k' MOVE 1 TO has-lower
           WHEN 'l' MOVE 1 TO has-lower
           WHEN 'm' MOVE 1 TO has-lower
           WHEN 'n' MOVE 1 TO has-lower
           WHEN 'o' MOVE 1 TO has-lower
           WHEN 'p' MOVE 1 TO has-lower
           WHEN 'q' MOVE 1 TO has-lower
           WHEN 'r' MOVE 1 TO has-lower
           WHEN 's' MOVE 1 TO has-lower
           WHEN 't' MOVE 1 TO has-lower
           WHEN 'u' MOVE 1 TO has-lower
           WHEN 'v' MOVE 1 TO has-lower
           WHEN 'w' MOVE 1 TO has-lower
           WHEN 'x' MOVE 1 TO has-lower
           WHEN 'y' MOVE 1 TO has-lower
           WHEN 'z' MOVE 1 TO has-lower
           WHEN 'A' MOVE 1 TO has-upper
           WHEN 'B' MOVE 1 TO has-upper
           WHEN 'C' MOVE 1 TO has-upper
           WHEN 'D' MOVE 1 TO has-upper
           WHEN 'E' MOVE 1 TO has-upper
           WHEN 'F' MOVE 1 TO has-upper
           WHEN 'G' MOVE 1 TO has-upper
           WHEN 'H' MOVE 1 TO has-upper
           WHEN 'I' MOVE 1 TO has-upper
           WHEN 'J' MOVE 1 TO has-upper
           WHEN 'K' MOVE 1 TO has-upper
           WHEN 'L' MOVE 1 TO has-upper
           WHEN 'M' MOVE 1 TO has-upper
           WHEN 'N' MOVE 1 TO has-upper
           WHEN 'O' MOVE 1 TO has-upper
           WHEN 'P' MOVE 1 TO has-upper
           WHEN 'Q' MOVE 1 TO has-upper
           WHEN 'R' MOVE 1 TO has-upper
           WHEN 'S' MOVE 1 TO has-upper
           WHEN 'T' MOVE 1 TO has-upper
           WHEN 'U' MOVE 1 TO has-upper
           WHEN 'V' MOVE 1 TO has-upper
           WHEN 'W' MOVE 1 TO has-upper
           WHEN 'X' MOVE 1 TO has-upper
           WHEN 'Y' MOVE 1 TO has-upper
           WHEN 'Z' MOVE 1 TO has-upper
           END-EVALUATE.
