       IDENTIFICATION DIVISION.
       PROGRAM-ID. hamming.
      * Calculate the Hamming distance between two DNA strands
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Pre : The left DNA strand 
      * Post: Unchanged
       01 WS-DNA-1 PIC X(32).
      * Pre : The right DNA strand 
      * Post: Unchanged
       01 WS-DNA-2 PIC X(32).
      * Pre : Ignored
      * Post: The hamming distance, or 0 if there was an error
       01 WS-HAMMING PIC 9(2) USAGE COMP-5.
      * Pre : Ignored
      * Post: Description of the error, or SPACES if there was none.
       01 WS-ERROR PIC X(31).

      * The following are internal variables.

      * The single allele being considered from the left strand
       01 l PIC X.
      * The single allele being considered from the right strand
       01 r PIC X.
      * 1-based offset of the alleles being considered
       01 i USAGE INDEX.
      * Whether we've scanned the entire inputs
          88 done VALUES 33 THRU 99.
       PROCEDURE DIVISION.
       HAMMING.
           MOVE 0 to WS-HAMMING.
           INITIALIZE WS-ERROR.
           PERFORM VARYING i FROM 1 UNTIL done
              MOVE WS-DNA-1(i:1) TO l
              MOVE WS-DNA-2(i:1) TO r
              IF l = ' ' AND r = ' '
                 EXIT
              ELSE IF l = ' ' OR r = ' '
                 MOVE 'Strands must be of equal length' TO WS-ERROR
                 MOVE 0 to WS-HAMMING
              ELSE IF l IS NOT EQUAL TO r
                 ADD 1 TO WS-HAMMING
              END-IF
           END-PERFORM.
