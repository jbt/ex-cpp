       IDENTIFICATION DIVISION.
       PROGRAM-ID. nucleotide-count.
      * Count each nucleotide in a DNA strand
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       REPOSITORY. FUNCTION ALL INTRINSIC.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Input: Some combination of the letters ACGT followed by space padding.
       01 WS-DNA PIC X(128).
      * Output: Count of A appearing in WS-DNA
       01 WS-A PIC 9(3) USAGE COMP-5.
      * Output: Count of C appearing in WS-DNA
       01 WS-C PIC 9(3) USAGE COMP-5.
      * Output: Count of G appearing in WS-DNA
       01 WS-G PIC 9(3) USAGE COMP-5.
      * Output: Count of T appearing in WS-DNA
       01 WS-T PIC 9(3) USAGE COMP-5.
      * Output: Error message, or blank if none occurred.
       01 WS-ERROR PIC X(36).

      * Internal variables:

      * Index into WS-DNA
       01 i USAGE INDEX.
          88 past-end VALUES 129 THRU 222.
       PROCEDURE DIVISION.
       NUCLEOTIDE-COUNT.
           INITIALIZE WS-A WS-C WS-G WS-T.
           MOVE SPACES TO WS-ERROR.
           PERFORM tally-nucleotide VARYING i FROM 1 UNTIL past-end.
       tally-nucleotide.
           EVALUATE WS-DNA(i:1)
           WHEN 'A' 
              ADD 1 TO WS-A
           WHEN 'C' 
              ADD 1 TO WS-C
           WHEN 'G' 
              ADD 1 TO WS-G
           WHEN 'T' 
              ADD 1 TO WS-T
           WHEN ' ' 
              EXIT
           WHEN other
              MOVE "ERROR: Invalid nucleotide in strand" TO WS-ERROR
              EXIT
           END-EVALUATE.
