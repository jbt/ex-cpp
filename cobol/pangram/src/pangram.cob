        IDENTIFICATION DIVISION.
        PROGRAM-ID. PANGRAM.
        DATE-COMPILED. Determine whether some 'sentence' is a pangram.
        ENVIRONMENT DIVISION.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
      * ALL-CAPS vars to indicate publicness
      * Input paramter, the sentence to check
        01 WS-SENTENCE PIC X(60).
      * Output paramter: 1 if pangram else 0
        01 WS-RESULT PIC 9.

      * Array of bool-ish, one for each letter of the Latin alphabet
        01 character-presence.
      *   Whether this letter (1=A/a, 2=B/b...) was detected
          05 character-present PIC A OCCURS 26 TIMES.
      * Index into WS-SENTENCE
        01 i PIC 99 USAGE BINARY.
          88 is-not-pangram VALUE 61.
      * An ordinal, converted to index into character-present
        01 j PIC 999 USAGE BINARY.
      * How many unique letters we've encountered
        01 letters-unique-count PIC 99 BINARY.
          88 is-pangram VALUE 26.
        PROCEDURE DIVISION.
        PANGRAM.
      *   Set every letter's seen flag to non-Y value, will treat as false
          INITIALIZE character-presence.
      *   And we have seen 0 of them
          MOVE 0 TO letters-unique-count
      *   Assume false until proven otherwise (inside loop)
          MOVE 0 TO WS-RESULT
      *   Scan the input and mark the letters seen.
          PERFORM note-letter VARYING i FROM 1 UNTIL is-pangra
      -     m OR is-not-pangram.
          IF is-pangram
            MOVE 1 to WS-RESULT
          ELSE
            MOVE 0 to WS-RESULT.
        note-letter.
          MOVE FUNCTION ORD(WS-SENTENCE(i:1)) TO j.
      *   These numeric literals assume we're in some form of unicode or ASCII or whatever
      *     The ordinals are 1 greater than their respective codepoints
          IF j > 65 AND j < 92
      *     Upper-case letters in a contiguous range where A = 66
            SUBTRACT 65 FROM j
          ELSE IF j > 97 AND j < 124
      *     Lower-case letters in a contiguous range where a = 98
            SUBTRACT 97 FROM j
          ELSE
      *     DISPLAY 'ignored letter "' WS-SENTENCE(i:1) '" ordinal value ' j
            EXIT PARAGRAPH.
          IF character-present(j) IS NOT EQUAL TO 'Y'
      *     Encountering a letter we haven't yet seen in this sentence.
            MOVE 'Y' TO character-present(j)
            ADD 1 TO letters-unique-count
