       IDENTIFICATION DIVISION.
       PROGRAM-ID. ISOGRAM.
      * Determines if a word or phrase is an isogram.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * PRE: The word/phrase to check
      * POST:Unchanged
       01 WS-PHRASE PIC X(60).
      * PRE: Ignored
      * POST:1 if it's an isogram, 0 otherwise
       01 WS-RESULT PIC 9.
          88 isnt VALUE 0.

      * Variables below are internal.

      * Booleans indicating which letters were encountered.
       01 letters.
      * The index is the letter (1=A/a, 2=B/b...), 1 if encountered
          05 letter PIC 9 OCCURS 26 TIMES.
      * An index into WS-PHRASE
       01 i PIC 99 USAGE COMP.
          88 is-iso VALUE 61.
      * An index into letter
       01 j PIC 999 USAGE COMP.
       PROCEDURE DIVISION.
       ISOGRAM.
      * Assume isogram until proven otherwise.
           MOVE 1 TO WS-RESULT.
           INITIALIZE letters.
           PERFORM note-letter VARYING i FROM 1 UNTIL is-iso OR isnt.
       note-letter.
           MOVE FUNCTION ORD(WS-PHRASE(i:1)) TO j
           EVALUATE j
           WHEN 98 THROUGH 123
      *       lower-case
      *       ORD('a') = 98, because ascii 'a' is 97, j becomes 1
              SUBTRACT 97 FROM j
           WHEN 66 THROUGH 91
      *       upper-case
      *       ORD('A') = 66, because ascii 'A' is 65, j becomes 1
              SUBTRACT 65 FROM j
           WHEN OTHER
      *       Character is not a letter. Includes all the space padding.
              EXIT PARAGRAPH
           END-EVALUATE.
           IF letter(j) IS EQUAL TO 1
      *       This letter has already been visited earlier in the phrase
      *       condition isnt becomes false ending the loop
              MOVE 0 TO WS-RESULT
           ELSE
      *       First time hitting this letter, take note.
              MOVE 1 TO letter(j).
