       IDENTIFICATION DIVISION.
       PROGRAM-ID. TRIANGLE.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *Incoming
       01 WS-SIDES PIC X(20).
       01 WS-PROPERTY PIC A(11).
      *Outgoing
       01 WS-RESULT PIC 9.

      *Internal
       01 sides OCCURS 3 TIMES.
          05 side PIC S999V99 USAGE COMP-5.
       77 cat PIC A(11).
       PROCEDURE DIVISION.
       TRIANGLE.
           UNSTRING WS-SIDES
                    DELIMITED BY ','
                    INTO side(1) side(2) side(3).
           SORT sides ON ASCENDING KEY side.
           MOVE 0 TO WS-RESULT.
           IF side(1) <= 000.00 OR side(1) + side(2) <= side(3)
      *       DISPLAY 'not a triangle'
              EXIT PARAGRAPH.
           EVALUATE WS-PROPERTY
           WHEN 'equilateral'
              IF side(1) = side(3)
                 MOVE 1 TO WS-RESULT
           WHEN 'isosceles'
              IF side(1) = side(2) OR side(2) = side(3)
                 MOVE 1 TO WS-RESULT
           WHEN 'scalene'
              IF side(1) = side(2) OR side(2) = side(3)
                 MOVE 0 TO WS-RESULT
              ELSE
                 MOVE 1 TO WS-RESULT
           .
