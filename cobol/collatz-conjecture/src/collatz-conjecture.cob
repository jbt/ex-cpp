       IDENTIFICATION DIVISION.
       PROGRAM-ID. collatz-conjecture.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-NUMBER PIC S9(8) USAGE COMP-5.
       01 WS-STEPS PIC 9(4) USAGE COMP-5.
       01 WS-ERROR PIC X(35).

       PROCEDURE DIVISION.
       COLLATZ-CONJECTURE.
           IF WS-NUMBER < 1
              MOVE 'Only positive integers are allowed' TO WS-ERROR
           ELSE
              PERFORM step VARYING WS-STEPS FROM 0 UNTIL WS-NUMBER = 1.

       step.
           IF FUNCTION MOD(WS-NUMBER, 2) IS EQUAL TO 0
              DIVIDE WS-NUMBER BY 2 GIVING WS-NUMBER
           ELSE
              COMPUTE WS-NUMBER = 3 * WS-NUMBER + 1.
