       IDENTIFICATION DIVISION.
       PROGRAM-ID. DARTS.
      * Score a dart throw
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      * Pre : horizontal cartesian coordinate with the origin at the center of the target
      * Post: Unchanged
       01 WS-X PIC 99V9 USAGE BINARY.
      * Pre : vertical cartesian coordinate with the origin at the center of the target
      * Post: Unchanged
       01 WS-Y PIC 99V9 USAGE BINARY.
      * Pre : ignored
      * Post: Points earned from this throw
       01 WS-RESULT PIC 99 USAGE BINARY.

      * Internal: The square of the distance from the center of the target to where the dart landed
       01 r2 PIC 9999V99 USAGE BINARY.

       PROCEDURE DIVISION.
       DARTS.
           COMPUTE r2 = WS-X * WS-X + WS-Y * WS-Y.
      * The outer circle has a radius of 10 units (this is equivalent to the total radius for the entire target)
           IF r2 > 100
      *   If the dart lands outside the target, player earns no points (0 points).
              MOVE 0 TO WS-RESULT
      * the middle circle a radius of 5 units
           ELSE IF r2 > 25
      *   If the dart lands in the outer circle of the target, player earns 1 point.
              MOVE 1 TO WS-RESULT
      * and the inner circle a radius of 1
           ELSE IF r2 > 1
      *   If the dart lands in the middle circle of the target, player earns 5 points.
              MOVE 5 TO WS-RESULT
           ELSE
      *   If the dart lands in the inner circle of the target, player earns 10 points.
              MOVE 10 TO WS-RESULT.