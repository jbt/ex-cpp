       IDENTIFICATION DIVISION.
       PROGRAM-ID. YACHT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * PRE:  The category the user is taking these dice under
      * POST: Unchanged
       01 WS-CATEGORY PIC X(15).
      * PRE:  The values of the dice rolled [1,6]. Not ordered.
      * POST: Please ignore
       01 WS-DICE PIC 9(5).
      * PRE:  Ignored
      * POST: The score of a set of dice given the category applied
       01 WS-RESULT PIC 99 VALUE 0.

      ** NOTE The remaining variables are all internal to the algo

      * The sum of the dice rolled
       01 dice-sum PIC 99.
      * The number of dice rolled for each value (index=value).
      * For example, IF WS-DICE=25231 then dice-counts=121010
       01 dice-counts.
         88 is-little-straight VALUE 111110.
         88 is-big-straight VALUE 011111.
         88 is-yacht VALUES 500000, 050000, 005000, 000500, 000050, 000005.
         05 bucket OCCURS 6 TIMES PIC 9.
      * An index into bucket (and therefore die value)
       01 i PIC 9.
      * An index into bucket used when I refers to something else
       01 j PIC 9.
       PROCEDURE DIVISION.
       YACHT.
      * Setup dice-{counts,sum}.
      *   The sum being done here to avoid the need for a second pass.
         INITIALIZE dice-counts.
         INITIALIZE dice-sum.
         PERFORM count_digits UNTIL WS-DICE = 0.

      * 0 is a common result, set it here allows other code to be simpler
         MOVE 0 to WS-RESULT
         EVALUATE WS-CATEGORY
           WHEN 'yacht'
             IF is-yacht
               MOVE 50 to WS-RESULT
           WHEN 'ones'
             MOVE bucket(1) to WS-RESULT
           WHEN 'twos'
             MULTIPLY bucket(2) by 2 giving WS-RESULT
           WHEN 'threes'
             MULTIPLY bucket(3) by 3 giving WS-RESULT
           WHEN 'fours'
             MULTIPLY bucket(4) by 4 giving WS-RESULT
           WHEN 'fives'
             MULTIPLY bucket(5) by 5 giving WS-RESULT
           WHEN 'sixes'
             MULTIPLY bucket(6) by 6 giving WS-RESULT
           WHEN 'full house'
      *      0 is a special value (not a valid index)
             MOVE 0 to j
             PERFORM seek_house VARYING i FROM 1 UNTIL i = 7
           WHEN 'four of a kind'
             PERFORM seek_four_of VARYING i FROM 1 UNTIL i = 7
           WHEN 'little straight'
             IF is-little-straight
               MOVE 30 to WS-RESULT
           WHEN 'big straight'
             IF is-big-straight
               MOVE 30 to WS-RESULT
           WHEN 'choice'
             MOVE dice-sum to WS-RESULT
         END-EVALUATE.

      *  This paragraph used to set up dice-counts and dice-sum,
      *    which should be set to 0 before calling this in a loop
         count_digits.
      *    Grab just the one dice, using division to pop a base-10 digit off
           DIVIDE WS-DICE BY 10 GIVING WS-DICE REMAINDER i
      *    Increment the bucket this die falls into
           ADD 1 to bucket(i)
      *    Add the value to the sum of the dice
           ADD i to dice-sum
           EXIT.

      *  Scanning buckets looking for one that can score as 4-of
         seek_four_of.
           IF bucket(i) = 4 or 5
      *      Regardless of how many dice were here, you score 4 of them.
             MULTIPLY 4 by i giving WS-RESULT
           EXIT.

      *  Scan buckets looking for a full house, which needs BOTH a 2-of & 3-of
         seek_house.
           EVALUATE bucket(i)
             WHEN 0
      *        Not useful information
               CONTINUE
             WHEN 2 THRU 3
      *        Progress toward a full house
               IF j = 0
      *          It's the first one hit, note it.
                 MOVE i to j
               ELSE
      *          It's the second one, this IS a full-house
                 MOVE dice-sum to WS-RESULT
                 EXIT SECTION
             WHEN OTHER
      *        A full house cannot contain any 1x, 4x, or 5x any value.
               EXIT SECTION
           END-EVALUATE.
           EXIT.
