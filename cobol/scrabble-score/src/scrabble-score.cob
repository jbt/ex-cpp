       IDENTIFICATION DIVISION.
       PROGRAM-ID. SCRABBLE-SCORE.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *Inputs
      * The board is only 15x15  WHEN '                                           so should be length 15  WHEN 'but a test case requires 26
       01 WS-WORD   PIC A(26).
      *Outputs
       01 WS-RESULT PIC 99.

      *Internal
       01 i USAGE INDEX.
       PROCEDURE DIVISION.
       SCRABBLE-SCORE.
           MOVE 0 TO WS-RESULT.
           MOVE FUNCTION UPPER-CASE(WS-WORD) TO WS-WORD.
           PERFORM score-letter 
                   VARYING i FROM 1 
                   UNTIL i > FUNCTION LENGTH(WS-WORD).

       score-letter.
           EVALUATE WS-WORD(i:1)
           WHEN 'A'
           WHEN 'E'
           WHEN 'I'
           WHEN 'O'
           WHEN 'U'
           WHEN 'L'
           WHEN 'N'
           WHEN 'R'
           WHEN 'S'
           WHEN 'T'
              ADD 1 TO WS-RESULT
           WHEN 'D'
           WHEN 'G'
              ADD 2 TO WS-RESULT
           WHEN 'B'
           WHEN 'C'
           WHEN 'M'
           WHEN 'P'
              ADD 3 TO WS-RESULT
           WHEN 'F'
           WHEN 'H'
           WHEN 'V'
           WHEN 'W'
           WHEN 'Y'
              ADD 4 TO WS-RESULT
           WHEN 'K'
              ADD 5 TO WS-RESULT
           WHEN 'J'
           WHEN 'X'
              ADD 8 TO WS-RESULT
           WHEN 'Q'
           WHEN 'Z'
              ADD 10 TO WS-RESULT
           END-EVALUATE.
