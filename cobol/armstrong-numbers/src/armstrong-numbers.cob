       IDENTIFICATION DIVISION.
       PROGRAM-ID. armstrong-numbers.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       REPOSITORY. FUNCTION ALL INTRINSIC.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Pre : Base-10 textual version of the number to check. 
      *       Must-be at least one digit!
      * Post: Unchanged
       01 WS-NUMBER PIC X(8).
      * Pre : Ignored
      * Post: 1 if WS-NUMBER is an Armstrong number, 0 if not
       01 WS-RESULT PIC 9.

      * Internal variables

      * Index into WS-NUMBER
       01 i PIC 9 USAGE COMP-5.
          88 past-end VALUE 9.
      * The value derived from one particular digit of WS-NUMBER
       01 digit PIC 9(9) USAGE COMP-5.
      * The sum of the various values of digit
       01 val PIC 9(18) USAGE COMP-5.
      * The power to raise each digit to
       01 power PIC 9 USAGE COMP-5.
       PROCEDURE DIVISION.
       ARMSTRONG-NUMBERS.
           MOVE FUNCTION LENGTH(FUNCTION TRIM(WS-NUMBER)) TO power.
           MOVE 0 TO val.
           PERFORM add-digit VARYING i FROM 1 UNTIL past-end.
           IF val IS EQUAL TO FUNCTION NUMVAL(WS-NUMBER)
              MOVE 1 TO WS-RESULT
           ELSE 
              MOVE 0 TO WS-RESULT.
       add-digit.
           IF NOT WS-NUMBER(i:1) IS NUMERIC
              EXIT PARAGRAPH.
           MOVE WS-NUMBER(i:1) TO digit.
           COMPUTE digit = digit ** power.
           ADD digit TO val.
