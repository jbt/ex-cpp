       IDENTIFICATION DIVISION.
       PROGRAM-ID. luhn.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       REPOSITORY. FUNCTION ALL INTRINSIC.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      * Pre : The card number to check
      * Post: Please ignore
       01 WS-CARD-NUMBER PIC X(32).
      * Pre : Ignored
      * Post: "VALID" or "FALSE"
       01 WS-VALID PIC X(5).
       
      * Internal variables

      * A digit
       01 digit PIC 99 USAGE BINARY.
      * The sum of the digits after doubling some
       01 ws-sum PIC 9(18) USAGE COMP-5.
      * Index into card numbers
       01 i USAGE INDEX.
          88 past-end VALUES 33 THRU 99.
       01 len PIC 99 BINARY.
       01 factor PIC 9 BINARY.

       PROCEDURE DIVISION.
       LUHN.
           MOVE LENGTH(FUNCTION TRIM(WS-CARD-NUMBER)) TO len.
           IF len < 2
              MOVE 'FALSE' TO WS-VALID
              EXIT PARAGRAPH.
           MOVE FUNCTION REVERSE(WS-CARD-NUMBER(1:len)) 
             TO WS-CARD-NUMBER.
           MOVE 0 to ws-sum.
           MOVE 2 to factor.
           PERFORM handle-digit VARYING i FROM 2 UNTIL past-end.
           COMPUTE digit = FUNCTION MOD(
                   10 - FUNCTION MOD( ws-sum, 10)
                   , 10).
           IF digit IS EQUAL TO NUMVAL(WS-CARD-NUMBER(1:1))
              MOVE 'VALID' TO WS-VALID
           ELSE 
              MOVE 'FALSE' TO WS-VALID.

       handle-digit.
           IF WS-CARD-NUMBER(i:1) IS EQUAL TO ' '
              EXIT PARAGRAPH.
           MOVE WS-CARD-NUMBER(i:1) TO digit.
           MULTIPLY factor BY digit.
           IF digit > 9 
              SUBTRACT 9 FROM digit.
           ADD digit TO ws-sum.
           SUBTRACT factor FROM 3 GIVING factor.
           MOVE FUNCTION MOD(ws-sum, 10) TO ws-sum.
