       IDENTIFICATION DIVISION.
       PROGRAM-ID. raindrops.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Pre : The number to consider
      * Post: Unchanged
       01 WS-NUMBER PIC 9(4) BINARY.
      * Pre : Ignored
      * Post: PlingPlangPlong combinations or the number's base-10 rep
       01 WS-RESULT PIC X(20).

      * Internal variable(s)

      * The position of the first trailing space (padding) in WS-RESULT
       01 i USAGE INDEX.
       PROCEDURE DIVISION.
       RAINDROPS.
           INITIALIZE WS-RESULT.
           MOVE 1 TO i.
           IF FUNCTION MOD(WS-NUMBER, 3) IS EQUAL TO 0
              STRING "Pling" INTO WS-RESULT WITH POINTER i.
           IF FUNCTION MOD(WS-NUMBER, 5) IS EQUAL TO 0
              STRING "Plang" INTO WS-RESULT WITH POINTER i.
           IF FUNCTION MOD(WS-NUMBER, 7) IS EQUAL TO 0
              STRING "Plong" INTO WS-RESULT WITH POINTER i.
           IF i IS EQUAL TO 1
              MOVE WS-NUMBER TO WS-RESULT.
