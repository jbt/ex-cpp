       IDENTIFICATION DIVISION.
       PROGRAM-ID. square-root.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-NUMBER PIC 9(32).
       01 WS-SQRT PIC 9(18) USAGE COMP-5.

       01 hi PIC 9(32).
       01 lo PIC 9(18) USAGE COMP-5.
       PROCEDURE DIVISION.
       SQUARE-ROOT.
           MOVE 0 TO lo.
           ADD 1 TO WS-NUMBER GIVING hi.
           PERFORM bisect 
                   UNTIL hi IS EQUAL TO lo 
                   OR WS-SQRT * WS-SQRT IS EQUAL TO WS-NUMBER.
       bisect.
           COMPUTE WS-SQRT = lo + (hi - lo) / 2.
           DISPLAY WS-NUMBER ' : hi ' hi ' lo ' lo ' mid ' WS-SQRT
           IF WS-SQRT * WS-SQRT > WS-NUMBER
              MOVE WS-SQRT TO hi
           ELSE IF WS-SQRT IS EQUAL TO lo
              ADD 1 TO lo
           ELSE 
              MOVE WS-SQRT TO lo.
