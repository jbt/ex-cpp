       IDENTIFICATION DIVISION.
       PROGRAM-ID. roman-digit.
      *  *Callable module*
      * Select the next digit in a roman numeral representing the given number
      * Precondition: numval is positive
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * A "5" digit and a "1" digit for a given order of magnitude
       01 symbols.
         05 symbol PIC A OCCURS 2 TIMES.
      * The value of 1 in the current order of magnitude
       01 multiplier PIC 9999 USAGE BINARY.
      * The most-significant part of numval, truncated to order-of-magnitude
       01 bigend PIC 99 USAGE BINARY.
       LINKAGE SECTION.
      * IN/OUT
      * PRE: The a positive value less than 5000 being converted
      * POST:The value after accomodating for digit. Typically this means it's reduced.
      *     NOTE: In the subtractive case it will actually go up temporarily.
       01 numval PIC 9999.
      * PRE: ignored
      * POST:THe next digit to be added to the roman numeral
       01 digit PIC A.
       PROCEDURE DIVISION USING numval, digit.
      * Initial value should be ignored. Place in a digit that will make errors more clear.
         MOVE '?' to digit.

         MOVE 1000 to multiplier.
      * We don't have a digit for 5000. Putting ! in that slot to make the error more obvious.
         MOVE '!M' to symbols.
         PERFORM pick-digit.

         MOVE 100 to multiplier.
         MOVE 'DC' to symbols.
         PERFORM pick-digit.

         MOVE 10 to multiplier.
         MOVE 'LX' to symbols.
         PERFORM pick-digit.

         MOVE 1 to multiplier.
         MOVE 'VI' to symbols.
         PERFORM pick-digit.

         pick-digit.
            DIVIDE numval by multiplier giving bigend.
            EVALUATE bigend
              WHEN 5 THRU 8
      *         Enough room in the number's value for a V/L/D digit.
                MOVE symbol(1) to digit
                COMPUTE numval = numval - 5 * multiplier
              WHEN 1 thru 3
      *         Enough room left in the valuef or a I/X/C/M
                MOVE symbol(2) to digit
                SUBTRACT multiplier from numval
              WHEN 0
      *         No value left at this order-of-magnitude. Bail back to main program.
                EXIT PARAGRAPH
              WHEN OTHER
      *         Subtractive (4/9) case!! e.g. IX , IV , XL , XC , etc.
      *         Return the I/X/C/M _but_ ....
                MOVE symbol(2) to digit
      *         *INCREASE* numval by 1, so next time through we'll pick up the 5 or 10 digit.
                ADD multiplier to numval
            END-EVALUATE.
      *     If we get here we populated digit, or numval was zero
            EXIT PROGRAM.
