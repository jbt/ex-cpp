000001 IDENTIFICATION DIVISION.
       PROGRAM-ID. ROMAN-NUMERALS.
      * Convert a positive numeric value to a string in roman numerals
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * PRE: The positive number to convert
      * POST:Please ignore (zero)
       01 WS-NUMBER PIC 9999.
         88 is-zero VALUE 0.
      * PRE: Ignored
      * POST:A string of roman numerals representing that value
       01 WS-RESULT PIC X(20).

      ** *NOTE* : Below are internal variables

      * String representation of the _next_ digit to append
       01 digit PIC A.
      * Index into WS-RESULT of where the next digit is to go
       01 i PIC 99 USAGE BINARY.
       PROCEDURE DIVISION.
       ROMAN-NUMERALS.
      * Set to the empty string
       INITIALIZE WS-RESULT.
      * The first digit goes in the first slot
       MOVE 1 to i.
      * Keep consuming WS-NUMBER until it's gone.
       PERFORM UNTIL is-zero OR i >= 19
      * I'm well aware that this could've been a paragraph instead of a module.
      *   The only excuse is that I wanted to try my hand at a callable module.
         CALL "roman-digit" USING WS-NUMBER, digit
      * Append that digit into the result.
         STRING digit INTO WS-RESULT WITH POINTER i
       END-PERFORM.
