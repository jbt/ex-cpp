       IDENTIFICATION DIVISION.
       PROGRAM-ID. two-fer.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Pre : The user's name
      * Post: Unchanged
       01 WS-NAME PIC X(16).
      * Pre : Ignored
      * Post: The desired phrase
       01 WS-RESULT PIC X(64).
       
       PROCEDURE DIVISION.
       TWO-FER.
           INITIALIZE WS-RESULT.
           IF FUNCTION LENGTH(FUNCTION TRIM(WS-NAME)) = 0
              MOVE "One for you, one for me." TO WS-RESULT
           ELSE
              STRING "One for " FUNCTION TRIM(WS-NAME) ", one for me." 
                     INTO WS-RESULT.
